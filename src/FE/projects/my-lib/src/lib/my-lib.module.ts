import { ModuleWithProviders, NgModule } from '@angular/core';
import { TableModule } from 'primeng/table';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ImageModule } from 'primeng/image';
import { SelectButtonModule } from 'primeng/selectbutton';
import { FileUploadModule } from 'primeng/fileupload';
import { MultiSelectModule } from 'primeng/multiselect';
import { HandleLinkYoutubePipe } from './pipes/handle-link-youtube.pipe';

import { DateTimePipe } from './pipes/date-time.pipe';
import { AcceptFilePipe } from './pipes/accept-file.pipe';
import { CurrencyPipe } from './pipes/currency.pipe';
import { FunctionPipe } from './pipes/function.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';

import { CalendarComponent } from './component/calendar/calendar.component';
import { TableComponent } from './component/table/table.component';
import { ConfirmComponent } from './component/confirm/confirm.component';
import { CheckboxComponent } from './component/checkbox/checkbox.component';
import { LoadingComponent } from './component/loading/loading.component';
import { ViewComponent } from './component/view/view.component';
import { UploadComponent } from './component/upload/upload.component';
import { MarkdownHtmlComponent } from './component/markdown-html/markdown-html.component';

import { SettingDisplayColumnTableComponent } from './component/table/setting-display-column-table/setting-display-column-table.component';
import { CropImageComponent } from './component/upload/crop-image/crop-image.component';
import { PaginatorModule } from 'primeng/paginator';
import { InputSearchComponent } from './component/input-search/input-search.component';
import { InputTextModule } from 'primeng/inputtext';
import { TagModule } from 'primeng/tag';
import { MenuModule } from 'primeng/menu';
import { ResizeColumnTableDirective } from './directives/resize-column-table.directive';
import { MarkdownModule } from "ngx-markdown";
import { AngularEditorModule } from "@kolkov/angular-editor";
import { InputTextareaModule } from 'primeng/inputtextarea';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LibHelperService } from './shared/services/lib-helper.service';
import { SkeletonModule } from 'primeng/skeleton';
import { SkeletonComponent } from './component/skeleton/skeleton.component';
import { MultiSelectComponent } from './component/multi-select/multi-select.component';
import { VideoComponent } from './component/video/video.component';
import {PopupConfirmComponent} from './component/popup-confirm/popup-confirm.component';
import { LoginComponent } from './component/login/login.component';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { ProductPopUpTopBarComponent } from './component/product-pop-up-top-bar/product-pop-up-top-bar.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { BreadcrumbComponent } from './component/layout/breadcrumb/breadcrumb.component';
import { LabelDropdownPipe } from './pipes/label-dropdown.pipe';
import { TextareaAutoresizeDirective } from './directives/textarea-autoresize.directive';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SliderModule } from 'primeng/slider';
import { KeyFilterModule } from 'primeng/keyfilter';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ImageResizePipe } from './pipes/image-resize.pipe';
import { ClickDropdownDirective } from './directives/click-dropdown.directive';
import { ButtonComponent } from './component/button/button.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TableModule,
        CalendarModule,
        CheckboxModule,
        ImageModule,
        SelectButtonModule,
        FileUploadModule,
        PaginatorModule,
        InputTextModule,
        TagModule,
        MenuModule,
        InputTextareaModule,
        MarkdownModule,
        AngularEditorModule,
        NgxSpinnerModule,
        SkeletonModule,
        MultiSelectModule,
        ReactiveFormsModule,
        ImageCropperModule,
        SliderModule,
		KeyFilterModule, 
        RadioButtonModule,
    ],
    declarations: [
        ResizeColumnTableDirective,
        TableComponent,
        CalendarComponent,
        ViewComponent,
        ConfirmComponent,
        CheckboxComponent,
        LoadingComponent,
        ViewComponent,
        UploadComponent,
        MarkdownHtmlComponent,
        SettingDisplayColumnTableComponent,
        CropImageComponent,
        HandleLinkYoutubePipe,
        DateTimePipe,
        AcceptFilePipe,
        CurrencyPipe,
        FunctionPipe,
        TruncatePipe,
        InputSearchComponent,
        SkeletonComponent,
        MultiSelectComponent,
        VideoComponent,
        PopupConfirmComponent,
        LoginComponent,
        ClickOutsideDirective,
		TextareaAutoresizeDirective,
        ProductPopUpTopBarComponent,
        PageNotFoundComponent,
        BreadcrumbComponent,
        LabelDropdownPipe,
        ImageResizePipe,
		ClickDropdownDirective,
        ButtonComponent
    ],
    exports: [
        ResizeColumnTableDirective,
        TableComponent,
        CalendarComponent,
        ViewComponent,
        ConfirmComponent,
        CheckboxComponent,
        LoadingComponent,
        ViewComponent,
        UploadComponent,
        MarkdownHtmlComponent,
        SettingDisplayColumnTableComponent,
        CropImageComponent,
		ProductPopUpTopBarComponent,
        LoginComponent,
        PageNotFoundComponent,
        BreadcrumbComponent,
		InputSearchComponent,
        SkeletonComponent,
        MultiSelectComponent,
        PopupConfirmComponent,
        VideoComponent,
		// Pipes
        HandleLinkYoutubePipe,
        DateTimePipe,
        AcceptFilePipe,
        CurrencyPipe,
        FunctionPipe,
        TruncatePipe,
		LabelDropdownPipe,
		ImageResizePipe,
		// Directives
        ClickOutsideDirective,
		TextareaAutoresizeDirective,
		ClickDropdownDirective,
        ButtonComponent,
    ]
})

export class MyLibModule {
    public static forRoot(environment: any): ModuleWithProviders<MyLibModule> {
        return {
            ngModule: MyLibModule,
            providers: [
                LibHelperService,
                {
                    provide: 'env',
                    useValue: environment
                },
            ]
        };
    }
}
