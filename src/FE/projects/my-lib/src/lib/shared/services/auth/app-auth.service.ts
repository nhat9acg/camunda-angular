import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';
import { Observable, concatMap, of } from 'rxjs';
import { TokenService } from './token.service';
import { AuthParamConsts, BaseConsts } from '../../consts/base.consts';
import { ITokenDecode, ITokenResponse } from '../../interfaces/token.interface';
import { Utils } from '../../utils';
import jwtDecode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class AppAuthService {

    rememberMe: boolean;
    environment: any;
    constructor(
        private _tokenService: TokenService,
        private http: HttpClient,
        @Inject('env') environment
    ) {
        this.environment = environment;
        this.api = environment.api;
        this.urlAuthLogin = environment.urlAuthLogin;
    }

    api:string;
    urlAuthLogin:string;

    logout(): Observable<any>{
        const headers = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: "text/plain",
        });
		return this.http.post<any>(`${this.api}/connect/logout`, null, { headers: headers });
	}


    getAuthenticateLogout(returnUrl: string): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'text/plain',
        });

        const params = new HttpParams().set('returnUrl', returnUrl);

        return this.http.get<any>(`${this.api}/authenticate/logout`, { headers: headers, params: params });
    }

    connectAuthorize(baseUrlProduct?:string): void {
      window.location.href = `${baseUrlProduct || this.environment.baseUrlHome}`
        // const redirectLoginPath = '/auth/login/redirect';
        // const redirectUri = `${baseUrlProduct || this.environment.baseUrlHome}${redirectLoginPath}?callBack=true`;
        // const base64URL = (string) => {
        //     return string.toString(CryptoJS.enc.Base64).replace(/=/g, '').replace(/\+/g, '-').replace(/\//g, '_')
        // }
        // //
        // const codeVerifier = Utils.makeRandom();
        // const state = Utils.makeRandom(20);
        // const codeChallenge = base64URL(CryptoJS.SHA256(codeVerifier));
        // //
        // this._tokenService.setCodeVerifier(codeVerifier);
        // this._tokenService.setState(state);
        // //
        // let url = `${this.urlAuthLogin}/connect/authorize?`;
        // url += Utils.convertParamUrl('response_type', AuthParamConsts.responseType);
        // url += Utils.convertParamUrl('redirect_uri', redirectUri);
        // url += Utils.convertParamUrl('client_id', AuthParamConsts.clientId);
        // url += Utils.convertParamUrl('scope', AuthParamConsts.scope);
        // url += Utils.convertParamUrl('state', state);
        // url += Utils.convertParamUrl('code_challenge', codeChallenge);
        // url += Utils.convertParamUrl('code_challenge_method', AuthParamConsts.codeChallengeMethod);
        // //
        // window.location.href = url;
    }

    connectToken(code: string, codeVerifier: string, redirectUri: string) {
        let url = `${this.urlAuthLogin}/connect/token`;
        const params = new HttpParams()
            .set('grant_type', AuthParamConsts.grantTypeAuthorization)
            .set('client_id', AuthParamConsts.clientId)
            .set('client_secret', AuthParamConsts.clientSecret)
            .set('redirect_uri', redirectUri)
            .set('code', code)
            .set('code_verifier', codeVerifier);
        //
        const headers = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: "text/plain",
        });

        return this.http.post(url, params.toString(), { headers: headers}).pipe(
            concatMap((response: ITokenResponse) => {                //
                const tokenExpireDate = (response?.expires_in ? (+response.expires_in/3600) : 1)/24; // Quy đổi thời gian sang đơn vị ngày
                this._tokenService.setToken(response.access_token, tokenExpireDate);
                this._tokenService.setRefreshToken(response.refresh_token, tokenExpireDate*3);
                const tokenDecode: ITokenDecode = jwtDecode(response?.access_token);
                Utils.setLocalStorage(
                    BaseConsts.localStorageUser,
                    {
                        username: tokenDecode?.username,
                        userType: tokenDecode?.user_type,
                        id: tokenDecode?.user_id,
                        fullName: tokenDecode?.name
                    }
                )
                //
                this._tokenService.clearStateAndCodeVerifier();
                //
                return of(response);
            })
        );
    }

    refreshToken() {
        let url = `${this.urlAuthLogin}/connect/token`;
        const refreshToken = this._tokenService.getRefreshToken();
        const params = new HttpParams()
                    .set("refresh_token", refreshToken)
                    .set("grant_type", AuthParamConsts.grantTypeRefreshToken)
                    .set("client_id", AuthParamConsts.clientId)
                    .set("client_secret", AuthParamConsts.clientSecret);
        //
        const headers = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: "text/plain",
        });
        //
        return this.http.post(url, params.toString(), { headers: headers}).pipe(
            concatMap((response: ITokenResponse) => {
                const tokenExpireDate = (response?.expires_in ? (+response.expires_in/3600) : 1)/24; // Quy đổi thời gian sang đơn vị ngày
                this._tokenService.setToken(response.access_token, tokenExpireDate);
                this._tokenService.setRefreshToken(response.refresh_token, tokenExpireDate*3);
                return of(response);
            })
        );
    }

	getUserInfo() {
		const domainApi = this.environment.urlAuthLogin;
		return this.http.get(`${domainApi}/api/auth/user/find-by-user`);
	}

	changePassword(body) {
		const domainApi = this.environment.api;
		return this.http.put(`${domainApi}/api/auth/user/change-password`, body);
	}
}
