import { Injectable } from "@angular/core";
import { DomSanitizer, SafeHtml, SafeUrl } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { Location } from "@angular/common";
import { ConfirmComponent } from "../../component/confirm/confirm.component";
import { UploadComponent } from "../../component/upload/upload.component";
import { ViewComponent } from "../../component/view/view.component";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable, catchError, finalize, throwError, timeout } from "rxjs";
import { IResponseItem } from "../interfaces/response.interface";
import { ContentTypeEView, IconConfirm, StatusResponseConst } from "../consts/base.consts";
import { PopupConfirmComponent } from "../../component/popup-confirm/popup-confirm.component";
import { IDialogConfirmConfig, IDialogUploadFileConfig, ILabelButton, IParamHandleDTO } from "../interfaces/base.interface";
import createHmac from "create-hmac";

@Injectable({
  providedIn: 'root'
})

export class LibHelperService {

    constructor(
        public messageService: MessageService,
        public sanitizer: DomSanitizer,
        public dialogService: DialogService,
        public router: Router,
        public activatedRoute: ActivatedRoute,
        public location: Location,
        public http: HttpClient,
    ) {}

    exceptionCodeWarnings = [6005, 6006, 7012, 7013, 7055, 4106, 4107, 7095, 7094, 7099, 7100, 7105, 7106, 7107, 7108, 7113, 7114, 7115, 7116, 7084, 7120, 7140, 7141];
    exceptionErrorCodeNothingPass = [4104, 1031, 1003, 7120];

    handleResponseInterceptor(response: any, message?: string): boolean {
        if (response?.status === StatusResponseConst.RESPONSE_TRUE) {
            if (message) {
                this.messageService.add({ severity: 'success', summary: '', detail: message, life: 1000 });
            } else if(response?.successFE && message === undefined) {
                // MESSAGE CHUNG CHO CREATE | UPDATE | DELETE KHI KHÔNG TRUYỀN VÀO PARAM MESSAGE
                this.messageService.add({ severity: 'success', summary: '', detail: response.successFE, life: 1000 });
            }
            return true;
        } else {
            if(this.exceptionCodeWarnings.includes(response.code)){
                this.messageService.add({ severity: 'warn', summary: '', detail: response.message, life: 3000 });
                return true;
            } else if(this.exceptionErrorCodeNothingPass.includes(response.code)) {
                //warning không pass
                this.messageService.add({ severity: 'warn', summary: '', detail: response.message, life: 3000 });
                return false;
            }
            let dataMessage = response?.data;
            if (dataMessage && typeof dataMessage === 'object') {
                this.messageService.add({ severity: 'error', summary: '', detail: dataMessage[Object.keys(dataMessage)[0]], life: 2500 });
            } else {
                let message = response?.message;
                if(response?.code > 1 && response?.code < 1000) {
                    message = "Có lỗi xảy ra vui lòng thử lại sau!";
                }
                this.messageService.add({ severity: 'error', summary: '', detail: response?.message, life: 2500 });
            }
            return false;
        }
    }

    messageError(msg = '', summary = '', life = 2500) {
        this.messageService.add({ severity: 'error', summary, detail: msg, life: life });
    }

    messageSuccess(msg = '', summary = '', life = 2000) {
        this.messageService.add({ severity: 'success', summary, detail: msg, life: life });
    }

    messageWarn(msg = '', life = 3000) {
        this.messageService.add({ severity: 'warn', summary: '', detail: msg, life: life, icon: 'pi-bell' });
    }

    // LỌC PROPERTY CHỈ LẤY CÁC PROPERTY CỦA MODEL ĐỂ PUSH VÀO API
    mapDTO(data: Object, model: Object, paramHanldes?: IParamHandleDTO) {
        paramHanldes = {
            idName: paramHanldes?.idName || 'id',
            isCheckNull: !!paramHanldes?.isCheckNull,
        }
        //
        let newData: any = {};
        for (const key of Object.keys(model)) {
            // NẾU ID NULL THÌ KHÔNG ĐẨY LÊN, NẾU CHECK NULL THÌ CÁC TRƯỜNG NULL KHÔNG ĐẨY LÊN
            if((key === paramHanldes?.idName || paramHanldes.isCheckNull) && !data[key]) continue;
            newData[key] = data[key];
        }
        return newData;
    }

    getAtributionPopupConfirmDialog(message: string, icon: IconConfirm = IconConfirm.WARNING, reason: boolean) {
        return {
            header: "Thông báo",
            with: 'auto',
            style: {'min-width': '350px'},
            data: {
                message : message,
                icon: icon,
                reason: reason,
            },
        } as DynamicDialogConfig;
    }

    getBlobUrlImage(image: File): SafeUrl {
        return this.sanitizer.bypassSecurityTrustUrl(image?.['objectURL']?.['changingThisBreaksApplicationSecurity']);
    }

    getContentHtml(content): SafeHtml {
        return this.sanitizer.bypassSecurityTrustHtml(content);
    }

    checkValidForm(isValid: boolean, ) {
        if(!isValid) this.messageError('Vui lòng nhập đủ thông tin');
    }

    dialogConfirmRef(message: string, options?: IDialogConfirmConfig): DynamicDialogRef {
        return this.dialogService.open(
            ConfirmComponent,
            {
                header: options?.header || "Thông báo",
                width: options?.width || '560px',
                data: {
                    message: message,
                    ...(options || {}),
                },
            },
        );
    }


    dialogConfirmPopupRef(message: string, icon?: IconConfirm, reason?: boolean): DynamicDialogRef {
        return this.dialogService.open(
            PopupConfirmComponent,
            this.getAtributionPopupConfirmDialog(message, icon, reason)
        );
    }

    // dialogRequestRef(id: number, summary: string): DynamicDialogRef {
    //     return this.dialogService.open(
    //         ERequestComponent,
    //         {
    //             header: "Trình duyệt",
    //             width: '600px',
    //             data: {
    //                 id: id,
    //                 summary: summary,
    //             },
    //         }
    //     );
    // }

    // dialogApproveRef(id: number): DynamicDialogRef {
    //     return this.dialogService.open(
    //         EApproveComponent,
    //         {
    //             header: "Phê duyệt",
    //             width: '600px',
    //             data: {
    //                 id: id
    //             },
    //         }
    //     );
    // }

    dialogUploadRef(params?: IDialogUploadFileConfig): DynamicDialogRef {
        return this.dialogService.open(
            UploadComponent,
            {
                header: params?.header || 'Preview Media',
                width: params?.width || '650px',
                data: {
                    ...(params || {}),
                },
            }
        )
    }

    dialogViewerRef(content: any, type: ContentTypeEView, params?: DynamicDialogConfig) {
        params = {
            ...(params || new DynamicDialogConfig()),
            contentStyle: params?.contentStyle || {},
        }
        //
        if(type === ContentTypeEView.FILE) {
            params.contentStyle['padding-top'] = 0;
            params.contentStyle['padding-bottom'] = 0;
        } else {
            if(!params.contentStyle['padding-bottom']) {
                params.contentStyle['padding-bottom'] = '10px';
            }
        }
        //
        params.header = params.header || 'Preview';
        if(type === ContentTypeEView.IMAGE) params.header = '';
        //
        return this.dialogService.open(ViewComponent, {
            header: params.header,
            width: params.width || '100%',
            style: {...(params.style || {}), 'border-radius': 0 },
            contentStyle: {...(params.contentStyle || {}) },
            styleClass: params.styleClass + ' height-100 ' + (type === ContentTypeEView.IMAGE && ' no-background '),
            data: {
                content: content,
                type: type,
            },
        });
    }

    // Xử lý isLoading khi upload hoặc move ảnh
    private $isUploadMedia = new BehaviorSubject(false);

    public get IsUploadMediaObservable(): Observable<any> {
        return this.$isUploadMedia.asObservable();
    }

    public setUploadMedia(value?:boolean) {
        this.$isUploadMedia.next(value);
    }

	messageTimeout = "Vui lòng kiểm tra lại đường truyền mạng. Thời gian phản hồi tối đa 15s!";

    upload(body, domain): Observable<any> {
        this.setUploadMedia(true);
        const url = domain + '/v1/media'
        return this.http.post<any>(url, body, {
            params: { unAuthorize: true },
            headers: {
                "X-Client-Source": "meeybank"
            }
        }).pipe(
            timeout(15000),
            catchError(error => {
                if(!error?.error?.message) {
                    this.messageError(this.messageTimeout)
                }
                return error;
            }),
            finalize(() => {
                this.setUploadMedia(false);
            })
        );
    }

    moveData(body, domain): Observable<any> {
        this.setUploadMedia(true);
        const url = domain + '/v1/media/move';
		const headers = new HttpHeaders({"X-Client-Source": "meeybank"});

        return this.http.patch(url, body , { headers }).pipe(
            finalize(() => {
                this.setUploadMedia(false);
            })
        );
    }

    downloadFile(url: string, unAuthorize = true): Observable<any> {
        return this.http.get(url, { responseType: 'blob', params: { unAuthorize: unAuthorize }, observe: 'response' });
    }

    deleteMedia(body, domain): Observable<any>{
        let url = domain + '/v1/image/upload-image-cube-map';
        return this.http.request('delete', url,
            {
                body: body,
                headers: { "X-Client-Source": "meeybank" }
            }
        )
    }

    resizeImage(body, domain): Observable<any>{
        this.setUploadMedia(true);
        const url = domain + '/v1/image/resize-on-fly';
        return this.http.post<any>(url, body, {
            params: { unAuthorize: true },
            headers: {
                "X-Client-Source": "meeybank"
            }
        }).pipe(
            timeout(15000),
            catchError(error => {
                if(!error?.error?.message) {
                    this.messageError(this.messageTimeout)
                }
                return error;
            }),
            finalize(() => {
                this.setUploadMedia(false);
            })
        );
    }

    redirectPageNotFound(){
        this.router.navigate(['/page-not-found'])
    }
}
