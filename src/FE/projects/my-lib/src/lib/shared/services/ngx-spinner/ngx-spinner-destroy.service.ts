import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NgxSpinnerDestroyService {
    constructor() {}

    ngxSpinner = new BehaviorSubject<any>(true);


}
