import { EFormatDate, EFormatDateDisplay } from "../consts/base.consts";
import { ETableColumnType, ETableFrozen, OrderSort, ValueType } from "../consts/lib-table.consts";

export interface IColumn {
    field: string;
    header: string;
    title?:string,
    width?: number;
    minWidth?: number;
    left?: number; // fix frozen left
    right?: number; // fix frozen right
    isShow?: boolean,
    hasUpdateIsShow?: boolean,
    isSort?: boolean,
    type?: ETableColumnType | EFormatDateDisplay;
	otherTypes?: ETableColumnType[];
    isPin?: boolean; // Khóa cột
    isFrozen?: boolean; // Fixed cột
    alignFrozen?: ETableFrozen;   // Vị trí fixed cột left|right
    isResize?: boolean; // width auto
    class?: string;
    icon?: string;
    classButton?: string;
    isCutText?: boolean;
    position?: number;
    displaySettingColumn?: boolean;
    unit?: string;
    fieldSort?: string;
    getTagInfo?: Function;
    getDisabled?: Function;
    action?: Function;
    customValue?: Function;
    valueType?: ValueType;
    sliceString?: number;
    isPermission?: boolean;
    checkDisableButtonAction?: Function;
}

export interface IAction {
    data: any;
    label: string;
    icon: string;
    command: Function;
}

export interface ISort {
    field: string;
    order: OrderSort;
}

export interface IDropdown {
    name: string;
    code: number | string | boolean;
    severity?: string;
    rawData?: any;
  }