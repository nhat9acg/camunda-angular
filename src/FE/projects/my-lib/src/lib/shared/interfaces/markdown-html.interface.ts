export interface ITempMediaContentImage{
    s3KeyTemp: string,
    urlTemp: string,
    mediatype?: number
}