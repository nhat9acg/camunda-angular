import { EAcceptFile, IconConfirm } from "../consts/base.consts";

export interface IEConfirm {
    message: string,
    icon?: IconConfirm,
    labelButton?: ILabelButton,
}

export interface IParamHandleDTO {
    idName?: string;
    isCheckNull?: boolean;
}

export interface IType {
    name?: string;
    code?: string | number;
}

export interface ITag extends IType {
    severity: string,
}

export interface IErrorCode {
    [key: number] : string,
}

export interface IDialogUploadFileConfig {
    folderUpload?: string,
    header?: string,
    width?:string,
    uploadServer?: boolean,
    multiple?: boolean,
    accept?: EAcceptFile | string,
    quantity?: number, // Số phần tử upload lấy từ phần tử đầu tiên
    previewBeforeUpload?: boolean,
    callback?: Function,
    chooseLabel?: string,
    domain?: string,
    //
    titleInput?: string,
    inputValue?: string,
    inputRequired?: boolean,
    isChooseNow?: boolean,
    isMove?: boolean,
}

export interface IResponseDialogUpload {
    inputData: string,
    fileUrls: string[],
}

export interface ILabelButton {
    accept: string;
    cancel: string;
}

export interface IDialogConfirmConfig {
    header?: string,
    message?: string,
    width?: string,
    icon?: IconConfirm,
    labelButton?: ILabelButton,
    isNoteConfirm?: boolean,
    dropdownData?: any[]
}
