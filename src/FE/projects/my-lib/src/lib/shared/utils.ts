import * as moment from "moment";
import { BaseConsts, ETypeUrlYoutube } from "./consts/base.consts";

export class Utils {

     // LOCAL STORAGE
     public static getLocalStorage(key: string) {
        return JSON.parse(localStorage.getItem(key))
    }

    public static setLocalStorage(key: string,data: any, ) {
        localStorage.setItem(key, JSON.stringify(data));
    }

    public static refreshData(data) {
        return JSON.parse(JSON.stringify(data));
    }

    public static formatDateMonth(value) {
        return (moment(value).isValid() && value) ? moment(value).format('DD/MM') : '';
    }

    public static formatDate(value) {
        return (moment(value).isValid() && value) ? moment(value).format('DD/MM/YYYY') : '';
    }

    public static convertLowerCase(string: string = '') {
        if (string.length > 0) {
            return string.charAt(0).toLocaleLowerCase() + string.slice(1);
        }
        return '';
    }

    /**
     * đảo từ 1-12-2021 -> 2021-12-1
     * @param date
     * @returns
     */
    public static reverseDateString(date: string) {
        return date.split(/[-,/]/).reverse().join("-");
    }

    public static replaceAll(str, find, replace) {
        var escapedFind=find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        return str.replace(new RegExp(escapedFind, 'g'), replace);
    }

    public static transformMoney(num: number, ...args: any[]): string {
        const value = `${num}`;
		    if (value === '' || value === null || typeof value === 'undefined') {
			      return '';
		    }

        let locales = 'vi-VN';
        const cur = Number(value);

        if (args.length > 0) {
            locales = args[0];
        }

        const result = new Intl.NumberFormat(locales).format(cur);
        return result === 'NaN' ? '' : result;
    }

    public static transformPercent(num: number, ...args: any[]): string {
        const value = `${num}`;
        if (value === '' || value === null || typeof value === 'undefined') {
            return '';
        }

        let locales = 'vi-VN';
        const cur = Number(value);

        if (args.length > 0) {
            locales = args[0];
        }

        const result = new Intl.NumberFormat(locales).format(cur);
        return result === 'NaN' ? '' : this.replaceAll(result,'.', ',');
    }

    // BLOCK REQUEST API AFTER 3s

    public static countDifferencePageHeight(): number {
        // const diffenceScreenHeight = window.innerHeight - document.body.offsetHeight;

        const layoutContentEl = document.getElementById("layout-content");
        const pageContentEl = document.getElementById(BaseConsts.pageContentId);

        let diffenceContentHeight : number = 0;
        if(layoutContentEl && pageContentEl) {
            diffenceContentHeight = layoutContentEl.offsetHeight - pageContentEl.offsetHeight;
        }

        console.log('inforHeight',
          {
            pageContentEl: pageContentEl,
            innerHeight: window.innerHeight,
            bodyHeight: document.body.offsetHeight,
            layoutContent: layoutContentEl.offsetHeight,
            pageContentElHeight: pageContentEl.offsetHeight,
            diffenceContentHeight: diffenceContentHeight
          }
        );
        return diffenceContentHeight;
    }

    public static checkLinkYoutube(link: string) {

        let isCheck: boolean = false;
        const urlYoutubes = {
            ...ETypeUrlYoutube,
            ORIGIN: "https://www.youtube.com"
        }
        //
        try {
            if(typeof link === 'string') {
                for(const [key, url] of Object.entries(urlYoutubes)) {
                    isCheck = link?.includes(url);
                    if(isCheck) break;
                }
            }
            //
            return isCheck;
        } catch (error) {
            this.log('checkLinkYoutube', error);
            return false;
        }
    }

    public static log(titleError:string, error?: any) {
        console.log(`%c ${titleError} `, 'background:black; color: red', error);
    }

    public static isExtensionImage(path: string) {
        const extension = path.split('.').pop();
        return BaseConsts.imageExtensions.includes(extension);
    }

    public static isExtensionVideo(path: string) {
        const extension = path.split('.').pop();
        return BaseConsts.videoExtensions.includes(extension);
    }

    public static convertParamUrl(name: string, value: number | string | boolean) {
        return name + "=" + encodeURIComponent("" + value) + "&";
    }

    public static makeRandom(lengthOfCode: number = 100, possible?: string) {
        possible = "AbBCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890-_";
        let text = "";
        for (let i = 0; i < lengthOfCode; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        //
        return text;
    }

    static convertVietnameseToEng(str, isKeepCase = false)
    {
        // if(!isKeepCase) {
        //     str= str.toLowerCase();
        // }
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");

        str= str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g,"A");
        str= str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g,"E");
        str= str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g,"I");
        str= str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g,"O");
        str= str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g,"U");
        str= str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g,"Y");
        str= str.replace(/Đ/g,"D");

        return str;
    }
}

