export enum EMaxLength {
    DEFAULT = 50,
    PHONE = 10,
    ACCOUNT_BANK = 20,
    REFERRAL_CODE = 30,
}

export enum EMediaTypeEnum {
    Image = 1,
    Video = 2,
    File = 3,
}
