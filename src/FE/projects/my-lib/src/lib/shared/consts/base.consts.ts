export enum StatusResonse {
    SUCCESS = 1,
    ERROR = 0,
}

export enum YesNo {
    YES = "Y",
    NO = "N",
}

export enum EFormatDate {
    DATE_TIME_SECOND = 'DD-MM-YYYY HH:mm:ss',
    DATE_TIME = 'DD-MM-YYYY HH:mm',
    DATE = 'DD-MM-YYYY',
    DATE_DMY_Hms = 'DD-MM-YYYY HH:mm:ss',
    DATE_DMY_Hm = 'DD-MM-YYYY HH:mm',
    DATE_DMY = 'DD-MM-YYYY',
}

export enum EFormatDateDisplay {
    DATE_TIME_SECOND = 'DD/MM/YYYY HH:mm:ss',
    DATE_TIME = 'DD/MM/YYYY HH:mm',
    DATE = 'DD/MM/YYYY',
    DATE_DMY_Hms = 'DD/MM/YYYY HH:mm:ss',
    DATE_DMY_Hm = 'DD/MM/YYYY HH:mm',
    DATE_DMY = 'DD/MM/YYYY',
    DATE_YMD_Hms = 'YYYY/MM/DDTHH:mm:ss',
}

export enum IconConfirm {
    APPROVE = '/shared/assets/layout/images/icon-dialog-confirm/approve.svg',
    DELETE = '/shared/assets/layout/images/icon-dialog-confirm/delete.svg',
    WARNING = '/shared/assets/layout/images/icon-dialog-confirm/warning.svg',
    QUESTION = '/shared/assets/layout/images/icon-dialog-confirm/question.svg',
}

export enum IconPopupConfirm {
    ORANGE_ALERT = 'assets/layout/images/icon-dialog-confirm/orange-alert.svg',
    RED_ALERT = 'assets/layout/images/icon-dialog-confirm/red-alert.svg',
}

export enum ContentTypeEView {
    MARKDOWN = 'MARKDOWN',
    HTML = 'HTML',
    IMAGE = "IMAGE",
    FILE = "FILE",
}

export enum EAcceptFile {
    ALL = '',
    IMAGE = 'image',
    VIDEO = 'video',
    MEDIA = 'media',
}

export enum ETypeHandleLinkYoutube {
    CHECK_LINK = 'CHECK_LINK',
    GET_ID = 'GET_ID',
    GET_EMBED_LINK = 'GET_EMBED_LINK',
    GET_WATCH_LINK = 'GET_WATCH_LINK',
}

export enum ETypeUrlYoutube {
    WATCH = 'https://www.youtube.com/watch',
    LIVE = 'https://www.youtube.com/live',
    SHORT = 'https://youtu.be',
}

export class BaseConsts {
    /* Variable readonly  */
    static readonly imageExtensions = ['jpg', 'jpeg', 'png', 'bmp'];
    static readonly fileExtensions = ['pdf', 'xlsx', 'xls', 'doc', 'docx', 'pptx', 'dwg', 'kmz', 'mp3', 'wma'];
    static readonly videoExtensions = ['mp4', 'avi', 'mkv', 'wmv', 'vob', 'flv', 'wmv9', 'mpeg', '3gp', 'webm', 'hevc', 'mov', 'mpg', '3gpp', 'mpeg1', 'mpeg2', 'mpeg4', 'mpegps'];
    //
    static readonly imageExtensionStrings = '.jpg, .jpeg, .png, .bmp';
    static readonly fileExtensionStrings = '.pdf, .xlsx, .xls, .doc, .docx, .pptx, .dwg, .kmz, .mp3, .wma';
    static readonly videoExtensionStrings = '.mp4, .avi, .mkv, .wmv, .vob, .flv, .wmv9, .mpeg, .3gp, .webm, .hevc, .mov, .mpg, .3gpp, .mpeg1, .mpeg2, .mpeg4, .mpegps';
    //
    static readonly pageContentId = "page-content";
    static readonly localStorageUser = 'userInfo';
    static readonly separatorHistory = 'rowSeparator';

    static readonly authorization = {
        accessToken: 'access_token',
        refreshToken: 'refresh_token',
        state: 'state',
        codeVerifier: 'code_verifier',
    };

    static readonly maxSizeUpdate = 10485760;
    static readonly PAGINATOR_MIN_LENGTH_SHOW = 25;
}

export class StatusResponseConst {
  public static list = [
      {
          value: false,
          status: 0,
      },
      {
          value: true,
          status: 1,
      },
  ]

  public static RESPONSE_TRUE = 1;
  public static RESPONSE_FALSE = 0;
  public static HTTP_STATUS_OK = 200;
}

export enum TypeSkeleton {
    FORM = 'FORM',
    TABLE = 'TABLE',
    TAB_VIEW = 'TAB_VIEW',
}

export class AuthParamConsts {
    static grantTypeAuthorization = 'authorization_code';
    static grantTypeRefreshToken = 'refresh_token';
    static responseType = 'code';
    static clientId = 'client-angular';
    static scope = 'openid offline_access';
    static codeChallengeMethod = 'S256';
    static clientSecret = '52F4A9A45C1F21B53B62F56DA52F7';
}

export class PermissionTypes {
    // user
    public static readonly Web = 1;
    public static readonly Menu = 2;
    public static readonly Page = 3;
    public static readonly Table = 4;
    public static readonly Tab = 5;
    public static readonly Form = 6;
    public static readonly ButtonTable = 7;
    public static readonly ButtonAction = 8;
    public static readonly ButtonForm = 9;
}

export class WebKeys {
    //
    public static readonly Home = 1;
    public static readonly User = 1;
    public static readonly Core = 2;
    public static readonly Saler = 3;
    public static readonly Invest = 4;
}
