import { IErrorCode } from "../interfaces/base.interface";

export class ErrorCode {
    public static list: IErrorCode = {
        400: 'BestRequest',
        401: 'Unauthorized',
        403: 'Forbidden',
        404: 'Not Found',
        405: 'Method Not Allowed',
        408: 'Request Time-out',
        415: 'Unsupported Media Type',
        500: 'Internal Server Error',
        502: 'Bad Gateway',
        504: 'Gateway Time-out',
    }
}
