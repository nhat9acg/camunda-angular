export class ActionsConst {

	public static actionList : {icon: string, label: string[]}[] = [
        {
			icon: 'pi pi-upload',
			label: ['Tải lên']
		},
        {
			icon: 'pi pi-plus',
			label: ['Thêm mới'],
		},
        {
			icon: 'pi pi-info-circle',
			label: ['Thông tin chi tiết'],
		},
        {
			icon: 'pi pi-pencil',
			label: ['Chỉnh sửa'],
		},
        {
			icon: 'pi pi-check-circle',
			label: ['Kích hoạt'],
		},
        {
			icon: 'pi pi-times',
			label: ['Hủy kích hoạt', 'Hủy chi trả'],
		},
        {
			icon: 'pi pi-trash',
			label: ['Xóa', 'Xóa tài liệu', 'Xóa đơn vị'],
		},
		{
			icon: 'pi pi-book',
			label: ['Duyệt chi tự động', 'Duyệt chi thủ công', 'Lập danh sách'],
		},
		{
			icon: 'pi pi-download',
			label: ['Test fill data pdf', 'Test fill data word', 'Test fill HĐ cá nhân', 'Test fill HĐ doanh nghiệp', 'Xuất Excel']
		},
	];

	public static getIcon(label) {
		const item = this.actionList.find(item => {
			if (Array.isArray(item.label)) {
				return item.label.includes(label);
			} else {
				return item.label === label;
			}
		});
		return item ? item?.icon : null;
	}
}