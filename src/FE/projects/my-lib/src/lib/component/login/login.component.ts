import { Component, Inject, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppAuthService } from '../../shared/services/auth/app-auth.service';
import { TokenService } from '../../shared/services/auth/token.service';
import { ITokenResponse } from '../../shared/interfaces/token.interface';
import { IEnvironment } from '../../shared/interfaces/environment.interface';

@Component({
    selector: 'lib-login',
    template: '',
})
export class LoginComponent {
    submitting = false;
    dark: boolean;
    environment: IEnvironment;

    constructor(
        public _authService: AppAuthService,
        private _tokenService: TokenService,
        private route: ActivatedRoute,
        private router: Router,
        @Inject('env') environment
    ) {
        this.authorizeCalback = this.route.snapshot.queryParamMap.get('callBack');
        this.environment = environment;
    }

    authorizeCalback: string;

    ngOnInit() {
        let baseUrl: string;
        this.router.navigate(['/home']);
        // this.route.data.subscribe((data: {baseUrl: string}) => {
        //     baseUrl = data?.baseUrl;
        //     const stateCache = this._tokenService.getState();
        //     if(this.authorizeCalback) {
        //         const redirectLoginPath = '/auth/login/redirect';
        //         const redirectUriCallbackLogin = `${baseUrl || this.environment.baseUrlHome}${redirectLoginPath}?callBack=true`;
        //         const codeVerifierCache = this._tokenService.getCodeVerifier();
        //         const codeResponse = this.route.snapshot.queryParamMap.get('code');
        //         const stateRespone = this.route.snapshot.queryParamMap.get('state');
        //         //
        //         // console.log('state', stateRespone, stateCache);

        //         if(stateRespone === stateCache) {
        //             this._authService.connectToken(codeResponse, codeVerifierCache, redirectUriCallbackLogin).subscribe({
        //                 next: (response: ITokenResponse) => {
        //                     this.router.navigate(['/home']);
        //                 },
        //                 error: () => {
				// 			this._tokenService.clearAllCookie();
        //                     this.router.navigate([redirectLoginPath]);
        //                 }
        //             })
        //         } else {
        //             this._authService.connectAuthorize(baseUrl);
        //         }
        //     } else {
        //         this._authService.connectAuthorize(baseUrl);
        //     }
        // })

    }
}
