import { Component, OnInit } from '@angular/core';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { IconPopupConfirm } from '../../shared/consts/base.consts';

@Component({
  selector: 'lib-popup-confirm',
  templateUrl: './popup-confirm.component.html',
})
export class PopupConfirmComponent implements OnInit {

  constructor(
    public ref: DynamicDialogRef, 
    public configDialog: DynamicDialogConfig
  ) { 

  }

  default: any;
  submitted = false;


  showApproveBy: boolean = false;
  acceptStatus: boolean = true;

  DEFAULT_IMAGE = IconPopupConfirm;

  data = {
    title: null,
    icon: null,
  }

  ngOnInit(): void {
    this.data.title = this.configDialog.data.title;
    this.default = this.configDialog.data.icon;
    if(this.default == IconPopupConfirm.ORANGE_ALERT){
      this.data.icon = this.DEFAULT_IMAGE.ORANGE_ALERT;
    } else if(this.default == IconPopupConfirm.RED_ALERT) {
      this.data.icon = this.DEFAULT_IMAGE.RED_ALERT;
    }
  } 

  hideDialog() {
  }

  accept() {
    this.acceptStatus = true;
    this.onAccept();
  }

  cancel() {
    this.acceptStatus = false;
    this.onAccept();
  }

  onAccept() {
    this.ref.close({data: this.data,accept: this.acceptStatus});
  }
}