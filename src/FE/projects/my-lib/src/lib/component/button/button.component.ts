import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActionsConst } from '../../shared/consts/actions.const';

@Component({
  selector: 'lib-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  constructor(
      private ref: ChangeDetectorRef,
  ) { 

  }
  //input
  @Input() type: string ='button';
  @Input() styleClass: string;
  @Input() class: string;
  @Input() isShow: boolean = true;
  @Input() label: string = '';
  @Input() icon: string = '';
  @Input() disabled: boolean = false;
  //output
  @Output() buttonClick: EventEmitter<void> = new EventEmitter<void>();
  ActionsConst = ActionsConst; 

  ngOnInit(): void {
    if(this.label) {
      this.icon = ActionsConst.getIcon(this.label) || ''
    }
  }

  handleButtonClick() {
    this.buttonClick.emit();
  }
}
