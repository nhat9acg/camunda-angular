import { Component, Inject, Input } from '@angular/core';
import ListWebConfig, { PermissionWebConst } from '../../shared/consts/ListWebConfig.const';

@Component({
  selector: 'lib-product-pop-up-top-bar',
  templateUrl: './product-pop-up-top-bar.component.html',
  styleUrls: ['./product-pop-up-top-bar.component.scss']
})
export class ProductPopUpTopBarComponent {

    env: any;

    constructor(
        @Inject('env') environment
    ) {
        this.env = environment;
    }

    @Input() isOpen: boolean = false;
    listWebConfig = ListWebConfig
    permissionWebConst = PermissionWebConst
    private currentOrigin = window.location.origin;
    isCoreActive(): boolean {
        return this.currentOrigin === this.env.baseUrlCore;
    }

    isSalerActive(): boolean {
        return this.currentOrigin === this.env.baseUrlSaler;
    }

    isUserActive(): boolean {
        return this.currentOrigin === this.env.baseUrlUser;
    }

    isInvestActive(): boolean {
        return this.currentOrigin === this.env.baseUrlInvest;
    }

    ngOnInit(): void {} 

}
