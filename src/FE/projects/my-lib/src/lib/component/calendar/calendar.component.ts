import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import * as moment from 'moment';
import Inputmask from 'inputmask';

@Component({
    selector: 'lib-calendar',
    template: `
        <p-calendar
            [styleClass]="'custom-input-disabled custom-calendar custom-style-calendar ' + class"
            [placeholder]="placeholder"
            [inputId]="inputId" [dateFormat]="dateFormat" appendTo="body"
            [(ngModel)]="value"
            [defaultDate]="defaultDate"
            [showIcon]="showIcon"
            [showClear]="showClear"
            [readonlyInput]="readonlyInput"
            [minDate]="minDate" [maxDate]="maxDate"
            [showTime]="showTime"
            [showSeconds]="showSeconds"
            [disabled]="disabled"
            [showButtonBar]="showButtonBar"
            [selectionMode]="selectionMode"
            [style]="styleInline"
            [showWeek]="showWeek"
            [inline]="inline"
            (onSelect)="onChange($event)"
            (onInput)="onInput($event)"
            (onClickOutside)="onClickOutside($event)"
            (onClearClick)="onClear()"
            (onClear)="onClear()"
            [panelStyleClass]="panelStyleClass"
            >
        </p-calendar>
    `
})
export class CalendarComponent implements OnInit {

    constructor(
        private ref: ChangeDetectorRef,
    ) { }

    @Input() placeholder: string = "dd/mm/yyyy";
    @Input() inputFormat: string;
    @Input() class: string;
    @Input() panelStyleClass: string;
    @Input() inputId: string = "calendar-primeng";
    @Input() dateFormat: string = "dd/mm/yy";
    @Input() value: any | any[];
    @Input() showIcon: boolean = true;
    @Input() readonlyInput: boolean = true;
    @Input() minDate: Date = null;
    @Input() maxDate: Date = null;
    @Input() showTime: boolean = false;
    @Input() showSeconds: boolean = false;
    @Input() disabled: boolean = false;
    @Input() showButtonBar: boolean = false;
    @Input() showWeek: boolean = false;
    @Input() inline: boolean = false;
    @Input() selectionMode: string = "single";
    @Input() styleInline: any;
    @Input() isParam: boolean = false;
    @Input() isDataNodeJs: boolean = false;
    @Input() dateFormatOutput: string;
    @Input() defaultDate: Date;
    @Input() showClear: boolean = false;

    @Output() _onChange = new EventEmitter<string|string[]>();
    @Output() valueChange = new EventEmitter<string|string[]>();
    @Output() _onClear = new EventEmitter<string>();
    @Output() _onInput = new EventEmitter<any>();

    modeRange: string = 'range';

    ngOnInit(): void {}

    ngAfterViewInit() {
        if(this.inputFormat) {
            Inputmask('datetime', {
                inputFormat: this.inputFormat,
                alias: 'datetime',
                min: '01/01/1930',
            }).mask(document.getElementById(this.inputId));
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes?.['value'] && changes['value'].currentValue) {
            // NẾU VALUE NHẬN ĐƯỢC LÀ NEW DATE() THÌ CẦN FORMAT VÀ EMIT LẠI VÌ NÓ CÓ TIMEZONE UTC+7
            let currentValue = changes['value'].currentValue;
            if(currentValue instanceof Date || currentValue[0] instanceof Date) {
                this.onChange();
            } else {
                // DỮ LIỆU SAU KHI FORMAT VÀ EMIT THÌ TYPE LÀ STRING KO PHẢI DATE NÊN KHÔNG HIỂN THỊ ĐƯỢC TRONG P-CALENDAR
                // VÀ CẦN FORMAT LẦN NỮA VỀ LẠI DẠNG DATE ĐỂ HIỂN THỊ NHƯNG KHÔNG EMIT
                if(this.selectionMode === this.modeRange) {
                    this.value = currentValue.map(val => new Date(val));
                } else {
                    this.value = new Date(currentValue);
                }
            }
        }

        if(changes?.['minDate'] && changes?.['minDate'].currentValue) {
            this.minDate = new Date(changes['minDate'].currentValue);
        }

        if(changes?.['maxDate'] && changes?.['maxDate'].currentValue) {
            this.maxDate = new Date(changes['maxDate'].currentValue);
        }
        // console.log('changes', changes);
    }

    onChange(event?:Date) {
        let dateFormatOutput = 'YYYY-MM-DDTHH:mm:ss';
        if(this.isParam) dateFormatOutput = 'YYYY-MM-DD';
        let value: any;
        // XỬ LÝ CHỌN KHOẢNG NGÀY
        if(this.selectionMode === this.modeRange) {
            if(this.value[1]) {
                value = this.value.map(e => moment(e).format(dateFormatOutput))
                this.emit(value, event);
            }
        } // XỬ LÝ CHỌN 1 NGÀY
        else {
            value = moment(event || this.value).format(dateFormatOutput);
            this.emit(value, event);
        }
    }

    onInput(event) {
      this._onInput.emit(event);
    }

    onClickOutside(event) {}

    emit(value, event) {
        this.valueChange.emit(value);
        if(event) this._onChange.emit(value);
    }

    onClear() {
        this.valueChange.emit(null);
        this._onClear.emit();
    }
}
