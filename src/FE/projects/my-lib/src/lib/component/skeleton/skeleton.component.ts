import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { TypeSkeleton } from '../../shared/consts/base.consts';

@Component({
  selector: 'lib-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss']
})

export class SkeletonComponent implements OnInit {

    TypeSkeleton = TypeSkeleton;

    @Input() typeLoading: TypeSkeleton;
    @Input() isLoading: boolean;
    @Input() titlePage: string;
    @Input() tabNames: string[];

    isShow: boolean;

    constructor() {}

    ngOnInit(): void {
        this.isShow = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        if(!changes?.['isLoading'].currentValue) {
            setTimeout(() => {
                const skeletonElement = document.getElementsByTagName("e-skeleton");
                skeletonElement[0].parentNode.removeChild(skeletonElement[0]);
                this.isShow = false;
            }, 600);
        }
    }


}
