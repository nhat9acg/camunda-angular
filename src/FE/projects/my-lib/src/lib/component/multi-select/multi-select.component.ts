import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { IDropdown } from '../../shared/interfaces/lib-table.interface';
export const LABEL = 'label';
@Component({
  selector: 'lib-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss']
})
export class MultiSelectComponent implements OnInit {
  @Input()
  public floatLabel: boolean = false;
  @Input()
  public classContainer: string = '';
  @Input()
  public classLabel: string = '';
  @Input()
  public classMultiSelect: string = '';
  @Input()
  public widthMultiSelect: string = '100%';
  @Input()
  public options: IDropdown[] = [];
  @Input()
  public ngModelValue: number[] | string[] | undefined = undefined;
  @Input()
  public showClear: boolean = true;
  @Input()
  public showLabel: boolean = true;
  @Input()
  public label: string = '';
  @Input()
  public placeholder: string = '';
  @Input()
  public isRequired: boolean = false;
  @Input()
  public autoDisplayFirst: boolean = false;
  @Input()
  public isDisabled: boolean = false;
  @Input()
  public filterField: string = LABEL;
  @Input()
  public selectionLimit: number | undefined = undefined;
  @Output()
  public _onChange: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  public ngModelValueChange: EventEmitter<number[] | string[] | undefined> = new EventEmitter<
    number[] | string[] | undefined
  >();

  constructor() {}

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.placeholder = !!this.floatLabel ? this.placeholder || 'Tất cả' : this.placeholder;
  }

  public onChange(event: any) {
    if (event) {
      this.ngModelValueChange.emit(event.value);
      this._onChange.emit(event);
    }
  }

  public get filterBy() {
    return this.filterField === LABEL ? this.filterField : `rawData.${this.filterField}`;
  }

  public onClickClear(event: any) {
    if (event) {
      this.ngModelValueChange.emit(undefined);
      this._onChange.emit(event);
    }
  }

  public get showIconClear() {
    return this.showClear && this.ngModelValue && this.ngModelValue.length;
  }
}
