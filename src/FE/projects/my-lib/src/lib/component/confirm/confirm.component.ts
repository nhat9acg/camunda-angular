import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { IDialogConfirmConfig, IEConfirm } from '../../shared/interfaces/base.interface';
import { IconConfirm } from '../../shared/consts/base.consts';
import { LibHelperService } from '../../shared/services/lib-helper.service';

@Component({
  selector: 'lib-confirm',
  templateUrl: './confirm.component.html',
})
export class ConfirmComponent {
    constructor(
        public ref: DynamicDialogRef,
        public configDialog: DynamicDialogConfig,
        public sanitizer: DomSanitizer,
        private _libHelperService: LibHelperService,
    ) { }

    Object = Object;
    IconConfirm = IconConfirm;
    styleIcon = {'border-radius':'8px', 'width': '60px'};
    dialogData: IDialogConfirmConfig;

    responseData = {
        dropdownReason: null,
        noteReason: '',
        accept: false
    };
    isShowSmall: boolean = false;
    
    ngOnInit(): void {
        let data = this.configDialog.data;
        this.dialogData  = {
            ...data,
            icon: data?.icon || IconConfirm.WARNING,
            labelButton: data?.labelButton || { accept: 'Đồng ý', cancel: 'Hủy'}
        };
    }

    checkValid(){
        this.isShowSmall = false;
        if((this.dialogData?.dropdownData && !this.responseData.dropdownReason) || (this.dialogData?.isNoteConfirm && !this.responseData.noteReason)){
            this.isShowSmall = true;
        } 
    }

    sanitizerUr(src): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(src);
    }

    accept() {
        this.isShowSmall = true;
        if(this.dialogData?.dropdownData){
            if(!this.responseData?.dropdownReason) return this._libHelperService.messageError('Vui lòng chọn lý do');
        }
        if(this.dialogData?.isNoteConfirm){
            if(!this.responseData?.noteReason?.trim()) return this._libHelperService.messageError('Vui lòng nhập lý do');
        }
        this.responseData.accept = true;
        this.ref.close(this.responseData);
    }

    cancel() {
        this.ref.close(this.responseData);
    }
}
