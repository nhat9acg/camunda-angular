import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AccountRoutingModule } from './auth-routing.module';
import { AccountComponent } from './auth.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ToastModule } from 'primeng/toast';
import { AppAuthService } from 'projects/my-lib/src/lib/shared/services/auth/app-auth.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        AccountRoutingModule,
        ButtonModule,
        InputTextModule,
        PasswordModule,
        ToastModule
    ],
    declarations: [
        AccountComponent,
    ],
    providers: [
        AppAuthService,
    ],
})

export class AuthModule {}
