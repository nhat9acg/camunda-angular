import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccountComponent } from './auth.component';
import { LoginComponent } from 'projects/my-lib/src/public-api';
import { environment } from 'projects/shared/environments/environment';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AccountComponent,
                children: [
                    { path: 'login/redirect', component: LoginComponent, data: {baseUrl: environment.baseUrlCore} }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }
