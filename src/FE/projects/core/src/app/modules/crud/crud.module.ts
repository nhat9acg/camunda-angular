import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrudRoutingModule } from './crud-routing.module';
import { DemoComponent } from './components/demo/demo.component';
import { CreateComponent } from './components/demo/create/create.component';
import { DetailComponent } from './components/demo/detail/detail.component';
import { CoreModule } from '../../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';
import { InputTextModule } from 'primeng/inputtext';
import { CrudService } from './services/crud.service';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CrudRoutingModule,
    CoreModule,
    ButtonModule, 
    FileUploadModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    InputTextareaModule,
    MultiSelectModule

  ],
  declarations: [
    DemoComponent,
    CreateComponent,
    DetailComponent,
  ],
  providers: [
    CrudService
  ]
})

export class CrudModule { }
