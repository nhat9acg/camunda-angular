import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ISelectedFileUpload } from 'projects/shared/interfaces/event.interface';
import { CrudService } from '../../services/crud.service';
import { ComponentBase } from 'projects/shared/component-base';

@Component({
    selector: 'src-demo',
    templateUrl: './demo.component.html',
    styleUrls: ['./demo.component.scss']
})
export class DemoComponent extends ComponentBase implements OnInit, OnDestroy {
    constructor(
        injector: Injector,
        private spinner: NgxSpinnerService,
        private fb: FormBuilder,
        private crudService: CrudService,
    ) {
      super(injector)
    }

    fileUploads: File[];
    fileUpload: File;
    postForm: FormGroup;

    createForm() {
      this.postForm = this.fb.group({
        input1: ['input1qqq', Validators.required],
        input2: ['input2', Validators.required],
        group1: this.fb.group({
          input1G1: ['input1G1', Validators.required],
          input2G1: ['input2G1', Validators.required],
        }),
        group2: this.fb.group({
          input1G2: ['inputG2', Validators.required],
          input2G2: ['inputG2', Validators.required],
        }),
        input3: ['input2', Validators.required],
      })
    }

    ngOnInit() {
        this.createForm();
        console.log('postFormValue', this.postForm.value);
        /** spinner starts on init */
        this.spinner.show();

        setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.spinner.hide();
        }, 500000);
    }

    ngOnDestroy() {
      this.spinner.hide();
    }

    uploadImage() {
        const body = {
            images: this.fileUploads
        }
        //
        let formData: FormData = new FormData();
        // formData.append(`images`, this.fileUpload);
        this.fileUploads.forEach((file, index) => {
            formData.append(`images`, file);
        })
        //
        this.crudService.uploadImage(formData).subscribe((response) => {
            console.log('response', response);
        })
    }

    chooseFile(event: ISelectedFileUpload) {
        this.fileUploads = event.currentFiles;
    }

    onSubmit() {
        console.log('postFormValue', this.postForm.value);
    }
}
