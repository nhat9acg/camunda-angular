import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'projects/shared/environments/environment';

@Injectable()
export class CrudService {
    constructor(
        private http: HttpClient
    ) { }

    uploadImage(body) {
        const url = environment.apiPostMedia + '/v1/media'
        return this.http.post(url, body, { params: { postMedia: true }});
    }
}
