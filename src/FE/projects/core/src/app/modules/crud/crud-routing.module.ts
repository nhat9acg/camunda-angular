import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoComponent } from './components/demo/demo.component';
import { CreateComponent } from './components/demo/create/create.component';
import { DetailComponent } from './components/demo/detail/detail.component';

const routes: Routes = [
    {
        path:'demo',
        children: [
            { path: 'list', component: DemoComponent },
            { path: 'create', component: CreateComponent },
            { path: 'detail', component: DetailComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class CrudRoutingModule { }
