import { Component, Injector, OnInit } from '@angular/core';
import { AppMainComponent } from '../main/app.main.component';
import { ComponentBase } from 'projects/shared/component-base';
import { Utils } from 'projects/shared/utils';
import { IMenu } from 'projects/shared/interfaces/menu.interface';

@Component({
	selector: 'app-menu',
	templateUrl: './app.menu.component.html',
})
export class AppMenuComponent extends ComponentBase {
	model: IMenu[];

	constructor(
		injector: Injector,
		public appMain: AppMainComponent
	) {
		super(injector)
	}

	menuCache: any;

	ngOnInit() {
		this._permissionService.getPermissions$.subscribe(() => {
			this.setMenu();
		})
	}

	setMenu() {
		const menuModel: IMenu[] = [
			{ label: 'Tổng quan', icon: 'pi pi-home', routerLink: ['home'] },
			// {
			//   label: 'Component test',
			//   icon: 'pi pi-fw pi-copy',
			//   routerLink: ['crud/demo'],
			//   items: [
			//   { label: 'Danh sách', icon: '', routerLink: ['list'], isShow: true },
			//   { label: 'Thêm mới', icon: '', routerLink: ['create'], isShow: true },
			//   { label: 'Chi tiết', icon: '', routerLink: ['detail'], isShow: true },
			//   ],
			// },

		];

		if(!Utils.compareData(menuModel, this.menuCache)) {
			this.menuCache = JSON.parse(JSON.stringify(menuModel));
			// Hanlde add endPoint Path Url
			this.model = menuModel.map((menu) => {
				// if (menu?.routerLink?.length) menu.routerLink = ['/' + menu.routerLink];
				if (menu?.items?.length) {
					menu.items.forEach((subMenu) => {
						subMenu.routerLink = [
							'/' + menu.routerLink + '/' + subMenu.routerLink,
						];
					});
				}
				return menu;
			});
		}
	}

	onMenuClick() {
		this.appMain.menuClick = true;
	}

	activeSubMenu: boolean = false;
}
