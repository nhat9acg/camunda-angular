import {Component, Injector} from '@angular/core';
import { AppMainComponent } from '../main/app.main.component';
import { IUserLogin } from 'projects/shared/interfaces/user.interface';
import { BaseConsts } from 'projects/shared/consts/base.consts';
import { ComponentBase } from 'projects/shared/component-base';
import { environment } from 'projects/shared/environments/environment';
import { AppAuthService } from 'projects/my-lib/src/lib/shared/services/auth/app-auth.service';

@Component({
    selector: 'app-topbar',
    template: `
        <div class="layout-topbar">
			<div class="layout-topbar-wrapper">
                <div class="layout-topbar-left">
                    <div class="wrapper-logo">
                        <a href="" class="link">
                            <img src="shared/assets/layout/images/topbar/logo-left.svg" class="logo-left"/>
                        </a>
                        <!-- <img (click)="toggleProductPopup(product)"  (clickOutside)="product = false" src="shared/assets/layout/images/topbar/logo-right.svg" class="logo-right"/> -->
                        <lib-product-pop-up-top-bar [isOpen]="product"></lib-product-pop-up-top-bar>
                    </div>
                </div>
                <div class="layout-topbar-right fadeInDown">
                    <ul class="layout-topbar-actions">
                        <li #profile class="topbar-item profile-item" [ngClass]="{'active-topmenuitem': appMain.activeTopbarItem === profile}">
                            <a href="#" (click)="appMain.onTopbarItemClick($event,profile)">
                                <span class="profile-image-wrapper">
                                    <img class="avatar" [src]="getImage(userLogin?.avatarImageUri)" alt="mirage-layout" />
                                </span>
                                <span class="profile-info-wrapper">
                                    <h3 class="profile-info-username">{{ userLogin?.username }}</h3>
                                    <!-- <span>{{ UserManagerConst.getInfo(userLogin?.position)?.name }}</span> -->
                                </span>
                                <i class="pi pi-angle-down ml-3"></i>
                            </a>
                            <ul class="profile-item-submenu fadeInDown">
								<!-- <li style="cursor: pointer; border-bottom: 1px solid #0000001a;" class="layout-submenu-item p-3 pl-5" >
									<i class="pi pi-user icon"></i>
									<div class="menu-text ml-2">
										<p>Tài khoản</p>
									</div>
								</li> -->
								<li style="cursor: pointer;" (click)="logout()" class="layout-submenu-item p-3 pl-5">
									<i class="pi pi-sign-out icon"></i>
									<div class="menu-text ml-2">
										<p>Đăng xuất</p>
									</div>
								</li>
							</ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    `,
	providers: [AppAuthService],
})
export class AppTopBarComponent extends ComponentBase {

    activeItem: number;

    constructor(
        injector: Injector,
		public appMain: AppMainComponent,
		private authService: AppAuthService,
	) {
        super(injector);
    }

    // BaseConsts = BaseConsts;
    // UserManagerConst = UserManagerConst
    userLogin: IUserLogin;
    product: boolean = false;
    env = environment;

    ngOnInit() {
        // this.authService.getUserInfo().subscribe((res) => {
        //     if(this.handleResponseInterceptor(res)) {
        //         this.userLogin = res['data'];
        //     }
        // })

	}

    /**
     * This function returns the URL of an image.
     * If the provided image is falsy, it returns the default avatar.
     * Otherwise, it appends the image to the apiViewImage URL.
     * @param image - The image to retrieve the URL for.
     * @returns The URL of the image.
     */
    getImage(image) {
        if(!image) return BaseConsts.defaultAvatar;
        let apiView = environment?.apiViewImage;

        return apiView + image;
    }

    logout() {
        this.authService.logout().subscribe((res) => {
            this._tokenService.clearToken();
            window.location.href = `${this.env.urlAuthLogin}/authenticate/logout?returnUrl=${this.env.baseUrlCore}`;
        });
    }

    toggleProductPopup(product: boolean) {
        this.product = !product;
    }
}
