import { FormTaskComponent } from "../home/task-list/form-task/form-task.component";
import { ETaskDefinitionKey } from "./camunda.enum";

export class CamundaConst {
    public static readonly componentList = [
        {
            component: FormTaskComponent,
            taskDefinitionKey: ETaskDefinitionKey.FORM_LANGUAGE,
        },
    ];

    public static getComponent(taskDefinitionKey: string) {
		const item = this.componentList.find(item => {
			if (Array.isArray(item.taskDefinitionKey)) {
				return item.taskDefinitionKey.includes(taskDefinitionKey);
			} else {
				return item.taskDefinitionKey === taskDefinitionKey;
			}
		});
		return item ? item?.component : null;
	}
}