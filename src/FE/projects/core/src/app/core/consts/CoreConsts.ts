import { environment } from "projects/shared/environments/environment";
import { BaseConsts } from "projects/shared/consts/base.consts";

export class CoreConsts {
    static readonly redirectUriCallbackLogin = `${environment.baseUrlCore}`//`${environment.baseUrlCore}${BaseConsts.redirectLoginPath}?callBack=true`;
}
