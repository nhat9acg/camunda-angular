import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { HomeComponent } from './home/home.component';
import { CalendarModule } from 'primeng/calendar';
import { MyLibModule } from 'projects/my-lib/src/public-api';
import { AppMainComponent } from './layout/main/app.main.component';
import { AppMenuComponent } from './layout/menu/app.menu.component';
import { AppTopBarComponent } from './layout/top-bar/app.topbar.component';
import { AppMenuitemComponent } from './layout/menu/app.menuitem.component';
import { ToastModule } from 'primeng/toast';
import { SelectButtonModule } from 'primeng/selectbutton';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { environment } from '../../../../shared/environments/environment';
import { RadioButtonModule } from 'primeng/radiobutton';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ButtonModule } from 'primeng/button';
import { NgxEchartsModule } from 'ngx-echarts';
import { DashboardService } from './services/dashboard.service';
import { CamundaRestService } from './services/camunda-rest.service';
import { FormioService } from './services/formio.service';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { FormioModule, FormioAppConfig } from 'angular-formio';
import { StartProcessComponent } from './home/start-process/start-process.component';
import { DropdownModule } from 'primeng/dropdown';
import { ImageModule } from 'primeng/image';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { TabViewModule } from 'primeng/tabview';
import { ConfirmationService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { MenuService } from './layout/menu/app.menu.service';
import { TaskListComponent } from './home/task-list/task-list.component';
import { FormTaskComponent } from './home/task-list/form-task/form-task.component';

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: 'http://localhost:8080',
        realm: 'your-realm',
        clientId: 'your-client-id'
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html'
      }
    });
}

@NgModule({
    imports: [
        CommonModule,
        CoreRoutingModule,
        FormsModule,
        CalendarModule,
        MyLibModule.forRoot(environment),
        ToastModule,
        SelectButtonModule,
        InputTextModule,
        InputTextareaModule,
        InputNumberModule,
        InputSwitchModule,
        ReactiveFormsModule,
        RadioButtonModule,
        BreadcrumbModule,
        NgxSpinnerModule.forRoot(),
        ConfirmDialogModule,
        ButtonModule,
        ButtonModule,
        InputTextModule,
        CalendarModule,
        DropdownModule,
        TabViewModule,
        ImageModule,
        InputTextareaModule,
        InputSwitchModule,
        RadioButtonModule,
        MultiSelectModule,
        TableModule,
        PaginatorModule,
        NgxEchartsModule.forRoot({
            echarts: () => import('echarts')
        }),
        KeycloakAngularModule,
    ],
    declarations: [
        HomeComponent,
        AppMainComponent,
        AppMenuComponent,
        AppTopBarComponent,
        AppMenuitemComponent,
        StartProcessComponent,
        TaskListComponent,
        FormTaskComponent,
    ],
    exports: [
        MyLibModule,
        NgxSpinnerModule,
        FormsModule,
        InputTextModule,
        InputTextareaModule,
        InputNumberModule,
        InputSwitchModule,
        BreadcrumbModule,
        ButtonModule
    ],
    providers: [
      MenuService,
      DialogService,
      DynamicDialogRef,
      DynamicDialogConfig,
      ConfirmationService,
        DashboardService,
        CamundaRestService,
        FormioService,
        // {provide: FormioAppConfig, useValue: AppConfig},
        {
          provide: APP_INITIALIZER,
          useFactory: initializeKeycloak,
          multi: true,
          deps: [KeycloakService]
        }
    ]
})
export class CoreModule { }
