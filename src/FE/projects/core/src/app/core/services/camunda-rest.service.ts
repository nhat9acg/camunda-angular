import { catchError, map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CamundaRestService {
  private engineRestUrl = 'http://localhost:8080/engine-rest/'
  public processDefinitionId: string = null;
  constructor(private http: HttpClient) {
  }

  getTasks(dataFilter): Observable<Object[]> {
    let params = new HttpParams()
    .set('sortBy', 'created')
    .set('sortOrder', 'desc')
    .set('maxResults', '10');
    for(const[key, value] of Object.entries(dataFilter)){
			if(value) params = params.set(key, dataFilter[key]);
		}
  return this.http.get<Object[]>(`${this.engineRestUrl}task`, { params }).pipe(
    tap(() => console.log('Fetched tasks')),
    catchError(this.handleError<Object[]>('getTasks', []))
  );
  }

  getTaskById(taskId: string): Observable<any> {
    const endpoint = `${this.engineRestUrl}task/${taskId}`;
    return this.http.get<any>(endpoint).pipe(
      tap(form => this.log(`fetched task ${taskId}`)),
      catchError(this.handleError('getTaskById', []))
    );
  }

  getUser() {
    const endpoint = `${this.engineRestUrl}user`;
    return this.http.get<any>(endpoint).pipe(
      tap(form => this.log(`getUser `)),
      catchError(this.handleError('getUser', []))
    );
  }
  claimTask(taskId: string, user?: Object): Observable<any> {
    const endpoint = `${this.engineRestUrl}task/${taskId}/claim`;
    return this.http.post<any>(endpoint, user).pipe(
      tap(form => this.log(`claimed task ${taskId}`)),
      catchError(this.handleError('claimTask', []))
    );
  }

  getTaskFormKey(taskId: string): Observable<any> {
    const endpoint = `${this.engineRestUrl}task/${taskId}/form`;
    return this.http.get<any>(endpoint).pipe(
      tap(form => this.log(`fetched taskform`)),
      catchError(this.handleError('getTaskFormKey', []))
    );
  }

  getTaskForm(taskId: string): Observable<any> {
    const endpoint = `${this.engineRestUrl}task/${taskId}/rendered-form`;
    return this.http.get<any>(endpoint, { responseType: 'text' as 'json' }).pipe(
      tap(form => this.log(`getTaskForm`)),
      catchError(this.handleError('getTaskForm', []))
    );
  }

  getVariablesForTask(taskId: string, variableNames: string): Observable<any> {
    const endpoint = `${this.engineRestUrl}task/${taskId}/form-variables?variableNames=${variableNames}`;
    return this.http.get<any>(endpoint).pipe(
      tap(form => this.log(`fetched variables`)),
      catchError(this.handleError('getVariablesForTask', []))
    );
  }

  postCompleteTask(taskId: string, variables: Object): Observable<any> {
    const endpoint = `${this.engineRestUrl}task/${taskId}/complete`;
    return this.http.post<any>(endpoint, variables).pipe(
      tap(tasks => this.log(`posted complete task`)),
      catchError(this.handleError('postCompleteTask', []))
    );
  }

  getVariablesForProcessDefinitionKey(processDefinitionKey, variableNames: string): Observable<any> {
    const url = `${this.engineRestUrl}process-definition/key/${processDefinitionKey}/form-variables?variableNames=${variableNames}`;
    return this.http.get<any>(url).pipe(
      tap(form => this.log(`fetched definition formvariables`)),
      catchError(this.handleError('getVariablesForProcessDefinitionTaskKey', []))
    );
  }

  getProcessDefinitionTaskKey(processDefinitionKey): Observable<any> {
    const url = `${this.engineRestUrl}process-definition/key/${processDefinitionKey}/startForm`;
    return this.http.get<any>(url).pipe(
      tap(form => this.log(`fetched formkey`)),
      catchError(this.handleError('getProcessDeifnitionFormKey', []))
    );
  }

  getProcessDefinitions(): Observable<Object[]> {
    return this.http.get<Object[]>(this.engineRestUrl + 'process-definition?latestVersion=true').pipe(
      tap(processDefinitions => this.log(`fetched processDefinitions`)),
      catchError(this.handleError('getProcessDefinitions', []))
    );
  }

  postProcessInstance(processDefinitionKey, variables?): Observable<any> {
    const endpoint = `${this.engineRestUrl}process-definition/key/${processDefinitionKey}/start`;
    return this.http.post<any>(endpoint, variables).pipe(
      tap(processDefinitions => this.log(`posted process instance`)),
      catchError(this.handleError('postProcessInstance', []))
    );
  }

  deployProcess(fileToUpload: File): Observable<any> {
    const endpoint = `${this.engineRestUrl}deployment/create`;
    const formData = new FormData();

    formData.append('fileKey', fileToUpload, fileToUpload.name);

    return this.http.post(endpoint, formData);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
