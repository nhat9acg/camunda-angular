import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'projects/shared/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class DashboardService {
	constructor(private http: HttpClient) {}

	private apiUrl = `${environment.api}/api/core/dashboard`;

	getInfoDashboard(): Observable<any>{
		return this.http.get(`${this.apiUrl}/find-all`);
	}
}
