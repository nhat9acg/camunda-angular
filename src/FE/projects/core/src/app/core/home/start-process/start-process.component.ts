import { Component, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentBase } from 'projects/shared/component-base';
import { CamundaRestService } from '../../services/camunda-rest.service';
import { IAction, IColumn } from 'projects/shared/interfaces/lib-table.interface';
import { DataTableEmit } from 'projects/shared/models/lib-table.model';
import { ETableColumnType, ETableFrozen } from 'projects/shared/consts/e-table.consts';
import { SuccessMessage } from 'projects/shared/consts/success-message.const';

@Component({
  selector: 'app-start-process',
  templateUrl: './start-process.component.html',
  styleUrls: ['./start-process.component.scss']
})
export class StartProcessComponent extends ComponentBase {
  processDefinitionKey: string = null;
   formKey: string = '';
  private rootViewContainer = null;

  constructor(
    injector: Injector,
    private routeActive: ActivatedRoute,
    private router: Router,
    public camundaRestService: CamundaRestService
  ) {
    super(injector);
  }

  listAction: IAction[][] = [];
	columns: IColumn[] = [];
	rows: Object[] = [];
	dataTableEmit = new DataTableEmit();
  dataFilter: Object

  ngOnInit() {
    this.columns = [
			{
				field: 'id',
				header: 'ID',
        action: (row) => this.view(row),
				width: 5,
				isFrozen: true,
				alignFrozen: ETableFrozen.LEFT,
				class: 'b-border-frozen-left',
        otherType: ETableColumnType.LINK,
				isPin: true,
			},
      {
				field: 'key',
				header: 'Key',
				minWidth: 8,
				isPin: true,
			},
			{
				field: 'name',
				header: 'Tên',
				minWidth: 8,
				isPin: true,
			},
			{
				field: 'category',
				header: 'Loại',
				minWidth: 8,
				isPin: true,
			},
      {
				field: 'description',
				header: 'Mô tả',
				minWidth: 8,
				isPin: true,
			},
      {
        field: 'claim', header: 'Start process', width: 12, type: ETableColumnType.ACTION_BUTTON,
        action: (row) => this.postProcessInstance(row), icon: 'pi pi-download', isPin: true,
        class: 'justify-content-center', classButton: 'btn-action-table',
      },
			{
				field: '',
				header: '',
				width: 3,
				displaySettingColumn: false,
				isFrozen: true,
				alignFrozen: ETableFrozen.RIGHT,
				type: ETableColumnType.ACTION_DROPDOWN,
			},
		];
    this.setPage()
  }

  view(row) {
    const queryParams = {
      id: row?.id,
  };
  this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  this.router.onSameUrlNavigation = 'reload';
  this.router.navigate([], {
      relativeTo: this.routeActive,
      queryParams: queryParams,
      queryParamsHandling: 'merge',
  });

    // this.camundaRestService.processDefinitionId = row?.id
  }

  setPage() {
    this.isLoading = true;
		this.camundaRestService.getProcessDefinitions()
			.subscribe(
        {
          next: (res) => {
            this.isLoading = false;
            this.rows = res
            this.page.totalItems = res?.length
          },
          error: () => {
              this.isLoading = false;
          }
      });
  }
  userList: Object[] = []
  getUserList() {
    this.isLoading = true;
		this.camundaRestService.getUser()
			.subscribe({
        next: (res) => {
          this.isLoading = false;
          this.userList = res
        },
        error: () => {
            this.isLoading = false;
        }
    });
  }

  postProcessInstance(row) {
    this.isLoading = true;
		this.camundaRestService.postProcessInstance(row?.key, {})
			.subscribe({
        next: (res) => {
          this.isLoading = false;
          this.messageSuccess(SuccessMessage.SUC_UPDATE);
          this.setPage()
        },
        error: () => {
            this.isLoading = false;
        }
    });
  }
}
