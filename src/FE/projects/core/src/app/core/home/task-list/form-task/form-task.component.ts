import { Component, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentBase } from 'projects/shared/component-base';
import { CamundaRestService } from '../../../services/camunda-rest.service';
import { IAction, IColumn } from 'projects/shared/interfaces/lib-table.interface';
import { DataTableEmit } from 'projects/shared/models/lib-table.model';
import { EFormElements } from 'projects/shared/enums/form-elements.enum';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FieldDetails, FormInputList } from 'projects/shared/interfaces/form-input-list.interface';
import { required } from 'projects/shared/validators/validator-common';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Utils } from 'projects/shared/utils';
import { ErrorMessage } from 'projects/shared/consts/error-code.const';
import { SuccessMessage } from 'projects/shared/consts/success-message.const';

@Component({
  selector: 'app-form-task',
  templateUrl: './form-task.component.html',
  styleUrls: ['./form-task.component.scss']
})
export class FormTaskComponent extends ComponentBase {
  processDefinitionKey: string = null;
  formKey: string = '';
  private rootViewContainer = null;

  constructor(
    injector: Injector,
    private route: ActivatedRoute,
    private router: Router,
    private camundaRestService: CamundaRestService,
    private fb: FormBuilder,
    public configDialog: DynamicDialogConfig,
    public ref: DynamicDialogRef,
  ) {
    super(injector);
  }

  listAction: IAction[][] = [];
  columns: IColumn[] = [];
  rows: Object[] = [];
  dataTableEmit = new DataTableEmit();
  EFormElements = EFormElements
  postForm: FormGroup;
  options = [
    {
      code: 'java',
      name: 'Java'
    },
    {
      code: 'csharp',
      name: 'C#'
    },
  ]
  formInputs: FormInputList[] = [
    {
      title: "",
      inputs: [
        {
          label: '', controlName: 'id', type: null, col: 0, required: false,
          defaultValue: null, defaultValuePath: 'id'
        },
        {
          label: 'Chọn ngôn ngữ', controlName: 'language', type: EFormElements.DROPDOWN, col: 12, required: true,
          customValidator: [required()], optionLabel: 'name', optionValue: 'code', options: this.options,
          placeholder: 'Chọn ',
          defaultValue: null, defaultValuePath: 'language'
        },
        // { label: 'Bỏ qua', controlName: 'isSkip', type: EFormElements.INPUT_SWITCH, col: 3, defaultValue: false },
      ],
    },
  ]
  ngOnInit() {
    this.setForm(this.configDialog?.data?.item);
    if(this.configDialog?.data?.isSkip) {
      this.disableControls();
    }
  }

  onChange(event, controlName) {
    const actions = {
      isSkip: () => {
        this.disableControls();
      },
    };

    if (event && actions[controlName]) {
      actions[controlName]();
    }
  }

  private disableControls(controlNamesToDisable: string[] = []): void {
    const allControlNames = Object.keys(this.postForm?.controls);
    if (controlNamesToDisable.length === 0) {
      // Nếu mảng trường rỗng, vô hiệu hóa tất cả các trường
      for (const controlName of allControlNames) {
        const control = this.postForm.get(controlName);
        control.disable();
      }
    } else {
      // Nếu có mảng trường được truyền vào, vô hiệu hóa chỉ các trường trong mảng đó
      for (const controlName of controlNamesToDisable) {
        if (allControlNames.includes(controlName)) {
          const control = this.postForm.get(controlName);
          control.disable();
        }
      }
    }
  }

  private setForm(itemDetail?) {
    const formGroupConfig = {};
    this.formInputs.forEach((section) => {
      section.inputs.forEach((input) => {
        const controlName = input?.controlName;
        if (controlName?.length > 0) {
          let controlValue = itemDetail ? this.getPropertyObj(itemDetail, input?.defaultValuePath, input?.type) || null : null;
          if (input?.defaultValue !== undefined && !controlValue) {
            controlValue = input?.defaultValue;
          }

          const validators = input?.customValidator ? input?.customValidator : [];
          formGroupConfig[controlName] = [controlValue, validators];
        }
      });
    });
    this.postForm = this.fb.group(formGroupConfig);
  }
  getPlaceholder = (input: FieldDetails) => {
    if (!input?.placeholder) return '';

    let text = '';
    if ([EFormElements.INPUT, EFormElements.TEXTAREA, EFormElements.MULTI_SELECT].includes(input?.type)) {
      text = 'Nhập ';
    } else if ([EFormElements.DROPDOWN, EFormElements.CALENDAR].includes(input?.type)) {
      text = 'Chọn ';
    }

    if (input.placeholder !== text) {
      return input.placeholder;
    } else {
      return input.placeholder + (input.label ? input.label.toLowerCase() : '');
    }
  }

  onSubmit() {
    Utils.trimFormGroup(this.postForm);
    if (this.checkInValidForm(this.postForm)) {
      this.messageError(ErrorMessage.ERR_INVALID_FORM)
    } else {
      this.isLoading = true;
      const formValues = this.postForm.value;
      // const variables = {
      //   variables: {
      //     language: {
      //       value: this.postForm.value.language
      //     },
      //   }
      // }
      const variables = {
        variables: Object.keys(formValues).reduce((acc, key) => {
          acc[key] = { value: formValues[key] };
          return acc;
        }, {})
      };
      this.camundaRestService.postCompleteTask(this.postForm.value.id, variables)
        .subscribe({
          next: (res) => {
            this.isLoading = false;
            this.messageSuccess(SuccessMessage.SUC_UPDATE);
            this.ref.close(true)
          },
          error: () => {
            this.isLoading = false;
          }
        });
    }
  }
}

