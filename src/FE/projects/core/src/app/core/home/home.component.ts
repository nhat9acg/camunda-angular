import { ChangeDetectorRef, Component, HostListener, Injector, ViewChild } from '@angular/core';
import { ComponentBase } from 'projects/shared/component-base';
import { BreadcrumbService } from 'projects/my-lib/src/public-api';
import { Location } from '@angular/common';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { TabView } from 'primeng/tabview';
import { CamundaRestService } from '../services/camunda-rest.service';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent extends ComponentBase {
  constructor(
    injector: Injector,
    public ref: DynamicDialogRef,
    private breadcrumbService: BreadcrumbService,
    public camundaRestService: CamundaRestService,
    private detectorRef: ChangeDetectorRef,
    private _location: Location,
    private routeActive: ActivatedRoute,
  ) {
    super(injector);
    const breadcrumbItems: { label: string; routerLink?: string[] }[] = [
      { label: 'Trang chủ', routerLink: ['/home'] },
    ];

    this.breadcrumbService.setItems(breadcrumbItems);
  };
  recipient
  isSave: boolean = false;
  tabViewActive: {
    startProcess: boolean;
    taskList: boolean;
  } = {
      startProcess: true,
      taskList: false,
  };

  @ViewChild(TabView) tabView: TabView;

  ngOnInit(): void {
    if(this.routeActive.snapshot.queryParamMap.get('id')) {
      this.camundaRestService.processDefinitionId = this.routeActive.snapshot.queryParamMap.get('id')
      this.tabViewActive['taskList'] = true;
      const index = Object.keys(this.tabViewActive).findIndex(
          (key) => key === 'taskList'
      );
      this.activeIndex = index;
    }
    // if(this.routeActive.snapshot.queryParamMap.get('task-list')) {
    //   this.tabViewActive['taskList'] = true;
    //   const index = Object.keys(this.tabViewActive).findIndex(
    //       (key) => key === 'taskList'
    //   );
    //   this.activeIndex = index;
    // }

  }

  tabViewActiveRequest = {
    startProcess: true,
    taskList: true,
  };
  selectedTabRequest: number = 0;
  changeTab(event: any) {
		const tabIndex = event.index;
        let tabHeader = this.tabView.tabs[tabIndex].header;
        this.tabViewActive[tabHeader] = true;
        this.detectorRef.detectChanges();

		let tabViewActiveRequest: string[] = [];
		for(let [key, value] of Object.entries(this.tabViewActiveRequest)) {
			if(value) tabViewActiveRequest.push(key);
		}
		let selectedTabRequest: number = tabViewActiveRequest.findIndex(tabName => tabName === tabHeader) || 0;
		this.selectedTabRequest = selectedTabRequest >= 0 ? selectedTabRequest : 0;
		let params = new HttpParams();
		params = params.append('selectedTab', tabIndex);
		this._location.go(window.location.pathname+`?${params.toString()}`);
    }
}
