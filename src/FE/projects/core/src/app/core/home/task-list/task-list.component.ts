import { Component, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentBase } from 'projects/shared/component-base';
import { CamundaRestService } from '../../services/camunda-rest.service';
import { IAction, IColumn } from 'projects/shared/interfaces/lib-table.interface';
import { DataTableEmit } from 'projects/shared/models/lib-table.model';
import { ETableColumnType, ETableFrozen } from 'projects/shared/consts/e-table.consts';
import { DialogService } from 'primeng/dynamicdialog';
import { FormTaskComponent } from './form-task/form-task.component';
import { SuccessMessage } from 'projects/shared/consts/success-message.const';
import { CamundaConst } from '../../consts/camunda.const';
import { EFormatDateDisplay } from 'projects/shared/consts/base.consts';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent extends ComponentBase {
  processDefinitionKey: string = null;
   formKey: string = '';
  private rootViewContainer = null;

  constructor(
    injector: Injector,
    private router: Router,
    private _dialogService: DialogService,
    private camundaRestService: CamundaRestService,
    private routeActive: ActivatedRoute
  ) {
    super(injector);
  }

  listAction: IAction[][] = [];
	columns: IColumn[] = [];
	rows: Object[] = [];
	dataTableEmit = new DataTableEmit();


  ngOnInit() {
   
    this.columns = [
			{
				field: 'id',
				header: 'ID',
        otherType: ETableColumnType.LINK,
				width: 5,
        action: (row) => this.view(row),
				isFrozen: true,
				alignFrozen: ETableFrozen.LEFT,
				class: 'b-border-frozen-left',
				isPin: true,
			},
			{
				field: 'name',
				header: 'Tên nhiệm vụ',
				minWidth: 8,
				isPin: true,
			},
			{
				field: 'created',
				header: 'Ngày tạo',
        type: EFormatDateDisplay.DATE_DMY,
				minWidth: 8,
				isPin: true,
			},
      {
				field: 'assignee',
				header: 'Người nhận',
				minWidth: 8,
				isPin: true,
			},
      {
        field: 'claim', header: 'Nhận task', width: 12, type: ETableColumnType.ACTION_BUTTON,
        action: (row) => this.claimTask(row?.id), icon: 'pi pi-download', isPin: true,
        class: 'justify-content-center', classButton: 'btn-action-table',
      },
			{
				field: '',
				header: '',
				width: 3,
				displaySettingColumn: false,
				isFrozen: true,
				alignFrozen: ETableFrozen.RIGHT,
				type: ETableColumnType.ACTION_DROPDOWN,
			},
		];
    this.setPage()
    this.getUserList()
  }
  CamundaConst = CamundaConst
  view(row): void {
    let component = CamundaConst.getComponent(row?.taskDefinitionKey)
    if(component) {
      const dialogConfig = {
        component: component,
        header: 'Thực hiện task',
        data:  {item: row},
      }
      this.openDialog(dialogConfig);
    } else {
      const dialogConfig = {
        component: FormTaskComponent,
        header: 'Xác nhận hoàn thành',
        data:  {item: row, isSkip: true},
      }
      this.openDialog(dialogConfig);
    }
  }

	openDialog(dialogConfig) {
		const ref = this._dialogService.open(dialogConfig?.component, {
			header: dialogConfig?.header,
			width: '900px',
			data: dialogConfig?.data
		});

		ref.onClose.subscribe(result => {
			if (result) {
				this.setPage();
			}
		});
	}
  dataFilter: Object = new Object()
  setPage() {
    this.isLoading = true;
    this.dataFilter = {
      processDefinitionId : this.camundaRestService.processDefinitionId,
    }
		this.camundaRestService.getTasks(this.dataFilter)
			.subscribe(
        {
          next: (res) => {
            this.isLoading = false;
            this.rows = res
            this.page.totalItems = res?.length
          },
          error: () => {
              this.isLoading = false;
          }
      });
  }
  userList: Object[] = []
  getUserList() {
    this.isLoading = true;
		this.camundaRestService.getUser()
			.subscribe({
        next: (res) => {
          this.isLoading = false;
          this.userList = res
        },
        error: () => {
            this.isLoading = false;
        }
    });
  }

  claimTask(id: string, user?: Object) {
    this.isLoading = true;
		this.camundaRestService.claimTask(id, {userId : this.userList[0]?.['id']})
			.subscribe({
        next: (res) => {
          this.isLoading = false;
          this.messageSuccess(SuccessMessage.SUC_UPDATE);
          this.setPage()
        },
        error: () => {
            this.isLoading = false;
        }
    });
  }
}

