import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

// Application Components
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Application services
import { HanldeHttpInterceptor } from '../../../shared/providers/hanlde.interceptor';
import { MenuService } from './core/layout/menu/app.menu.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { MarkdownModule } from 'ngx-markdown';
import { MyLibModule } from 'projects/my-lib/src/public-api';
import { environment } from '../../../shared/environments/environment';
import { NgxEchartsModule } from 'ngx-echarts';
import { GlobalErrorHandler } from 'projects/shared/providers/global-error-hanlder';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        MarkdownModule.forRoot(),
        MyLibModule.forRoot(environment),
        NgxEchartsModule.forRoot({
            echarts: () => import('echarts')
        }),
    ],
    declarations: [
        AppComponent,
    ],
    providers: [
		{ provide: HTTP_INTERCEPTORS, useClass: HanldeHttpInterceptor, multi: true },
		{ provide: ErrorHandler, useClass: GlobalErrorHandler },
		//
		MenuService,
		MessageService,
		DialogService,
		ConfirmationService,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
