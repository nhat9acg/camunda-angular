import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMainComponent } from './core/layout/main/app.main.component';
import { AuthGuard } from '../../../shared/guard/auth.guard';
import { PageNotFoundComponent } from 'projects/my-lib/src/public-api';
import { PermissionGuard } from 'projects/shared/guard/permission.guard';
import { WebKeys } from 'projects/shared/consts/permissionWeb/permission.enum';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    {
        path: 'auth',
        canActivate: [AuthGuard],
        loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
    },
    {
        path:'',
        component: AppMainComponent,
		canActivate: [AuthGuard, PermissionGuard],
        data: {
			permissionInWeb: WebKeys.Core,
		},
        children: [
            {
                path: '',
                loadChildren: () => import('./core/core.module').then(m => m.CoreModule),
            },
            {
                path: 'crud',
                loadChildren: () => import('./modules/crud/crud.module').then(m => m.CrudModule),
            },
        ]
    },

    { path: '**', component: PageNotFoundComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule { }
