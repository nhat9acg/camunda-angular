import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { TokenService } from "../../my-lib/src/lib/shared/services/auth/token.service";
import { Observable } from "rxjs";
import { BaseConsts } from "projects/shared/consts/base.consts";
import { AppAuthService } from "projects/my-lib/src/lib/shared/services/auth/app-auth.service";
import { environment } from "../environments/environment";
import { PermissionsService } from "../services/permission/permissions.service";
import { MessageService } from "primeng/api";
import { Utils } from "../utils";
import jwtDecode from "jwt-decode";
import { isChangePasswordTempStorage } from "../consts/local-storage-name.const";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {
    constructor(
        private _tokenService: TokenService,
        private _authService: AppAuthService,
        private router: Router,
        private _permissionService: PermissionsService,
        private messageService: MessageService,
    ) {}

	activeRoute: boolean;
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return new Promise<boolean>((resolve) => {
		const routerRequestChangePassword = '/auth/change-password/requestChange';
		const checkPermission = () => {
			//
			this._permissionService.getPermissions$.subscribe({
				next: (permissions) => {
					this._permissionService.activeCheckPermission(permissions);
					if(!route.data['permissions'] || state?.url === '/home') {
						this.activeRoute = (state?.url === '/home');
						return resolve(true);
					}
					if(permissions) {
						let permissionsKeys: string[] = (typeof route.data['permissions'] === 'string') ? [route.data['permissions']] : route.data['permissions'];
						if(Array.isArray(permissions)) {
							for(let key of permissionsKeys) {
								if(permissions.includes(key)) {
									this.activeRoute = true;
									return resolve(true);
								}
							}
						}
						//
						if(!this.activeRoute) location.href = '/home';
						this.messageService.add({ severity: 'error', detail: 'Tài khoản không có quyền truy cập' });
						return resolve(false);
					}
				},
				error: () => {
					this.messageService.add({ severity: 'error', detail: 'Có lỗi xảy ra vui lòng thử lại sau!' });
					return resolve(false);
				}
			});
		}
			  //
        const updateToken = () => {
            this._authService.refreshToken().subscribe({
                next: () => {
                    checkPermission();
                    resolve(true);
                },
                error: (err) => {
                    this._tokenService.clearToken();
                    this.router.navigate([BaseConsts.redirectLoginPath]);
                }
            });
        }

        const checkRequestChangePasswordFirstLogin = () => {
          return resolve(true);
            // this._authService.getUserInfo().subscribe({
            //     next: (res: any) =>  {
            //         if(res.data?.isPasswordTemp) {
            //             const urlRequestChangePassword = environment.baseUrlHome + routerRequestChangePassword;
            //             if(window.location.href !== urlRequestChangePassword) {
            //                 window.location.href = urlRequestChangePassword;
            //                 return resolve(false);
            //             }
            //         } else {
            //             Utils.setLocalStorage(`${isChangePasswordTempStorage}${res.data?.id}`, true);
            //         }
            //         return resolve(true);
            //     },
            //     //
            //     error: () => {
            //         return resolve(true);
            //     }
            // })
        }

        const token = this._tokenService.getToken();
        const refreshToken = this._tokenService.getRefreshToken();
        if(token) {
            // Nếu đã có token thì tự động redirect về home khi truy cập trang login
            const tokenDecode: any = jwtDecode(token);
            if(!Utils.getLocalStorage(`${isChangePasswordTempStorage}${tokenDecode?.user_id}`)) {
                checkRequestChangePasswordFirstLogin();
            } else if(state.url === routerRequestChangePassword) {
                this.router.navigate(['/home']);
                return resolve(false);
            }
            if(state.url === BaseConsts.redirectLoginPath) this.router.navigate(['/home']);
			checkPermission();
        } else {
                if(refreshToken) {
                    updateToken();
                } else {
                  if(state.url.includes(BaseConsts.redirectLoginPath))  {
                        return resolve(true);
                    }
                    //
                    this.router.navigate([BaseConsts.redirectLoginPath]);
                }
            }
        });
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.canActivate(route, state);
    }
}
