import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { PermissionsService } from "../services/permission/permissions.service";
import { TokenService } from "projects/my-lib/src/lib/shared/services/auth/token.service";
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard {
    constructor(
        private _permissionService: PermissionsService,
		private _tokenService: TokenService,
    ) {}

	env = environment;

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
      return new Promise<boolean>((resolve) => {
        return resolve(true)
      })

        // return new Promise<boolean>((resolve) => {
        //     let permissionInWeb: number = route.data?.['permissionInWeb'];
        //     //
        //     this._permissionService.getPermissionInWeb(permissionInWeb).subscribe({
        //         next: (response: any) => {
        //             const permissions = response?.data;
        //             this._permissionService.setPermissions(permissions);
        //             return resolve(true)
        //         },
        //         error: () => {
				// 	this._tokenService.clearToken();
				// 	window.location.href = `${this.env.urlAuthLogin}/authenticate/logout?returnUrl=${window.location.origin}`;
        //             return resolve(false);
        //         }
        //     });
        // })
    }
}
