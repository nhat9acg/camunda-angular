import { Injectable } from "@angular/core";
import { Utils } from "../utils";
import { NavigationCancelRouterLinkStorage } from "../consts/local-storage-name.const";

@Injectable({
  providedIn: 'root'
})
export class CancelNavigation {
    constructor() {}

    permissions = [];

    canDeactivate(): boolean {
        if(!!Utils.getLocalStorage(NavigationCancelRouterLinkStorage)) {
            return false;
        }
        return true;
    }
}
