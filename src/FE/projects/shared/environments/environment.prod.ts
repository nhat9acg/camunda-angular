// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: true,
    api: 'https://beapi.meeyfinance.com',
    urlAuthLogin: 'https://id.meeyfinance.com',
    apiPostMedia: 'https://api.meeymedia.com',
    apiViewMediaTemp: 'https://file.meeymedia.com',
    apiViewImage: 'https://io.meeymedia.com',
    apiViewVideo: 'https://v.meeymedia.com',
    apiNotificationNodeJs: 'https://beapi.meeyfinance.com',
    apiResizeImage: 'https://i.meeymedia.com',
	domain: 'meeyfinance.com',
    baseUrlHome: 'https://home.meeyfinance.com',
    baseUrlUser: 'https://user.meeyfinance.com',
    baseUrlCore: 'https://core.meeyfinance.com',
    baseUrlInvest: 'https://invest.meeyfinance.com',
    baseUrlSaler: 'https://saler.meeyfinance.com',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
