// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    api: 'https://staging-beapi.meeyfinance.com',
    urlAuthLogin: 'https://staging-id.meeyfinance.com',
    apiPostMedia: 'https://test-api.meeymedia.com',
    apiViewMediaTemp: 'https://file.meeymedia.com',
    apiViewImage: 'https://io.meeymedia.com',
    apiViewVideo: 'https://v.meeymedia.com',
    apiNotificationNodeJs: 'https://staging-beapi.meeyfinance.com',
    apiResizeImage: 'https://i.meeymedia.com',
	domain: 'meeyfinance.com',
    baseUrlHome: 'https://staging-home.meeyfinance.com',
    baseUrlUser: 'https://staging-user.meeyfinance.com',
    baseUrlCore: 'https://staging-core.meeyfinance.com',
    baseUrlInvest: 'https://staging-invest.meeyfinance.com',
    baseUrlSaler: 'https://staging-saler.meeyfinance.com',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
