// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    api: 'https://beapi-meeyfinance.meey.dev',
    urlAuthLogin: 'https://id-meeyfinance.meey.dev',
    apiPostMedia: 'https://test-api.meeymedia.com',
    apiViewMediaTemp: 'https://file.meeymedia.com',
    apiViewImage: 'https://io.meeymedia.com',
    apiViewVideo: 'https://v.meeymedia.com',
    apiNotificationNodeJs: 'https://beapi-meeyfinance.meey.dev',
    apiResizeImage: 'https://i.meeymedia.com',
	  domain: 'meey.dev',
    baseUrlHome: 'https://home-meeyfinance.meey.dev',
    baseUrlUser: 'https://user-meeyfinance.meey.dev',
    baseUrlCore: 'https://core-meeyfinance.meey.dev',
    baseUrlInvest: 'https://invest-meeyfinance.meey.dev',
    baseUrlSaler: 'https://saler-meeyfinance.meey.dev',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  //v Included with Angular CLI.
