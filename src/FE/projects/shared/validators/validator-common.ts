import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import moment from 'moment';
import { EFormatDateDisplay } from '../consts/base.consts';
import { ErrorMessage } from '../consts/error-code.const';
import { Utils } from '../utils';
import { IValidRangeNumber } from '../interfaces/validator.interface';
export const messageRequired = 'Trường bắt buộc nhập';

// Validator cho số
export function numberValidator(): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		const number = /[0-9]+/;
		if ((control.value && !control.value.toString().trim()) || !control.value) {
			return { errorMessage: messageRequired };
		}
		//
		if (!number.test(control.value)) {
			return { errorMessage: 'Giá trị phải là số' };
		}

		return null;
	};
}

export function fullNameValidator(): ValidatorFn {
	// Điều chỉnh biểu thức chính quy nếu cần thiết
	const fullNamePattern: RegExp = /^[\p{L}\s'"\-]+/u;
  
	return (control: AbstractControl): { [key: string]: any } | null => {
	  if ((control.value && !control.value.toString().trim()) || !control.value) {
		return {  errorMessage: messageRequired };
	  }
  
	  if (!fullNamePattern.test(control.value)) {
		return { errorMessage: 'Giá trị không hợp lệ' };
	  }
  
	  return null;
	};
  }



export function arrayRequiredValidator(): ValidatorFn {
	return (control: AbstractControl): ValidationErrors | null => {
	  if (control.value && Array.isArray(control.value) && control.value.length > 0) {
		return null; // Array is not empty, validation passes
	  } else {
		return { errorMessage: ErrorMessage.ERR_ARRAY_INVALID };
	  }
	};
  }

// Validator cho chuỗi
export function required(isRequired: boolean = true, maxLength?: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        if (isRequired) {
            if ((['number', 'string'].includes(typeof control?.value) && !control.value.toString().trim()) || (!control?.value && control?.value !== 0) ) {
                return { errorMessage: messageRequired };
            }
        }
        if (maxLength && control.value && control.value.toString().length > maxLength) {
            return { errorMessage: `Độ dài tối đa là ${maxLength}` };
        }
        return null;
    };
}

export function positiveNumber(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        if (control.value === 0) {
            return { errorMessage: `Chỉ nhận giá trị > 0` };
        }
        return null;
    };
}

export function rangeNumberValidator(params: IValidRangeNumber): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
        if (params?.min && control.value && control.value < params?.min) {
            return { errorRangeNumberMessage: params?.minMessage || `${params?.field} chưa đạt tối thiểu là ${Utils.formatCurrency(params?.min)} ${params?.unit || ''}` };
        } else if (params?.max && control.value && control.value > params?.max) {
            return { errorRangeNumberMessage: params?.maxMessage || `${params?.field} vượt quá tối đa là ${Utils.formatCurrency(params?.max)} ${params?.unit || ''}` };
        }
        return null;
    };
}

export function emailValidator(isRequired: boolean = true): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (isRequired && !control.value) {
      return { errorMessage: messageRequired };
    }

    if (control.value && !emailPattern.test(control.value)) {
      return { errorMessage: ErrorMessage.ERR_EMAIL_INVALID };
    }
    return null;
  };
}

// Validator cho số điện thoại
export function phoneValidator(isRequired: boolean = true): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
	const phoneNumber = Utils.convertToVietNamesePhone(control?.value)
    const phonePattern =
      /^(091|094|088|083|084|085|081|082|096|098|033|086|097|032|034|035|036|037|038|039|089|090|093|070|079|077|076|078)\d{7}$/;

	if (isRequired && !phoneNumber) {
      return { errorMessage: messageRequired };
    }

    if (phoneNumber && !phonePattern.test(phoneNumber)) {
      return { errorMessage: ErrorMessage.ERR_PHONE_INVALID };
    } else if(phoneNumber && control.value !== phoneNumber) {
		control.setValue(Utils.convertToVietNamesePhone(phoneNumber))
	}

    return null;
  };
}

// Validator cho trường dựa trên enum
export function enumValidator(enumType: any): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		const value = control.value;

		// Kiểm tra xem giá trị thuộc về enum hay không
		if (Object.values(enumType).includes(value)) {
			return null;
		} else {
			return { errorMessage: 'Giá trị không hợp lệ' }; //
		}
	};
}

// Validator cho trường date với yêu cầu bắt buộc và kiểm tra khoảng ngày
export function dateRangeValidator(minDate: Date | null, maxDate: Date | null, isRequired: boolean = true): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		if (isRequired && Validators.required(control)) {
			return { errorMessage: messageRequired };
		}

		const selectedDate = new Date(control.value);

		if (isNaN(selectedDate.getTime())) {
			return { errorMessage: 'Giá trị không hợp lệ' };
		}

		if (minDate && selectedDate < minDate) {
			return { errorMessage: `Giá trị phải hơn ${formatCalendar(minDate)}` };
		}

		if (maxDate && selectedDate > maxDate) {
			return { errorMessage: `Giá trị nhỏ hơn ${formatCalendar(maxDate)}` };
		}

		return null;
	};
}

export function formatCalendar(datetime: Date) {
	if (!(datetime instanceof Date)) {
		return null;
	}
	return moment(datetime, EFormatDateDisplay.DATE).format(
		EFormatDateDisplay.DATE
	);
}

// Validator password
export function passwordValidator(isRequired: boolean = true): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		if (isRequired) {
			if (!control?.value) {
				return { errorMessage: messageRequired};
			}
			if (control?.value?.length < 8) {
				return { errorMessage: 'Mật khẩu chứa ít nhất 8 ký tự' };
			  }
			// Kiểm tra xem mật khẩu có ít nhất một chữ viết hoa
			if (!/[A-Z]/.test(control?.value)) {
				return { errorMessage: 'Mật khẩu phải có ít nhất một chữ viết hoa' };
			}

			// Kiểm tra xem mật khẩu có ít nhất một ký tự đặc biệt
			if (!/[\W_]/.test(control?.value)) {
				return { errorMessage: 'Mật khẩu phải có ít nhất một ký tự đặc biệt' };
			}

			// Kiểm tra xem mật khẩu có ít nhất một số
			if (!/\d/.test(control?.value)) {
				return { errorMessage: 'Mật khẩu phải có ít nhất một số' };
			}
		}
	  	return null;
	};
}

export function usernameValidator(): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		const usernamePattern = /^[a-zA-Z0-9_-]{3,30}$/;
		if (control.value && !usernamePattern.test(control.value)) {
		  return { errorMessage: "Tên đăng nhập tối thiểu 3 ký tự bao gồm các chữ cái hoa, thường không dấu từ a-z, chữ số 0-9, dấu '-', dấu '_'"};
		}

		return null;
	};
}

export function emojiValidator(): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		const value = control.value;
		const emojiPattern = /[\u{1F600}-\u{1F64F}\u{1F300}-\u{1F5FF}\u{1F680}-\u{1F6FF}\u{1F700}-\u{1F77F}\u{1F780}-\u{1F7FF}\u{1F800}-\u{1F8FF}\u{1F900}-\u{1F9FF}\u{1FA00}-\u{1FA6F}\u{2600}-\u{26FF}\u{2700}-\u{27BF}\u{2B50}\u{2B06}]+/u;
		if (emojiPattern.test(value)) {
			return { errorMessage: ErrorMessage.ERR_EMOJI_INVALID};
		}
		return null;
	};

}
/**
 * Validator tên doanh nghiệp, đơn vị
 * @returns
 */
export function nameBusinessValidator(): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		const nameBusinessPattern = /^[\p{L}0-9,.&+\-_%():/;%"'\\ ]+$/u;
		if (control.value && !nameBusinessPattern.test(control.value)) {
			return { errorMessage: ErrorMessage.ERR_NAME_BUSINESS_INVALID };
		}

		return null;
	};
}

// condition password: Tối thiểu 8 ký tự gồm 1 chữ hoa, 1 chữ thường và số
export function conditionPassword(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!@#$&*-_?%()]{8,}$/;
        if (control.value && !password.test(control.value)) {
          return { errorMessage: "Mật khẩu tối thiểu 8 ký tự có ít nhất 1 chữ viết hoa, chữ thường, số "};
        }

        return null;
    };
}

//
export function mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        // Set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmedValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
        return null;
    }
}

export function justAlphabetAndNumberValidator(): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		const alphabetAndNumberPattern = /^[a-zA-Z0-9\s]+$/u;
		if (control.value && !alphabetAndNumberPattern.test(control.value)) {
			return { errorMessage: ErrorMessage.ERR_JUST_ALPHABET_AND_NUM_INVALID };
		}

		return null;
	};
}

export function longitudeAndLatitudeValidator(): ValidatorFn {
	return (control: AbstractControl): { [key: string]: any } | null => {
		const longitudeAndLatitudePattern = /^[1-9][0-9]*(\.[0-9]+)?$/u;
		if (control.value && !longitudeAndLatitudePattern.test(control.value)) {
			return { errorMessage: ErrorMessage.ERR_LONGITUDE_LATITUDE_INVALID };
		}

		return null;
	};
}
