// import {AccordionModule} from 'primeng/accordion';
// import {AutoCompleteModule} from 'primeng/autocomplete';
// import {AvatarModule} from 'primeng/avatar';
// import {AvatarGroupModule} from 'primeng/avatargroup';
// import {BadgeModule} from 'primeng/badge';
// import {CarouselModule} from 'primeng/carousel';
// import {CascadeSelectModule} from 'primeng/cascadeselect';
// import {ChartModule} from 'primeng/chart';
// import {ChipModule} from 'primeng/chip';
// import {ChipsModule} from 'primeng/chips';
// import {ConfirmDialogModule} from 'primeng/confirmdialog';
// import {ConfirmPopupModule} from 'primeng/confirmpopup';
// import {ColorPickerModule} from 'primeng/colorpicker';
// import {ContextMenuModule} from 'primeng/contextmenu';
// import {DataViewModule} from 'primeng/dataview';
// import {DividerModule} from 'primeng/divider';
// import {FieldsetModule} from 'primeng/fieldset';
// import {GalleriaModule} from 'primeng/galleria';
// import {InplaceModule} from 'primeng/inplace';
// import {InputMaskModule} from 'primeng/inputmask';
// import {KnobModule} from 'primeng/knob';
// import {ListboxModule} from 'primeng/listbox';
// import {MegaMenuModule} from 'primeng/megamenu';
// import {MenuModule} from 'primeng/menu';
// import {MenubarModule} from 'primeng/menubar';
// import {OrderListModule} from 'primeng/orderlist';
// import {OrganizationChartModule} from 'primeng/organizationchart';
// import {OverlayPanelModule} from 'primeng/overlaypanel';
// import {PaginatorModule} from 'primeng/paginator';
// import {PanelModule} from 'primeng/panel';
// import {PanelMenuModule} from 'primeng/panelmenu';
// import {PasswordModule} from 'primeng/password';
// import {PickListModule} from 'primeng/picklist';
// import {ProgressBarModule} from 'primeng/progressbar';
// import {RatingModule} from 'primeng/rating';
// import {RippleModule} from 'primeng/ripple';
// import {ScrollPanelModule} from 'primeng/scrollpanel';
// import {ScrollTopModule} from 'primeng/scrolltop';
// import {SidebarModule} from 'primeng/sidebar';
// import {SkeletonModule} from 'primeng/skeleton';
// import {SlideMenuModule} from 'primeng/slidemenu';
import {SliderModule} from 'primeng/slider';
// import {SplitButtonModule} from 'primeng/splitbutton';
// import {SplitterModule} from 'primeng/splitter';
// import {StepsModule} from 'primeng/steps';
// import {TabMenuModule} from 'primeng/tabmenu';
// import {TableModule} from 'primeng/table';
// import {TabViewModule} from 'primeng/tabview';
// import {TerminalModule} from 'primeng/terminal';
// import {TieredMenuModule} from 'primeng/tieredmenu';
// import {TimelineModule} from 'primeng/timeline';
// import {ToggleButtonModule} from 'primeng/togglebutton';
// import {ToolbarModule} from 'primeng/toolbar';
// import {TooltipModule} from 'primeng/tooltip';
// import {TreeModule} from 'primeng/tree';
// import {TreeTableModule} from 'primeng/treetable';
// import {VirtualScrollerModule} from 'primeng/virtualscroller';
// import {FullCalendarModule} from '@fullcalendar/angular';

//
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {ButtonModule} from 'primeng/button';
import {CalendarModule} from 'primeng/calendar';
import {CardModule} from 'primeng/card';
import {CheckboxModule} from 'primeng/checkbox';
import {DialogModule} from 'primeng/dialog';
import {FileUploadModule} from 'primeng/fileupload';
import {DropdownModule} from 'primeng/dropdown';
import {ImageModule} from 'primeng/image';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from 'primeng/inputnumber';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {MultiSelectModule} from 'primeng/multiselect';
import {RadioButtonModule} from 'primeng/radiobutton';
import {TagModule} from 'primeng/tag';
import {ToastModule} from 'primeng/toast';
import {SelectButtonModule} from 'primeng/selectbutton'
import { ProgressBarModule } from 'primeng/progressbar';
import { SkeletonModule } from 'primeng/skeleton';

export const PrimeNGModuleBase = [
    CardModule,
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CheckboxModule,
    DropdownModule,
    InputNumberModule,
    InputTextModule,
    InputTextareaModule,
    MessageModule,
    MessagesModule,
    ToastModule,
    TagModule,
    FileUploadModule,
    ImageModule,
    SelectButtonModule,
    MultiSelectModule,
    RadioButtonModule,
    InputSwitchModule,
    DialogModule,
    ProgressBarModule,
    SkeletonModule ,
    SliderModule 
]
