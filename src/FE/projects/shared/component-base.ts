import { Injector } from "@angular/core";
import { ActiveDeactiveConst, BaseConsts, EActiveDeactive, EFormatDate, EIconConfirm, EYesNo } from "projects/shared/consts/base.consts";
import { Page } from "projects/shared/models/page";
import { Utils } from "projects/shared/utils";
import { FormGroup } from "@angular/forms";
import moment from "moment";
import { LibHelperService } from "projects/my-lib/src/public-api";
import { KeyFilterPatternConst } from "projects/shared/consts/key-filter-pattern.const";
import { ReplaySubject } from "rxjs";
import { PermissionsService } from "./services/permission/permissions.service";
import { TokenService } from "../my-lib/src/lib/shared/services/auth/token.service";
import jwtDecode from "jwt-decode";
import { ITokenDecode } from "./interfaces/token.interface";
import { IUserLogin } from "./interfaces/user.interface";
import { EFormElements } from "./enums/form-elements.enum";
import { ErrorMessage } from "./consts/error-code.const";
import { PermissionCoreConst } from "./consts/permissionWeb/PermissionCore";
import { SuccessMessage } from "./consts/success-message.const";
import { EMaxLength } from "./consts/base.enum";
import { environment } from "./environments/environment";
import { PermissionSalerConst } from "./consts/permissionWeb/PermissionSaler";
import { PermissionUserConst } from "./consts/permissionWeb/PermissionUser";
import Cryptr from 'cryptr';
import { ActionsConst } from "projects/my-lib/src/lib/shared/consts/actions.const";
import { PermissionInvestConst } from "./consts/permissionWeb/PermissionInvest";
/**
 * Component base cho tất cả app
 */
export abstract class ComponentBase {

    _libHelperService: LibHelperService;
    _permissionService: PermissionsService;
    _tokenService: TokenService;
    utils = Utils;

    // CONST
    KeyFilterPatternConst = KeyFilterPatternConst;
    BaseConsts = BaseConsts;
    ErrorMessage = ErrorMessage;
    SuccessMessage = SuccessMessage;
    YesNoConst = EYesNo;
    EActiveDeactive = EActiveDeactive;
    ActiveDeactiveConst = ActiveDeactiveConst;
    IconConfirm = EIconConfirm;
	//
	PermissionCoreConst = PermissionCoreConst;
    EMaxLength = EMaxLength;
    PermissionSalerConst = PermissionSalerConst;
    PermissionUserConst = PermissionUserConst;
    PermissionInvestConst = PermissionInvestConst;
    ActionsConst = ActionsConst;

    constructor(
        injector: Injector,
    ) {
        this._libHelperService = injector.get(LibHelperService);
        this._permissionService = injector.get(PermissionsService);
        this._tokenService = injector.get(TokenService);
        this._permissionService.getPermissions();
    }

    cryptr = new Cryptr('encodeId', { encoding: 'hex', pbkdf2Iterations: 1, saltLength: 1 });
    screenHeight = window.innerHeight;
    screenWidth = window.innerWidth;
    isLoading: boolean = false;

    submitted: boolean = false;
    onClickSubmit: boolean = false;

    isValid: boolean = false;
    isSpinner: boolean = false;

    page: Page = new Page();

    activeIndex = 0;

    blockText: RegExp = /[0-9,.]/;
    number: RegExp = /[0-9]/;
    numberPercent: RegExp = /[0-9,]/;
    blockSpace: RegExp = /[^\s]/;
    regexTaxcode: RegExp = /[0-9-]/;

    destroyed$: ReplaySubject<boolean> = new ReplaySubject(3);

    todayDate = new Date(); // Ngày hiện tại

    isGranted(permissionNames: any): boolean {
        if (environment.baseUrlInvest === window.location.origin) {
            // return true;
        }

		permissionNames = (typeof permissionNames === 'string') ? [permissionNames] : permissionNames;
        return this._permissionService.isGrantedRoot(permissionNames);
    }

    protected formatCalendarItemSendApi(datetime: Date) {
        if (!(datetime instanceof Date)) {
            return null
        }
        return moment(datetime, EFormatDate.DATE_YMD_Hms).format(EFormatDate.DATE_YMD_Hms);
    }

    public checkInValidForm(formGroup: FormGroup, skipValidationForFields: string[] = []): boolean {
        for (const i in formGroup.controls) {
            if (!skipValidationForFields.includes(i)) {
                formGroup.controls[i].markAsDirty();
            } else {
                formGroup.controls[i].clearValidators();
            }
            //
            formGroup.controls[i].updateValueAndValidity({emitEvent: false});
        }
        for(const key in formGroup.controls) {
            if(formGroup.controls[key].status === "INVALID") {
                Utils.log(`invalid`, { controlName: key, controls: formGroup.controls[key]});
            }
        }
        return formGroup.invalid;
    }

    isInvalidMessage (form: FormGroup, controlName: string): boolean {
		    return !!(form?.get(controlName)?.hasError('errorMessage') && form?.get(controlName)?.invalid && form?.get(controlName)?.touched);
    }

    isInvalid (form: FormGroup, controlName: string, onClickSubmit?: boolean) : boolean {
            if(onClickSubmit) {
                return onClickSubmit && form?.get(controlName)?.invalid;
            }
            return !!(form?.get(controlName)?.invalid && form?.get(controlName)?.touched);
    }

    convertObjectToFormData(object, formData: FormData = new FormData(), parentKey = null, arrayIndex = null): FormData {
        for (const [key, value] of Object.entries(object)) {
            const finalKey = parentKey ? (arrayIndex !== null ? `${parentKey}[${arrayIndex}].${key}` : `${parentKey}.${key}`) : key;
            if (Array.isArray(value)) {
                for (let i = 0; i < value.length; i++) {
                    const arrayItemKey = `${finalKey}[${i}]`;
                    if (typeof value[i] === 'object' && value[i] !== null) {
                        this.convertObjectToFormData(value[i], formData, finalKey, i);
                    } else {
                        formData.append(arrayItemKey, String(value[i]));
                    }
                }
            } else if (typeof value === 'object' && value !== null) {
                if (value instanceof File) {
                    // Handle File objects separately and append them to FormData
                    formData.append(finalKey, value, value.name);
                } else {
                    this.convertObjectToFormData(value, formData, finalKey);
                }
            } else {
                formData.append(finalKey, String(value));
            }
        }

        return formData;
    }

    getConfigDialogServiceRAC(title: string, params: any) {
        return {
            header: title,
            width: '600px',
            baseZIndex: 10000,
            data: {
                id: params.id,
                summary: params?.summary,
            },
        };
    }

    cryptEncode(id): string {
		try {
			return this.cryptr.encrypt(id.toString());
		} catch (err) {
			return null;
		}
    }

    cryptDecode(codeId, isRedirect:boolean = true) {
		try {
			return this.cryptr.decrypt(codeId.toString());
		} catch (err) {
            console.log('errDecode: ', codeId);
            if(isRedirect) this._libHelperService.redirectPageNotFound();
			return null;
		}
    }

    handleResponseInterceptor(response, message?: string): boolean {
        return this._libHelperService.handleResponseInterceptor(response, message);
    }

    messageError(msg = '', summary = '', life = 3000) {
        return this._libHelperService.messageError(msg, summary, life);
    }

    messageSuccess(msg = '', summary = '', life = 2000) {
        return this._libHelperService.messageSuccess(msg, summary, life);
    }

    messageWarn(msg = '', life = 3000) {
        return this._libHelperService.messageWarn(msg, life);
    }

    checkValidForm(isValid: boolean) {
        if (!isValid) this.messageError('Vui lòng nhập đủ thông tin!');
        return isValid;
    }

    // lấy giá trị mặc định của obj
    getPropertyObj(obj, path, type: string = '') {
        if (!path) return null
        const value = path.split('.').reduce((o, key) => (o && o[key] !== 'undefined' ? o[key] : null), obj);
        if(type === EFormElements.CALENDAR) {
            return value ? new Date(value) : null;
        }
        return value;
    }

    getUser() : IUserLogin {
        let userInfo = Utils.getLocalStorage(BaseConsts.localStorageUser);
        console.log('userInfo', userInfo);
        if(userInfo) {
            return {
                username: userInfo?.username,
                userType: userInfo?.userType,
                id: userInfo?.id,
                fullName: userInfo?.fullName
            };
        } else {
            let token = this._tokenService.getToken();
            if(token) {
                let tokenDecode: ITokenDecode = jwtDecode(token);
                console.log('tokenDecode', tokenDecode);
                return {
                    username: tokenDecode.username,
                    userType: tokenDecode.user_type,
                    id: tokenDecode.user_id,
                    fullName: tokenDecode.name,
                }
            }
        }
        //
        return {};
    }
}
