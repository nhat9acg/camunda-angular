export class DefaultCustomerIdentification {
    id: number;
    customerId: number;
    idType: string;
    idNo: string;
    fullname: string;
    dateOfBirth: string;
    nationality: string;
    personalIdentification: string;
    idIssuer: string;
    idDate: string;
    idExpiredDate: string;
    placeOfOrigin: string;
    placeOfResidence: string;
    idFrontImageUrl: string;
    idBackImageUrl: string;
    idFrontImageS3Key: string;
    idBackImageS3Key: string;
    faceImageUrl: string;
    faceVideoUrl: string;
    status: number;
    isDefault: boolean;
    isVerifiedFace: boolean;
    isVerifiedIdentification: boolean;
    sex: number;
    ekycIncorrectFields: string;
    ekycInfoIsConfirmed: boolean;
  }
  
  export class DefaultBankAccount {
    id: number;
    customerId: number;
    bankId: number;
    bankName: string;
    fullBankName: string;
    bankAccount: string;
    ownerAccount: string;
    bankBranch: string;
    status: number;
    isDefault: boolean;
  }
  
  export class BankAccount {
    id: number;
    coreBankAccId: number;
    customerId: number;
    bankAccount: string;
    ownerAccount: string;
    bankName: string;
    bankBranch: string;
    bankCode: string;
    logo: string;
    bankId: number;
    fullBankName: string;
    status: number;
    isDefault: boolean;
  }