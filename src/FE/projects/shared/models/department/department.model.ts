export class DepartmentModel {
    id: number;
    departmentName: string;
    departmentAddress: string;
    parentId: number;
    departmentLevel: number;
    managerId: number;
    hasDepartmentChild: boolean;
}