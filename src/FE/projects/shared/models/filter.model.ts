import { ISort } from "../interfaces/lib-table.interface";

export class BasicFilter {
    keyword?: string;
    status?: number | string;
    field?: string;
    sortFields?: ISort[];
}
