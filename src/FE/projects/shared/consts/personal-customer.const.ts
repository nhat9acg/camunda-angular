import { EApproveAction, EApproveStatus, EIdentificationChangeReason, EIdentificationTypes, EPersonalCustomerStatus, EPersonalCustomerStatusTemp, ERelationship, ESaleStatus, ESaleType, ESex, ESource } from "./personal-customer.enum";

export class PersonalCustomerConst {
    public static fieldNamePersonalPhone = 'personalPhone';
    public static fieldNamePersonalEmail = 'personalEmail';
    public static statusTempList = [
        {
            name: 'Khởi tạo',
            class: 'tag-init',
            code: EPersonalCustomerStatusTemp.INACTIVE,
        },
        {
            name: 'Chờ xử lý',
            code: EPersonalCustomerStatusTemp.PENDING_APPROVAL,
            class: 'tag-request',
        },
        {
            name: 'Đã duyệt',
            code: EPersonalCustomerStatusTemp.ACTIVE,
            class: 'tag-active',
        },
        {
            name: 'Hủy duyệt',
            code: EPersonalCustomerStatusTemp.SUSPENDED,
            class: 'tag-cancel',
        },
    ];

    public static STATUSTEMP_TEMP = 1;
    public static STATUSTEMP_REQUEST = 2;
    public static STATUSTEMP_ACTIVE = 3;
    public static STATUSTEMP_CANCEL = 4;

    public static getStatusTempInfo(code, atribution = null) {
        let status = this.statusTempList.find(i => i.code === code);
        return status && atribution ? status[atribution] : status;
    }

    public static TYPE_BUSSINESS_CUSTOMER = 1;
    public static TYPE_PERSONAL_CUSTOMER = 2;


    public static types = [
        {
            name: 'Cá nhân',
            code: this.TYPE_PERSONAL_CUSTOMER,
        },
        {
            name: 'Doanh nghiệp',
            code: this.TYPE_BUSSINESS_CUSTOMER,
        },
    ]

    public static getTypeCustomer(code, atribution = null) {
        let status = this.types.find(i => i.code == code);
        return status && atribution ? status[atribution] : status;
    }



    public static statusList = [
        {
            name: 'Hoạt động',
            code: EPersonalCustomerStatus.ACTIVE,
            class: 'tag-active',
        },
        {
            name: 'Đang cập nhật',
            code: EPersonalCustomerStatus.UPDATETING,
            class: 'tag-active',
        },
    ];

    public static getRelationshipInfo(code, atribution = null) {
        let status = this.statusList.find(i => i.code == code);
        return status && atribution ? status[atribution] : status;
    }

    public static relationshipList = [
        {
            name: 'Chồng',
            code: ERelationship.CHONG,
        },
        {
            name: 'Vợ',
            code: ERelationship.VO,
        },
        {
            name: 'Bố mẹ đẻ',
            code: ERelationship.BO_ME_DE,
        },
        {
            name: 'Bố mẹ vợ chồng',
            code: ERelationship.BO_ME_VO_CHONG,
        },
        {
            name: 'Con đẻ',
            code: ERelationship.CON_DE,
        },
        {
            name: 'Con dâu/rể',
            code: ERelationship.CON_DAU_RE,
        },
        {
            name: 'Con nuôi',
            code: ERelationship.CON_NUOI,
        },
        {
            name: 'Anh ruột',
            code: ERelationship.ANH_RUOT,
        },
        {
            name: 'Chị ruột',
            code: ERelationship.CHI_RUOT,
        },
        {
            name: 'Em ruột',
            code: ERelationship.EM_RUOT,
        },
        {
            name: 'Anh/chị/em họ',
            code: ERelationship.ANH_EM_HO,
        },
        {
            name: 'Ông/Bà',
            code: ERelationship.ONG_BA,
        },
        {
            name: 'Người giám hộ',
            code: ERelationship.NGUOI_GIAM_HO,
        },
        {
            name: 'Khác',
            code: ERelationship.KHAC,
        },
    ];

    public static getStatusInfo(code, atribution = null) {
        let status = this.statusList.find(i => i.code == code);
        return status && atribution ? status[atribution] : status;
    }

    public static idTypes = [
        {
            name: 'Căn cước công dân',
            code: EIdentificationTypes.CCCD
        },
        {
            name: 'Căn cước công dân chip',
            code: EIdentificationTypes.CCCD_CHIP
        },
        {
            name: 'Chứng minh nhân dân',
            code: EIdentificationTypes.CMND
        },
        {
            name: 'Hộ chiếu',
            code: EIdentificationTypes.PASSPORT
        }
    ];

    public static getIdTypeInfo(code, atribution = null) {
        if (code) {
            code = code.toLowerCase();
        }
        let info = this.idTypes.find(i => i.code == code);
        return info && atribution ? info[atribution] : info;
    }

    public static TEMP = {
        YES: 1,
        NO: 0
    };

    public static sourceList = [
        {
            name: 'Online',
            code: ESource.ONLINE,
        },
        {
            name: 'Offline',
            code: ESource.OFFLINE,
        },
        {
            name: 'Tư vấn viên',
            code: ESource.SALE,
        },
    ];

    public static getSourceInfo(code, atribution = null) {
        let info = this.sourceList.find(i => i.code === code);
        return info && atribution ? info[atribution] : info;
    }


}

export class PersonalStockConst {
    public static stockList = [
        {
            name: 'Công ty cổ phần chứng khoán Tiên Phong - TPS',
            code: 1,
        },
    ];

    public static getInfo(code, atribution = null) {
        let status = this.stockList.find(i => i.code === code);
        return status && atribution ? status[atribution] : status;
    }
}


export class PersonalFileConst {

    public static FACE_IMAGE_SIMILARITY_LOW = 50;
    public static FACE_IMAGE_SIMILARITY_HIGH = 80;

    public static identificationList = [
        {
            name: 'Kích hoạt',
            code: 1,
            class: 'tag-active'
        },
        {
            name: 'Hủy kích hoạt',
            code: 2,
            class: 'tag-deactive',
        },
    ];

    public static getInfo(code, atribution = null) {
        let status = this.identificationList.find(i => i.code === code);
        return status && atribution ? status[atribution] : status;
    }

    public static reasonList = [
        {
            code: EIdentificationChangeReason.BLURRED,
            name: 'Giấy tờ bị mờ',
        },
        {
            code: EIdentificationChangeReason.TORNED,
            name: 'Giấy tờ bị rách',
        },
        {
            code: EIdentificationChangeReason.EXPIRED,
            name: 'Giấy tờ hết hạn',
        },
        {
            code: EIdentificationChangeReason.OTHER,
            name: 'Khác',
        },
    ];
}

export class ApproveConst {
    public static actionType = [
        {
            name: 'Thêm',
            code: EApproveAction.ADD,
            severity: 'success',
        },
        {
            name: 'Sửa',
            code: EApproveAction.UPDATE,
            severity: 'warning',
        },
        {
            name: 'Xoá',
            code: EApproveAction.DELETE,
            severity: 'danger',
        },
    ];

    public static getInfo(code, atribution = null) {
        let actionType = this.actionType.find(actionType => actionType.code == code);
        return actionType && atribution ? actionType[atribution] : actionType;
    }

    public static status = [
        {
            name: 'Chờ xử lý',
            code: EApproveStatus.PENIDNG,
            severity: 'warning',
        },
        {
            name: 'Đã duyệt',
            code: EApproveStatus.APPROVE,
            severity: 'success',
        },
        {
            name: 'Hủy',
            code: EApproveStatus.CANCEL,
            severity: 'danger',
        },
    ];

    public static getInfoStatus(code, atribution = null) {
        let status = this.status.find(status => status.code == code);
        return status && atribution ? status[atribution] : status;
    }
}

export class SaleConst {
    public static status = [
        {
            name: 'Hoạt động',
            code: ESaleStatus.ACIVE,
            class: 'tag-active'
        },
        {
            name: 'Đang khóa',
            code: ESaleStatus.DEACTIVE,
            class: 'tag-lock'
        },
    ];

    public static getInfo(code, atribution = null) {
        let status = this.status.find(status => status.code == code);
        return status && atribution ? status[atribution] : status;
    }

    public static types = [
        {
            name: 'Quản lý',
            code: ESaleType.MANAGER,
        },
        {
            name: 'Nhân viên',
            code: ESaleType.STAFF,
        },
        {
            name: 'Cộng tác viên',
            code: ESaleType.COLLABORATOR,
        },
    ];

    public static getSaleType(code, atribution = null) {
        const type = this.types.find(s => s.code == code);
        return type && atribution ? type[atribution] : type;
    }

    // Bộ lọc tìm kiếm nhập keyword theo loại sale active
    public static fieldSearchsSaleActive = [
        {
            name: 'Mã giới thiệu',
            code: 1,
            field: 'referralCode',
            placeholder: 'Nhập Mã giới thiệu...',
        },
        {
            name: 'Số điện thoại',
            code: 2,
            field: 'phone',
            placeholder: 'Nhập Số điện thoại...',
        },
        {
            name: 'Họ và tên/Tên doanh nghiệp',
            code: 3,
            field: 'customerName',
            placeholder: 'Nhập Họ và tên/Tên doanh nghiệp...',
        },
        // {
        //     name: 'Mã số thuế',
        //     code: 5,
        //     field: 'taxCode',
        //     placeholder: 'Nhập mã số thuế...',
        // },

    ];

     // Bộ lọc tìm kiếm nhập keyword theo loại
     public static fieldSearchs = [
        {
            name: 'Mã giới thiệu',
            code: 1,
            field: 'referralCode',
            placeholder: 'Nhập Mã giới thiệu...',
        },
        {
            name: 'Số điện thoại',
            code: 2,
            field: 'phone',
            placeholder: 'Nhập Số điện thoại...',
        },
        {
            name: 'Họ và tên/Tên doanh nghiệp',
            code: 3,
            field: 'customerName',
            placeholder: 'Nhập Họ và tên/Tên doanh nghiệp...',
        },

    ];


    public static getFieldSearchInfo(field, attribute: string) {
        const fieldSearch = this.fieldSearchs.find(fields => fields.field == field);
        return fieldSearch ? fieldSearch[attribute] : null;
    }
}
