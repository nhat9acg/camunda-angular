export enum PermissionTypes {
	Web = 1,
	Menu = 2,
	Page = 3,
	Table = 4,
	Tab = 5,
	Form = 6,
	ButtonTable = 7,
	ButtonAction = 8,
	ButtonForm = 9,
  }
  
  export enum WebKeys {
	  Home = 1,
	  User = 2,
	  Core = 3,
	  Saler = 4,
	  Invest = 5,
  }
  