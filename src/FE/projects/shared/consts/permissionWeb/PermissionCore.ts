export class PermissionCoreConst {
    private static readonly Menu = "menu_";
    private static readonly Tab = "tab_";
    private static readonly Table = "table_";
    private static readonly Button = "btn_";

    //Tổng quan 
    public static readonly CoreMenuDashboard = `${PermissionCoreConst.Menu}core_dashboad`;

    /* #region Tài khoản */
    public static readonly CoreMenuAppAccount = `${PermissionCoreConst.Menu}app_account_dashboard`;

        //Tài khoản người dùng
        public static readonly CoreMenuListUserAccount = `${PermissionCoreConst.Menu}list_user_account`;
        public static readonly CoreTableListUserAccount = `${PermissionCoreConst.Table}list_user_account`;
        public static readonly CoreButtonAddUserAccount = `${PermissionCoreConst.Button}add_user_account`;
        public static readonly CoreButtonLockUserAccount = `${PermissionCoreConst.Button}lock_user_account`;
        public static readonly CoreButtonResetPasswordUserAccount = `${PermissionCoreConst.Button}reset_password_user_account`;
        public static readonly CoreButtonResetPinCodeUserAccount = `${PermissionCoreConst.Button}reset_pin_code_user_account`;
        public static readonly CoreButtonDeleteUserAccount = `${PermissionCoreConst.Button}delete_user_account`;

        //Tài khoản chưa xác minh
        public static readonly CoreMenuUnverifiedAccount = `${PermissionCoreConst.Menu}list_unverified_account`;
        public static readonly CoreTableUnverifiedAccount = `${PermissionCoreConst.Table}list_unverified_account`;
        public static readonly CoreButtonAddUnverifiedAccount = `${PermissionCoreConst.Button}add_unverified_account`;
        public static readonly CoreButtonLockUnverifiedAccount = `${PermissionCoreConst.Button}lock_unverified_account`;
        public static readonly CoreButtonVerifyUnverifiedAccount = `${PermissionCoreConst.Button}verify_unverified_account`;
        public static readonly CoreButtonResetPasswordUnverifiedAccount = `${PermissionCoreConst.Button}reset_password_unverified_account`;
        public static readonly CoreButtonResetPinCodeUnverifiedAccount = `${PermissionCoreConst.Button}reset_pin_code_unverified_account`;
        public static readonly CoreButtonDeleteUnverifiedAccount = `${PermissionCoreConst.Button}delete_unverified_account`;
    /* #endregion */

    //#region Khách hàng
    public static readonly CoreMenuCustomerManagement = `${PermissionCoreConst.Menu}customer_management`;
        //Khách hàng cá nhân
        public static readonly CoreMenuPersonalCustomerManagement = `${PermissionCoreConst.Menu}personal_customer_management`;
        public static readonly CoreTablePersonalCustomer = `${PermissionCoreConst.Table}personal_customer`;
        public static readonly CoreButtonPersonalCustomerAdd = `${PermissionCoreConst.Button}add_personal_customer`;

        //thông tin chi tiết
        public static readonly CoreButtonPersonalCustomerInfo = `${PermissionCoreConst.Button}personal_customer_info_management`;

            //thông tin chung
            public static readonly CoreTabPersonalCustomerDetail = `${PermissionCoreConst.Tab}personal_customer_detail`;
            public static readonly CoreButtonPersonalCustomerUpdate = `${PermissionCoreConst.Button}update_personal_customer`;
            public static readonly CoreButtonPersonalCustomerUpdateAvatar = `${PermissionCoreConst.Button}update_avatar_personal_customer`;
            public static readonly CoreButtonPersonalCustomerChangePhone = `${PermissionCoreConst.Button}change_phone_personal_customer`;
            public static readonly CoreButtonPersonalCustomerChangeEmail = `${PermissionCoreConst.Button}change_email_personal_customer`;
            public static readonly CoreTabPersonalCustomerInfoDetail = `${PermissionCoreConst.Tab}personal_customer_info_detail`;

            //tài khoản ngân hàng
            public static readonly CoreTabPersonalCustomerBank = `${PermissionCoreConst.Tab}personal_customer_bank`;
            public static readonly CoreTablePersonalCustomerBank = `${PermissionCoreConst.Table}personal_customer_bank`;
            public static readonly CoreButtonPersonalCustomerBankAdd = `${PermissionCoreConst.Button}add_personal_customer_bank`;
            public static readonly CoreButtonPersonalCustomerBankUpdate = `${PermissionCoreConst.Button}update_personal_customer_bank`;
            public static readonly CoreButtonPersonalCustomerBankDelete = `${PermissionCoreConst.Button}delete_personal_customer_bank`;
            public static readonly CoreButtonPersonalCustomerBankSetDefault = `${PermissionCoreConst.Button}set_default_personal_customer_bank`;
            public static readonly CoreButtonPersonalCustomerBankViewDetail = `${PermissionCoreConst.Button}view_detail_personal_customer_bank`;

            //định danh
            public static readonly CoreTabPersonalCustomerIdentification = `${PermissionCoreConst.Tab}personal_customer_identification`;
            public static readonly CoreTablePersonalCustomerIdentification = `${PermissionCoreConst.Table}personal_customer_identification`;
            public static readonly CoreButtonPersonalCustomerIdentificationAdd = `${PermissionCoreConst.Button}add_personal_customer_identification`;
            public static readonly CoreButtonPersonalCustomerIdentificationUpdate = `${PermissionCoreConst.Button}update_personal_customer_identification`;
            public static readonly CoreButtonPersonalCustomerIdentificationDelete = `${PermissionCoreConst.Button}delete_personal_customer_identification`;
            public static readonly CoreButtonPersonalCustomerIdentificationSetDefault = `${PermissionCoreConst.Button}set_default_personal_customer_identification`;

            //liên hệ
            public static readonly CoreTabPersonalCustomerContact = `${PermissionCoreConst.Tab}personal_customer_contact`
            //địa chỉ liên hệ
            public static readonly CoreTabPersonalCustomerAddress = `${PermissionCoreConst.Tab}personal_customer_address`;
            public static readonly CoreTablePersonalCustomerAddress = `${PermissionCoreConst.Table}personal_customer_address`;
            public static readonly CoreButtonPersonalCustomerAddressAdd = `${PermissionCoreConst.Button}add_personal_customer_address`;
            public static readonly CoreButtonPersonalCustomerAddressUpdate = `${PermissionCoreConst.Button}update_personal_customer_address`;
            public static readonly CoreButtonPersonalCustomerAddressDelete = `${PermissionCoreConst.Button}delete_personal_customer_address`;
            public static readonly CoreButtonPersonalCustomerAddressViewDetail = `${PermissionCoreConst.Button}view_detail_personal_customer_address`;
            public static readonly CoreButtonPersonalCustomerAddressSetDefault = `${PermissionCoreConst.Button}set_default_personal_customer_address`;
            
            //người liên hệ khẩn cấp
            public static readonly CoreTabPersonalCustomerEmergencyContact = `${PermissionCoreConst.Tab}personal_customer_emertgency_contact`;
            public static readonly CoreTabPersonalCustomerEmergencyContactInfo = `${PermissionCoreConst.Tab}personal_customer_emertgency_contact_info`;
            public static readonly CoreButtonPersonalCustomerEmergencyContactUpdate = `${PermissionCoreConst.Button}update_personal_customer_emertgency_contact`;

            // Người đại diện
            public static readonly CoreTabPersonalCustomerRep = `${PermissionCoreConst.Tab}personal_customer_rep`;
            public static readonly CoreTabPersonalCustomerRepInfo = `${PermissionCoreConst.Tab}personal_customer_rep_info`;
            public static readonly CoreButtonPersonalCustomerRepUpdate = `${PermissionCoreConst.Button}update_personal_customer_rep`;

            //người giới thiệu          
            public static readonly CoreTabPersonalCustomerReferal = `${PermissionCoreConst.Tab}personal_customer_referal`;
            public static readonly CoreTablePersonalCustomerReferal = `${PermissionCoreConst.Table}personal_customer_referal`;
            public static readonly CoreTablePersonalCustomerReferalPerson = `${PermissionCoreConst.Table}personal_customer_referal_person`;
            public static readonly CoreButtonPersonalCustomerReferalAdd = `${PermissionCoreConst.Button}add_personal_customer_referal`;

            //lịch sử
            public static readonly CoreTabPersonalCustomerHistory = `${PermissionCoreConst.Tab}personal_customer_history`
            public static readonly CoreTablePersonalCustomerHistory = `${PermissionCoreConst.Table}personal_customer_history`
            public static readonly CoreButtonPersonalCustomerHistory = `${PermissionCoreConst.Button}personal_customer_history_detail`

            public static readonly CoreTabPersonalSale = `${PermissionCoreConst.Tab}personal_customer_sale`;
            public static readonly CoreTablePersonalSale = `${PermissionCoreConst.Table}personal_customer_sale`;
            public static readonly CoreButtonPersonalSaleAdd = `${PermissionCoreConst.Button}personal_customer_sale_add`;
            public static readonly CoreButtonPersonalSaleSetDefault = `${PermissionCoreConst.Button}personal_customer_sale_default`;

        //Khách hàng doanh nghiệp
        public static readonly CoreMenuBusinessCustomerManagement = `${PermissionCoreConst.Menu}business_customer_management`;
        public static readonly CoreTableBusinessCustomer = `${PermissionCoreConst.Table}business_customer`;
        public static readonly CoreButtonBusinessCustomerAdd = `${PermissionCoreConst.Button}add_business_customer`;

        //thông tin chi tiết
        public static readonly CoreButtonBusinessCustomerInfo = `${PermissionCoreConst.Button}business_customer_info_management`;

            //thông tin chung
            public static readonly CoreTabBusinessCustomerDetail = `${PermissionCoreConst.Tab}business_customer_detail`;
            public static readonly CoreButtonBusinessCustomerUpdate = `${PermissionCoreConst.Button}update_business_customer`;
            public static readonly CoreTabBusinessCustomerInfoDetail = `${PermissionCoreConst.Tab}business_customer_info_detail`;

            //tài khoản ngân hàng
            public static readonly CoreTabBusinessCustomerBank = `${PermissionCoreConst.Tab}business_customer_bank_management`;
            public static readonly CoreTableBusinessCustomerBank = `${PermissionCoreConst.Table}business_customer_bank`;
            public static readonly CoreButtonBusinessCustomerBankAdd = `${PermissionCoreConst.Button}add_business_customer_bank`;
            public static readonly CoreButtonBusinessCustomerBankUpdate = `${PermissionCoreConst.Button}update_business_customer_bank`;
            public static readonly CoreButtonBusinessCustomerBankDelete = `${PermissionCoreConst.Button}delete_business_customer_bank`;
            public static readonly CoreButtonBusinessCustomerBankSetDefault = `${PermissionCoreConst.Button}set_default_business_customer_bank`;

            //quản lý hồ sơ
            public static readonly CoreTabBusinessCustomerLicense = `${PermissionCoreConst.Tab}business_customer_license_management`;
            public static readonly CoreTableBusinessCustomerLicense = `${PermissionCoreConst.Table}business_customer_license`;
            public static readonly CoreButtonBusinessCustomerLicenseAdd = `${PermissionCoreConst.Button}add_business_customer_license`;
            public static readonly CoreButtonBusinessCustomerLicenseUpdate = `${PermissionCoreConst.Button}update_business_customer_license`;
            public static readonly CoreButtonBusinessCustomerLicenseDelete = `${PermissionCoreConst.Button}delete_business_customer_license`;
            public static readonly CoreButtonBusinessCustomerLicenseViewFile = `${PermissionCoreConst.Button}view_business_customer_license_file`;
            public static readonly CoreButtonBusinessCustomerLicenseDownload = `${PermissionCoreConst.Button}download_business_customer_license`;

            //Lịch sử 
            public static readonly CoreTabBusinessCustomerHistory = `${PermissionCoreConst.Tab}business_customer_history`
            public static readonly CoreTableBusinessCustomerHistory = `${PermissionCoreConst.Table}business_customer_history`
            public static readonly CoreButtonBusinessCustomerHistory = `${PermissionCoreConst.Button}business_customer_history_detail`
    //#endregion

    //#region Media
    public static readonly CoreMenuMediaDashboard = `${PermissionCoreConst.Menu}media_dashboard`;

        //Tin tức
        public static readonly CoreMenuMediaNewsDashboard = `${PermissionCoreConst.Menu}media_news_dashboard`;
        public static readonly CoreTableMediaNews = `${PermissionCoreConst.Table}media_news`;
        public static readonly CoreButtonMediaNewsAdd = `${PermissionCoreConst.Button}media_news_add`;
        public static readonly CoreButtonMediaNewsDetail = `${PermissionCoreConst.Button}media_news_detail`;
        public static readonly CoreButtonMediaNewsApprove = `${PermissionCoreConst.Button}media_news__approve`;
        public static readonly CoreButtonMediaNewsDelete = `${PermissionCoreConst.Button}media_news_delete`;
        public static readonly CoreButtonMediaNewsCancel = `${PermissionCoreConst.Button}media_news_cancel`;
        public static readonly CoreButtonMediaNewsRequest = `${PermissionCoreConst.Button}media_news_request`;

        //Hình ảnh
        public static readonly CoreMenuImageDashboard = `${PermissionCoreConst.Menu}image_dashboad`;
        public static readonly CoreTableImage = `${PermissionCoreConst.Table}image`;
        public static readonly CoreButtonImageAdd = `${PermissionCoreConst.Button}image_add`;
        public static readonly CoreButtonImageDetail = `${PermissionCoreConst.Button}image_detail`;
        public static readonly CoreButtonImageApprove = `${PermissionCoreConst.Button}image_approve`;
        public static readonly CoreButtonImageDelete = `${PermissionCoreConst.Button}image_delete`;
        public static readonly CoreButtonImageCancel = `${PermissionCoreConst.Button}image_cancel`;
        public static readonly CoreButtonImageRequest = `${PermissionCoreConst.Button}image_request`;

        //Kiến thức đầu tư
        public static readonly CoreMenuKnowledgeDashboard = `${PermissionCoreConst.Menu}knowledge_dashboad`;
        public static readonly CoreButtonKnowledgeAdd = `${PermissionCoreConst.Button}knowledge_add`;
        public static readonly CoreTableKnowledge = `${PermissionCoreConst.Table}knowledge`;
        public static readonly CoreButtonKnowledgeDetail = `${PermissionCoreConst.Button}knowledge_detail`;
        public static readonly CoreButtonKnowledgeUpdate = `${PermissionCoreConst.Button}update_knowledge`;
        public static readonly CoreButtonKnowledgeRequest = `${PermissionCoreConst.Button}knowledge_request`;
        public static readonly CoreButtonKnowledgeApprove = `${PermissionCoreConst.Button}knowledge_approve`;
        public static readonly CoreButtonKnowledgeCancel = `${PermissionCoreConst.Button}knowledge_cancel`;
        public static readonly CoreButtonKnowledgeDelete = `${PermissionCoreConst.Button}knowledge_delete`;
    //#endregion

    //#region Thông báo
    public static readonly CoreMenuNotification = `${PermissionCoreConst.Menu}notification`;
        //Mẫu thông báo
        public static readonly CoreMenuNotificationTemplate = `${PermissionCoreConst.Menu}notification_template`;
        public static readonly CoreButtonAddNotificationTemplate = `${PermissionCoreConst.Button}add_notification_template`;
        public static readonly CoreTableNotificationTemplate = `${PermissionCoreConst.Table}addnotification_template`;
        public static readonly CoreButtonDetailNotificationTemplate = `${PermissionCoreConst.Button}detail_notification_template`;
        public static readonly CoreButtonUpdateNotificationTemplate = `${PermissionCoreConst.Button}update_notification_template`;
        public static readonly CoreButtonLockNotificationTemplate = `${PermissionCoreConst.Button}lock_notification_template`;
        public static readonly CoreButtonDeleteNotificationTemplate = `${PermissionCoreConst.Button}delete_notification_template`;

        //Quản lý thông báo
        public static readonly CoreMenuNotificationManager = `${PermissionCoreConst.Menu}notification_manager`;
        public static readonly CoreButtonAddNotificationManager = `${PermissionCoreConst.Button}add_notification_manager`;
        public static readonly CoreTableNotificationManager = `${PermissionCoreConst.Table}notification_manager`;
        public static readonly CoreButtonDetailNotificationManager = `${PermissionCoreConst.Button}detail_notification_manager`;
        public static readonly CoreButtonDeleteNotificationManager = `${PermissionCoreConst.Button}delete_notification_manager`;
        public static readonly CoreTabDetailNotificationManager = `${PermissionCoreConst.Tab}detail_notification_manager`;
        public static readonly CoreButtonUpdateNotificationManager = `${PermissionCoreConst.Button}update_notification_manager`;
        public static readonly CoreTabListSendNotificationManager = `${PermissionCoreConst.Tab}list_send_notification_manager`;
        public static readonly CoreTableListSendNotificationManager = `${PermissionCoreConst.Table}list_send_notification_manager`;
        public static readonly CoreButtonSetupList = `${PermissionCoreConst.Button}setup_list`;      
        public static readonly CoreButtonSendNotification = `${PermissionCoreConst.Button}send_notification`;      
        public static readonly CoreButtonDeleteSendNotification = `${PermissionCoreConst.Button}delete_send_notification`;      
    //#endregion

    //#region Thiết lập
    public static readonly CoreMenuConfigurationManagement = `${PermissionCoreConst.Menu}configuration_management`;
        //Thông báo hệ thống
        public static readonly CoreMenuSystemNotificationConfig = `${PermissionCoreConst.Menu}system_notification_config`;
        public static readonly CoreTableSystemNotificationConfig = `${PermissionCoreConst.Table}system_notification_config`;
        public static readonly CoreButtonSystemNotificationUpdate = `${PermissionCoreConst.Button}system_notification_config_update`;

        //Server thông báo
        public static readonly CoreMenuServerNotificationConfig = `${PermissionCoreConst.Menu}server_notification_config`;
        public static readonly CoreButtonServerNotificationConfigUpdate = `${PermissionCoreConst.Button}server_notification_config_update`;    
    //#endregion

    //#region Quản lý yêu cầu
    public static readonly CoreMenuRequestManager = `${PermissionCoreConst.Menu}request_manager`;
        //Khách hàng cá nhân
        public static readonly CoreMenuRequestManagerPersonalCustomer = `${PermissionCoreConst.Menu}request_manager_personal_customer`;
        public static readonly CoreTableRequestManagerPersonalCustomer = `${PermissionCoreConst.Table}request_manager_personal_customer`;

        public static readonly CoreButtonRequestManagerPersonalCustomerRequestApprove = `${PermissionCoreConst.Button}request_manager_personal_customer_request_approve`;
        public static readonly CoreButtonRequestManagerPersonalCustomerDetail = `${PermissionCoreConst.Button}request_manager_personal_customer_detail`;
        public static readonly CoreTabRequestManagerPersonalCustomerDetail = `${PermissionCoreConst.Tab}request_manager_personal_customer_detail`;

        public static readonly CoreTableRequestManagerPersonalCustomerBank = `${PermissionCoreConst.Table}request_manager_personal_customer_bank`;
        public static readonly CoreButtonDetailRequestManagerPersonalCustomerBank = `${PermissionCoreConst.Button}detail_request_manager_personal_customer_bank`;
        public static readonly CoreButtonAddRequestManagerPersonalCustomerBank = `${PermissionCoreConst.Button}add_request_manager_personal_customer_bank`;
        public static readonly CoreButtonUpdateRequestManagerPersonalCustomerBank = `${PermissionCoreConst.Button}update_request_manager_personal_customer_bank`;
        public static readonly CoreButtonRequestManagerPersonalCustomerBankSetDefault = `${PermissionCoreConst.Button}request_manager_personal_customer_bank_default`;
        public static readonly CoreButtonRequestManagerPersonalCustomerBankDetail = `${PermissionCoreConst.Button}request_manager_personal_customer_bank_detail`;
        public static readonly CoreTabRequestManagerPersonalCustomerBank = `${PermissionCoreConst.Tab}request_manager_personal_customer_bank`;

        public static readonly CoreTabRequestManagerPersonalCustomerIdentification = `${PermissionCoreConst.Tab}request_manager_personal_customer_idenfication`;
        public static readonly CoreButtonRequestManagerPersonalCustomerIdentificationAdd = `${PermissionCoreConst.Button}request_manager_personal_customer_identification_add`;
        public static readonly CoreTableRequestManagerPersonalCustomerIdentification = `${PermissionCoreConst.Table}request_manager_personal_customer_idenfication`;
        public static readonly CoreButtonRequestManagerPersonalCustomerIdentificationUpdate = `${PermissionCoreConst.Button}request_manager_personal_customer_identification_update`;
        public static readonly CoreButtonRequestManagerPersonalCustomerIdentificationSetDefault = `${PermissionCoreConst.Button}request_manager_personal_customer_identification_set_default`;

        public static readonly CoreButtonRequestManagerPersonalCustomerRequestUpdate = `${PermissionCoreConst.Button}request_manager_personal_customer_request_update`;
        public static readonly CoreButtonRequestManagerPersonalCustomerApprove = `${PermissionCoreConst.Button}request_manager_personal_customer_approve`;

        public static readonly CoreTabRequestManagerPersonalCustomerContact = `${PermissionCoreConst.Tab}request_personal_customer_contact`;

        public static readonly CoreButtonAddRequestManagerPersonalCustomerContactAddress = `${PermissionCoreConst.Button}add_request_manager_personal_customer_contact_address`;
        public static readonly CoreTabRequestManagerPersonalCustomerContactAddress = `${PermissionCoreConst.Tab}request_manager_personal_customer_contact_address`;
        public static readonly CoreTableRequestManagerPersonalCustomerContactAddress = `${PermissionCoreConst.Table}request_manager_personal_customer_contact_address`;
        public static readonly CoreButtonUpdateRequestManagerPersonalCustomerContactAddress = `${PermissionCoreConst.Button}update_request_manager_personal_customer_contact_address`;
        public static readonly CoreButtonSetDefaultRequestManagerPersonalCustomerContactAddress = `${PermissionCoreConst.Button}set_default_request_manager_personal_customer_contact_address`;

        // Người liên hệ khẩn cấp
        public static readonly CoreTabRequestPersonalCustomerEmergencyContact = `${PermissionCoreConst.Tab}request_personal_customer_emertgency_contact`;
        public static readonly CoreTabRequestPersonalCustomerEmergencyContactInfo = `${PermissionCoreConst.Tab}request_personal_customer_emertgency_contact_info`;
        public static readonly CoreButtonRequestPersonalCustomerEmergencyContactUpdate = `${PermissionCoreConst.Button}request_update_personal_customer_emertgency_contact`;

        // Người đại diện
        public static readonly CoreTabRequestPersonalCustomerRep = `${PermissionCoreConst.Tab}request_personal_customer_rep`;
        public static readonly CoreTabRequestPersonalCustomerRepInfo = `${PermissionCoreConst.Tab}request_personal_customer_rep_info`;
        public static readonly CoreButtonRequestPersonalCustomerRepUpdate = `${PermissionCoreConst.Button}request_update_personal_customer_rep`;

        public static readonly CoreTabRequestManagerPersonalCustomerReferral = `${PermissionCoreConst.Tab}request_manager_personal_customer_referral`;
        public static readonly CoreTableRequestManagerPersonalCustomerReferral = `${PermissionCoreConst.Table}request_manager_personal_customer_referral`;
        public static readonly CoreButtonRequestManagerPersonalCustomerReferral = `${PermissionCoreConst.Button}request_manager_personal_customer_referral`;
        public static readonly CoreButtonRequestManagerPersonalCustomerBack = `${PermissionCoreConst.Button}request_manager_personal_customer_back`;
        public static readonly CoreTableRequestManagerPersonalCustomerReferralPerson = `${PermissionCoreConst.Table}request_manager_personal_customer_referral_person`;
        public static readonly CoreButtonRequestManagerPersonalCustomerRequest = `${PermissionCoreConst.Button}request_manager_personal_customer_request`;   
        
        public static readonly CoreTabRequestManagerPersonalSale = `${PermissionCoreConst.Tab}request_manager_personal_customer_sale`;
        public static readonly CoreTableRequestManagerPersonalSale = `${PermissionCoreConst.Table}request_manager_personal_customer_sale`;
        public static readonly CoreButtonRequestManagerPersonalSaleAdd = `${PermissionCoreConst.Button}request_manager_personal_customer_sale_add`;
        public static readonly CoreButtonRequestManagerPersonalSaleSetDefault = `${PermissionCoreConst.Button}request_manager_personal_customer_sale_default`;
        //Khách hàng doanh nghiệp
        public static readonly CoreMenuRequestManagerBusinessCustomer = `${PermissionCoreConst.Menu}request_manager_business_customer`;
        public static readonly CoreTableRequestManagerBusinessCustomer = `${PermissionCoreConst.Table}request_manager_business_customer`;
        public static readonly CoreButtonRequestManagerBusinessCustomerDetail = `${PermissionCoreConst.Button}request_manager_business_customer_detail`;
        public static readonly CoreButtonRequestManagerBusinessCustomerApprove = `${PermissionCoreConst.Button}request_manager_business_customer_approve`;
        public static readonly CoreButtonRequestManagerBusinessCustomerRequestApprove = `${PermissionCoreConst.Button}request_manager_business_customer_request_approve`;
        public static readonly CoreButtonUpdateRequestManagerBusinessCustomer = `${PermissionCoreConst.Button}update_request_manager_business_customer`;
        public static readonly CoreTabRequestManagerBusinessCustomerDetail = `${PermissionCoreConst.Tab}request_manager_business_customer_detail`;

        public static readonly CoreTabRequestManagerBusinessCustomerBank = `${PermissionCoreConst.Tab}request_manager_business_customer_bank`;
        public static readonly CoreTableRequestManagerBusinessCustomerBank = `${PermissionCoreConst.Table}request_manager_business_customer_bank`;
        public static readonly CoreButtonAddRequestManagerBusinessCustomerBank = `${PermissionCoreConst.Button}add_request_manager_business_customer_bank`;
        public static readonly CoreButtonUpdateRequestManagerBusinessCustomerBank = `${PermissionCoreConst.Button}update_request_manager_business_customer_bank`;
        public static readonly CoreButtonRequestManagerBusinessCustomerBankDetail = `${PermissionCoreConst.Button}request_manager_business_customer_bank_detail`;
        public static readonly CoreButtonSetDefaultRequestManagerBusinessCustomerBank = `${PermissionCoreConst.Button}default_request_manager_business_customer_bank`;


        public static readonly CoreTabRequestManagerBusinessCustomerIdentification = `${PermissionCoreConst.Tab}request_manager_business_customer_idenfication`;
        public static readonly CoreTabRequestManagerBusinessCustomerContactAddress = `${PermissionCoreConst.Tab}request_manager_business_customer_contact_address`;
        public static readonly CoreTabRequestManagerBusinessCustomerReferral = `${PermissionCoreConst.Tab}request_manager_business_customer_referral`;
        public static readonly CoreButtonRequestManagerBusinessCustomerReferral = `${PermissionCoreConst.Button}request_manager_business_customer_referral`;
        public static readonly CoreButtonRequestManagerBusinessCustomerBack = `${PermissionCoreConst.Button}request_manager_business_customer_back`;
        public static readonly CoreButtonRequestManagerBusinessCustomerUpdate = `${PermissionCoreConst.Button}request_manager_business_customer_update`;
        public static readonly CoreButtonRequestManagerBusinessCustomerRequest = `${PermissionCoreConst.Button}request_manager_business_customer_request`;
    //#endregion

    //#region Quản lý phê duyệt
    public static readonly CoreMenuApproveManager = `${PermissionCoreConst.Menu}approve_manager`;

        //phê duyệt khách hàng cá nhân
        public static readonly CoreMenuApprovePersonalCustomerManagement = `${PermissionCoreConst.Menu}approve_personal_customer_manager`;

        public static readonly CoreTableApprovePersonalCustomer = `${PermissionCoreConst.Table}approve_personal_customer_list`;
        public static readonly CoreButtonApprovePersonalCustomerDetail = `${PermissionCoreConst.Button}approve_personal_customer_detail`;

        //Thông tin chi tiết phê duyệt khách hàng cá nhân
        public static readonly CoreTabApprovePersonalCustomerInfo = `${PermissionCoreConst.Tab}approve_personal_customer_info`;
        public static readonly CoreButtonApprovePersonalCustomer = `${PermissionCoreConst.Button}approve_personal_customer`;

        public static readonly CoreTabApprovePersonalCustomerBank = `${PermissionCoreConst.Tab}approve_personal_customer_bank`;
        public static readonly CoreTableApprovePersonalCustomerBank = `${PermissionCoreConst.Table}approve_personal_customer_bank`;
        public static readonly CoreButtonApprovePersonalCustomerBankDetail = `${PermissionCoreConst.Button}view_detail_approve_personal_customer_bank`;

        public static readonly CoreTabApprovePersonalCustomerIdentification = `${PermissionCoreConst.Tab}approve_personal_customer_identification`;
        public static readonly CoreTableApprovePersonalCustomerIdentification = `${PermissionCoreConst.Table}approve_personal_customer_identification`;
        //liên hệ
        public static readonly CoreTabApprovePersonalCustomerContact = `${PermissionCoreConst.Tab}approve_personal_customer_contact`;
        //địa chỉ liên hệ
        public static readonly CoreTabApprovePersonalCustomerAddress = `${PermissionCoreConst.Tab}approve_personal_customer_address`;
        public static readonly CoreTableApprovePersonalCustomerAddress = `${PermissionCoreConst.Table}approve_personal_customer_address`;
        public static readonly CoreButtonApprovePersonalCustomerAddressDetail = `${PermissionCoreConst.Button}view_detail_approve_personal_customer_address`;
        //người liên hệ khẩn cấp
        public static readonly CoreTabApprovePersonalCustomerEmergencyContact = `${PermissionCoreConst.Tab}approve_personal_customer_emergency`;

        public static readonly CoreTableApprovePersonalCustomerEmergencyContact = `${PermissionCoreConst.Table}approve_personal_customer_emergency`;
        //người đại diện
        public static readonly CoreTabApprovePersonalCustomerRep = `${PermissionCoreConst.Tab}approve_personal_customer_rep`;

        public static readonly CoreTableApprovePersonalCustomerRep = `${PermissionCoreConst.Table}approve_personal_customer_rep`;

        public static readonly CoreTabApprovePersonalCustomerReferal = `${PermissionCoreConst.Tab}approve_personal_customer_referal`;
        public static readonly CoreTableApprovePersonalCustomerReferal = `${PermissionCoreConst.Table}approve_personal_customer_referal`;
        public static readonly CoreTableApprovePersonalCustomerReferalPerson = `${PermissionCoreConst.Table}approve_personal_customer_referal_person`;
        public static readonly CoreButtonApprovePersonalCustomerReferalAdd = `${PermissionCoreConst.Button}add_approve_personal_customer_referal`;

        //tư vấn viên
        public static readonly CoreTabApprovePersonalSale = `${PermissionCoreConst.Tab}approve_personal_customer_sale`;
        public static readonly CoreTableApprovePersonalSale = `${PermissionCoreConst.Table}approve_personal_customer_sale`;
        //phê duyệt khách hàng doanh nghiệp
        public static readonly CoreMenuApproveBusinessCustomerManagement = `${PermissionCoreConst.Menu}approve_business_customer_manager`;
        public static readonly CoreTableApproveBusinessCustomer = `${PermissionCoreConst.Table}approve_business_customer_list`;
        public static readonly CoreButtonApproveBusinessCustomerDetail = `${PermissionCoreConst.Button}approve_business_customer_detail`;

        //Thông tin chi tiết phê duyệt khách hàng doanh nghiệp
        public static readonly CoreTabApproveBusinessCustomerInfo = `${PermissionCoreConst.Tab}approve_business_customer_info`;
        public static readonly CoreButtonApproveBusinessCustomer = `${PermissionCoreConst.Button}approve_business_customer`;
        public static readonly CoreTabApproveBusinessCustomerBank = `${PermissionCoreConst.Tab}approve_business_customer_bank`;
        public static readonly CoreTableApproveBusinessCustomerBank = `${PermissionCoreConst.Table}approve_business_customer_bank`;
        public static readonly CoreButtonApproveBusinessCustomerBankViewDetail = `${PermissionCoreConst.Button}view_detail_approve_business_customer_bank`;
        //báo cáo thống kê
        public static readonly CoreMenuReport = `${PermissionCoreConst.Menu}report`

        //báo cáo quản trị
        public static readonly CoreMenuManagementReport = `${PermissionCoreConst.Menu}management_report`;
        public static readonly CoreTabUserManagementReport = `${PermissionCoreConst.Tab}user_management_report`;
        public static readonly CoreTabCustomerManagementReport = `${PermissionCoreConst.Tab}customer_management_report`;
        //public static readonly CoreButtonManagementReportDownload = $"{Button}download_management_report";

        //báo cáo vận hành
        public static readonly CoreMenuOperationalReport = `${PermissionCoreConst.Menu}operational_report`;
        public static readonly CoreTabUserOperationalReport = `${PermissionCoreConst.Tab}user_operational_report`;
        public static readonly CoreTabCustomerOperationalReport = `${PermissionCoreConst.Tab}customer_operational_report`;
        //public const string CoreButtonOperationalReportDownload = $"{Button}download_operational_report";

        //báo cáo kinh doanh
        public static readonly CoreMenuBusinessReport = `${PermissionCoreConst.Menu}customer_business_report`;
        public static readonly CoreTabUserBusinessReport = `${PermissionCoreConst.Tab}user_business_report`;
        public static readonly CoreTabCustomerBusinessReport = `${PermissionCoreConst.Tab}customer_business_report`;
    //#endregion
}

