import { environment } from "projects/shared/environments/environment";
import { PermissionUserConst } from "./PermissionUser";
import { PermissionTypes, WebKeys } from "./permission.enum";

const ListWebConfig = {};

export class PermissionWebConst {
    private static readonly Web: string = "web_";
    public static readonly CoreModule: string = "core.";
    public static readonly InvestModule: string = "invest.";
    public static readonly UserModule: string = "user.";
    public static readonly SalerModule: string = "saler.";
    public static readonly HomeModule: string = "home.";

    public static readonly Home: string = `${PermissionWebConst.HomeModule}${PermissionWebConst.Web}`;
    public static readonly User: string = `${PermissionWebConst.UserModule}${PermissionWebConst.Web}`;
    public static readonly Core: string = `${PermissionWebConst.CoreModule}${PermissionWebConst.Web}`;
    public static readonly Saler: string = `${PermissionWebConst.SalerModule}${PermissionWebConst.Web}`;
    public static readonly Invest: string = `${PermissionWebConst.InvestModule}${PermissionWebConst.Web}`;
}

    ListWebConfig[PermissionWebConst.User] = {
        image: '',
        code: '',
        type: PermissionTypes.Web,
        webKey: WebKeys.User,
        permissionWeb: PermissionWebConst.User,
        permissionKeySetting: PermissionUserConst.UserPhanQuyen_User,
        name: 'MeeyUser',
        url: environment.baseUrlUser,
        description: 'Hệ thống quản trị phân quyền tập trung',
        createdDate: '03/11/2023',
        logo: 'shared/assets/layout/images/logo/logo-meeyfinance.svg',
    };

    ListWebConfig[PermissionWebConst.Core] = {
        image: '',
        code: '',
        type: PermissionTypes.Web,
        webKey: WebKeys.Core,
        permissionWeb: PermissionWebConst.Core,
		permissionKeySetting: PermissionUserConst.UserPhanQuyen_Core,
        name: 'MeeyCore',
        url: environment.baseUrlCore,
        description: 'Hệ thống quản trị thông tin khách hàng tập trung',
        createdDate: '03/11/2023',
        logo: 'shared/assets/layout/images/logo/logo-meeyfinance.svg'
    };

    ListWebConfig[PermissionWebConst.Invest] = {
        image: '',
        code: '',
        type: PermissionTypes.Web,
        webKey: WebKeys.Invest,
        permissionWeb: PermissionWebConst.Invest,
		permissionKeySetting: PermissionUserConst.UserPhanQuyen_Invest,
        name: 'MeeyInvest',
        url: environment.baseUrlInvest,
        description: 'Hệ thống quản trị sản phẩm đầu tư tài chính',
        createdDate: '03/11/2023',
        logo: 'shared/assets/layout/images/logo/logo-meeyfinance.svg'
    };

    ListWebConfig[PermissionWebConst.Saler] = {
        image: '',
        code: '',
        type: PermissionTypes.Web,
        webKey: WebKeys.Saler,
        permissionWeb: PermissionWebConst.Saler,
		permissionKeySetting: PermissionUserConst.UserPhanQuyen_Saler,
        name: 'MeeySaler',
        url: environment.baseUrlSaler,
        description: 'Hệ thống quản trị mạng lưới phòng giao dịch và tư vấn viên',
        createdDate: '08-06-2022',
        logo: 'shared/assets/layout/images/logo/logo-meeyfinance.svg'
    };

export default ListWebConfig;
