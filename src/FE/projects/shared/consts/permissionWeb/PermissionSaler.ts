export class PermissionSalerConst{
    private static readonly Menu = "menu_";
    private static readonly Tab = "tab_";
    private static readonly Table = "table_";
    private static readonly Button = "btn_";

    //Tổng quan
    public static readonly SaleMenuDashboard = `${PermissionSalerConst.Menu}sale_dashboad`;

    //#region Quản lý tư vấn viên
    //Menu quản lý tư vấn viên
    public static readonly SaleMenuSaleManagement = `${PermissionSalerConst.Menu}sale_management`;
        public static readonly SaleTableListSale = `${PermissionSalerConst.Table}list_sale`;
        public static readonly SaleButtonViewSaleInfo = `${PermissionSalerConst.Button}view_sale_info`;
        public static readonly SaleButtonAddSale = `${PermissionSalerConst.Button}add_sale`;
        public static readonly SaleButtonSaleStatus = `${PermissionSalerConst.Button}sale_change_status`;
        public static readonly SaleButtonSaleTransfer = `${PermissionSalerConst.Button}sale_transfer`;
        public static readonly SaleTabSaleInfo = `${PermissionSalerConst.Tab}sale_info`;
        public static readonly SaleButtonUpdateSaleInfo = `${PermissionSalerConst.Button}update_sale_info`;
        public static readonly SaleTabViewSaleInfo = `${PermissionSalerConst.Tab}view_sale_info`;

        //Quản lý đồng cộng tác của sale
        public static readonly SaleTabSaleCollabContract = `${PermissionSalerConst.Tab}sale_sale_collab_contract`;
        public static readonly SaleTableSaleCollabContract = `${PermissionSalerConst.Table}sale_sale_collab_contract`;
        public static readonly SaleButtonUpdateSaleCollabContract = `${PermissionSalerConst.Button}update_sale_collab_contract`;
        public static readonly SaleButtonSignSaleCollabContract = `${PermissionSalerConst.Button}sign_sale_collab_contract`;
        public static readonly SaleButtonDownloadWordSaleCollabContract = `${PermissionSalerConst.Button}download_word_sale_collab_contract`;
        public static readonly SaleButtonDownloadPdfSaleCollabContract = `${PermissionSalerConst.Button}download_pdf_sale_collab_contract`;
        public static readonly SaleButtonDownloadSignatureSaleCollabContract = `${PermissionSalerConst.Button}download_signature_sale_collab_contract`;
        public static readonly SaleButtonViewSaleCollabContract = `${PermissionSalerConst.Button}view_sale_collab_contract`;
    //#endregion
    
    //#region Quản lý phòng ban
    public static readonly SaleMenuDepartmentManagement = `${PermissionSalerConst.Menu}sale_department_management`;
    public static readonly SaleTableDepartmentManagement = `${PermissionSalerConst.Table}sale_department_management`;
    public static readonly SaleButtonAddDepartment = `${PermissionSalerConst.Button}add_department`;
    public static readonly SaleButtonAddDepartmentManager = `${PermissionSalerConst.Button}add_department_manager`;
    public static readonly SaleButtonChangeDepartmentManager = `${PermissionSalerConst.Button}change_department_manager`;
    public static readonly SaleButtonRemoveDepartmentManager = `${PermissionSalerConst.Button}remove_department_manager`;
    public static readonly SaleButtonUpdateDepartment = `${PermissionSalerConst.Button}update_department`;
    public static readonly SaleButtonTransferDepartment = `${PermissionSalerConst.Button}transfer_department`;
    public static readonly SaleButtonDeleteDepartment = `${PermissionSalerConst.Button}delete_department`;
    public static readonly SaleTableSaleInDepartment = `${PermissionSalerConst.Table}sale_in_department`;
    public static readonly SaleButtonDetailSaleInDepartment = `${PermissionSalerConst.Button}detail_sale_in_department`;
    public static readonly SaleButtonLockSaleInDepartment = `${PermissionSalerConst.Button}lock_sale_in_department`;
    //Tab con tư vấn viên thông tin tư vấn viên
    public static readonly SaleInDepartmentTabSaleInfo = `${PermissionSalerConst.Tab}sale_in_department_info`;
    public static readonly SaleInDepartmentButtonUpdateSaleInfo = `${PermissionSalerConst.Button}update_sale_in_department_info`;
    public static readonly SaleInDepartmentTabViewSaleInfo = `${PermissionSalerConst.Tab}view_sale_in_department_info`;
    //Tab con tư vấn viên hợp đồng cộng tác
    public static readonly SaleInDepartmentTabSaleCollabContract = `${PermissionSalerConst.Tab}sale_in_department_sale_collab_contract`;
    public static readonly SaleInDepartmentTableSaleCollabContract = `${PermissionSalerConst.Table}sale_in_department_sale_collab_contract`;
    public static readonly SaleInDepartmentButtonUpdateSaleCollabContract = `${PermissionSalerConst.Button}update_sale_in_department_collab_contract`;
    //#endregion

    //#region Quản lý yêu cầu
    public static readonly SaleMenuRequestManagement = `${PermissionSalerConst.Menu}sale_request_management`;
    public static readonly SaleMenuRequestSale = `${PermissionSalerConst.Menu}_request_sale`;
    public static readonly SaleTableListRequestSale = `${PermissionSalerConst.Table}list_request_sale`;
    public static readonly SaleButtonRequestSaleInfo = `${PermissionSalerConst.Button}request_sale_info`;
    public static readonly SaleButtonCreateRequestSaleInfo = `${PermissionSalerConst.Button}create_request_sale_info`;
    public static readonly SaleTabRequestSaleInfo = `${PermissionSalerConst.Tab}sale_request_info`;
    public static readonly SaleButtonRequestSale = `${PermissionSalerConst.Button}request_sale`;
    public static readonly SaleButtonUpdateRequestSale = `${PermissionSalerConst.Button}update_request_sale`;
    public static readonly SaleButtonApproveRequestSale = `${PermissionSalerConst.Button}approve_request_sale`;
    //#endregion

    //#region Quản lý phê duyệt
    public static readonly SaleMenuApproveManagement = `${PermissionSalerConst.Menu}sale_approve_management`;
    public static readonly SaleTableApproveSale = `${PermissionSalerConst.Table}approve_sale`;
    public static readonly SaleButtonApproveInfo = `${PermissionSalerConst.Button}approve_info`;
    public static readonly SaleTabApproveInfo = `${PermissionSalerConst.Tab}approve_info`;
    public static readonly SaleButtonApproveRequest = `${PermissionSalerConst.Button}approve_request`;
    //#endregion

    //#region Thiết lập
    public static readonly SaleMenuConfiguration = `${PermissionSalerConst.Menu}configuration`;
    //Thông báo hệ thống
    public static readonly SaleMenuConfigNotification = `${PermissionSalerConst.Menu}sale_config_notification`;
    public static readonly SaleTableConfigNotification = `${PermissionSalerConst.Table}sale_config_notification`;
    public static readonly SaleButtonUpdateConfigNotification = `${PermissionSalerConst.Button}update_sale_config_notification`;
    //Mẫu hợp đồng cộng tác
    public static readonly SaleMenuCollabContract = `${PermissionSalerConst.Menu}sale_collab_contract`;
    public static readonly SaleTableCollabContract = `${PermissionSalerConst.Table}sale_collab_contract`;
    public static readonly SaleButtonUploadCollabContract = `${PermissionSalerConst.Button}upload_collab_contract`;
    public static readonly SaleButtonUpdateCollabContract = `${PermissionSalerConst.Button}update_collab_contract`;
    public static readonly SaleButtonDeleteCollabContract = `${PermissionSalerConst.Button}delete_collab_contract`;
    public static readonly SaleButtonTestFillWordCollabContract = `${PermissionSalerConst.Button}fill_word_collab_contract`;
    public static readonly SaleButtonTestFillPdfCollabContract = `${PermissionSalerConst.Button}fill_pdf_collab_contract`;
    //#endregion

    // Báo cáo thống kê
    public static readonly SaleMenuExportReport = `${PermissionSalerConst.Menu}sale_export_report`;
    public static readonly SaleButtonListReportSaler = `${PermissionSalerConst.Button}sale_list_report_saler`;

}