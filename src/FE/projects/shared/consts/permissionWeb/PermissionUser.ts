import { PermissionTypes, WebKeys } from "./permission.enum";

const PermissionUserConfig = {};
export class PermissionUserConst {
    private static readonly Menu = "menu_";
    private static readonly Tab = "tab_";
    public static readonly Page: string = "page_";
    private static readonly Table = "table_";
    private static readonly Button = "btn_";
    public static readonly CoreModule: string = "core.";
    public static readonly InvestModule: string = "invest.";
    public static readonly UserModule: string = "user.";
    public static readonly SalerModule: string = "saler.";
    public static readonly HomeModule: string = "home.";

    //
    
    // PHÂN QUYỀN WEBSITE
    public static readonly UserPhanQuyen_Websites: string = `${PermissionUserConst.UserModule}${PermissionUserConst.Menu}phan_quyen_websites`;
    public static readonly UserDanhSach_Website: string = `${PermissionUserConst.UserModule}${PermissionUserConst.Menu}danh_sach_website`;

    public static readonly UserPhanQuyen_Home: string = `${PermissionUserConst.UserModule}${PermissionUserConst.HomeModule}${PermissionUserConst.Page}cau_hinh_nhom_quyen_home`;
	
    public static readonly UserPhanQuyen_User: string = `${PermissionUserConst.UserModule}${PermissionUserConst.Page}cau_hinh_nhom_quyen_user`;
	
    public static readonly UserPhanQuyen_Core: string = `${PermissionUserConst.UserModule}${PermissionUserConst.CoreModule}${PermissionUserConst.Page}cau_hinh_nhom_quyen_core`;
	
    public static readonly UserPhanQuyen_Saler: string = `${PermissionUserConst.UserModule}${PermissionUserConst.SalerModule}${PermissionUserConst.Menu}cau_hinh_nhom_quyen_saler`;
	
    public static readonly UserPhanQuyen_Invest: string = `${PermissionUserConst.UserModule}${PermissionUserConst.InvestModule}${PermissionUserConst.Page}cau_hinh_nhom_quyen_invest`;
   
   
    //Tổng quan
    public static readonly UserMenuDashboard = `${PermissionUserConst.Menu}user_menu_dashboard`;

    //Phân quyền website
    public static readonly UserMenuPermission = `${PermissionUserConst.Menu}user_permission`;
    public static readonly UserTablePermission = `${PermissionUserConst.Table}user_permission`;
    public static readonly UserButtonPermissionSetting = `${PermissionUserConst.Button}user_permission_setting`;
    public static readonly UserButtonPermissionAdd = `${PermissionUserConst.Button}user_permission_add`;
    public static readonly UserTableRolePermission = `${PermissionUserConst.Table}user_role_permission`;
    public static readonly UserButtonRolePermissionUpdate = `${PermissionUserConst.Button}user_role_permission_update`;
    public static readonly UserButtonRolePermissionLock = `${PermissionUserConst.Button}user_role_permission_lock`;
    public static readonly UserButtonRolePermissionDelete = `${PermissionUserConst.Button}user_role_permission_delete`;

    // Quản lý tài khoản
    public static readonly UserMenuAccountManager = `${PermissionUserConst.Menu}account_manager`;
    public static readonly UserButtonAccountManagerAdd = `${PermissionUserConst.Button}add_account`;
    public static readonly UserTableAccountManager = `${PermissionUserConst.Table}account`;
    public static readonly UserButtonAccountManagerUpdatePermission = `${PermissionUserConst.Button}update_permission_account`;
    public static readonly UserButtonAccountManagerUpdatePassword = `${PermissionUserConst.Button}update_password_account`;
    public static readonly UserButtonAccountManagerLock = `${PermissionUserConst.Button}lock_account`;
    public static readonly UserButtonAccountManagerDelete = `${PermissionUserConst.Button}delete_account`;

    // Báo cáo thống kê
    public static readonly UserMenuExportReport = `${PermissionUserConst.Menu}export_report`;
    public static readonly UserButtonAccountManagementReport = `${PermissionUserConst.Button}account_manager_report`;

}

export default PermissionUserConfig;
