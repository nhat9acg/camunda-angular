export class PermissionInvestConst{
    private static readonly Menu = `menu_`;
    private static readonly Tab = `tab_`;
    private static readonly Table = `table_`;
    private static readonly Button = `btn_`;

    public static readonly InvestMenuDashbroard = `${PermissionInvestConst.Menu}invest_dashboard`;
    //Quản lý sản phẩm
    public static readonly InvestMenuProductManager = `${PermissionInvestConst.Menu}invest_product_manager`; 
    public static readonly InvestMenuInvestmentProduct = `${PermissionInvestConst.Menu}invest_investment_product`;
    public static readonly InvestButtonAddInvestmentProduct = `${PermissionInvestConst.Button}invest_add_investment_product`;
    public static readonly InvestTableListInvestmentProduct = `${PermissionInvestConst.Table}invest_investment_product`;
    public static readonly InvestButtonInfoInvestmentProduct = `${PermissionInvestConst.Button}invest_info_investment_product`;
    public static readonly InvestButtonLockInvestmentProduct = `${PermissionInvestConst.Button}invest_lock_investment_product`;
    //Tab thông tin chung
    public static readonly InvestTabGeneralInfoInvestmentProduct = `${PermissionInvestConst.Tab}invest_general_info_investment_product`;
    public static readonly InvestButtonHighlightInvestmentProduct = `${PermissionInvestConst.Button}invest_highlight_investment_product`;
    public static readonly InvestButtonShowAppInvestmentProduct = `${PermissionInvestConst.Button}invest_show_app_investment_product`;
    public static readonly InvestButtonRequestInvestmentProduct = `${PermissionInvestConst.Button}invest_request_investment_product`;
    public static readonly InvestButtonUpdateGeneralInfoInvestmentProduct = `${PermissionInvestConst.Button}invest_update_general_info_investment_product`;
    public static readonly InvestTabUpdateGeneralInfoInvestmentProduct = `${PermissionInvestConst.Tab}invest_update_general_info_investment_product`;
    //Tab tổng quan
    public static readonly InvestTabOverviewInvestmentProduct = `${PermissionInvestConst.Tab}invest_overview_investment_product`;
    public static readonly InvestButtonUpdateOverviewInvestmentProduct = `${PermissionInvestConst.Button}invest_overview_investment_product`;
    public static readonly InvestTabViewOverviewInvestmentProduct = `${PermissionInvestConst.Tab}view_invest_overview_investment_product`;

    //Tab chính sách áp dụng
    public static readonly InvestTabPolicyApplyInvestmentProduct = `${PermissionInvestConst.Tab}invest_policy_apply_investment_product`;
    public static readonly InvestButtonAddPolicyApplyInvestmentProduct = `${PermissionInvestConst.Button}invest_add_policy_apply_investment_product`;
    public static readonly InvestTablePolicyApplyInvestmentProduct = `${PermissionInvestConst.Table}invest_policy_apply_investment_product`;
    public static readonly InvestButtonDetailPolicyApplyInvestmentProduct = `${PermissionInvestConst.Button}invest_detail_policy_apply_investment_product`;
    public static readonly InvestButtonActivePolicyApplyInvestmentProduct = `${PermissionInvestConst.Button}invest_active_policy_apply_investment_product`;
    //Tab hợp đồng mẫu
    public static readonly InvestTabContractTemplateInvestmentProduct = `${PermissionInvestConst.Tab}invest_contract_template_investment_product`;
    public static readonly InvestButtonAddContractTemplateInvestmentProduct = `${PermissionInvestConst.Button}invest_add_contract_template_investment_product`;
    public static readonly InvestTableContractTemplateInvestmentProduct = `${PermissionInvestConst.Table}invest_contract_template_investment_product`;
    public static readonly InvestButtonUpdateContractTemplateInvestmentProduct = `${PermissionInvestConst.Button}invest_update_contract_template_investment_product`;
    public static readonly InvestButtonFillPersonalContractTemplateInvestmentProduct = `${PermissionInvestConst.Button}invest_test_fill_personal_contract_template_investment_product`;
    public static readonly InvestButtonFillBusinessContractTemplateInvestmentProduct = `${PermissionInvestConst.Button}invest_test_fill_business_contract_template_investment_product`;
    public static readonly InvestButtonActiveContractTemplateInvestmentProduct = `${PermissionInvestConst.Button}invest_active_contract_template_investment_product`;
    //Tab mẫu giao nhận HĐ
    public static readonly InvestTabReceiveContractInvestmentProduct = `${PermissionInvestConst.Tab}invest_receive_contract_investment_product`;
    public static readonly InvestButtonAddReceiveContractInvestmentProduct = `${PermissionInvestConst.Button}invest_add_receive_contract_investment_product`;
    public static readonly InvestTableReceiveContractInvestmentProduct = `${PermissionInvestConst.Table}invest_receive_contract_investment_product`;
    public static readonly InvestButtonUpdateReceiveContractInvestmentProduct = `${PermissionInvestConst.Button}invest_update_receive_contract_investment_product`;
    public static readonly InvestButtonTestFillWordReceiveContractInvestmentProduct = `${PermissionInvestConst.Button}invest_fill_word_receive_contract_investment_product`;
    public static readonly InvestButtonTestFillPdfReceiveContractInvestmentProduct = `${PermissionInvestConst.Button}invest_fill_pdf_receive_contract_investment_product`;
    public static readonly InvestButtonActiveReceiveContractInvestmentProduct = `${PermissionInvestConst.Button}invest_active_receive_contract_investment_product`;


    //Quản lý phê duyệt
    public static readonly InvestMenuApproveManager = `${PermissionInvestConst.Menu}approve_manager`;
    //Phê duyệt đầu tư
    public static readonly InvestMenuApproveInvestment = `${PermissionInvestConst.Menu}approve_investment`;
    public static readonly InvestTableApproveInvestment = `${PermissionInvestConst.Table}approve_investment`;
    public static readonly InvestButtonDetailApproveInvestment = `${PermissionInvestConst.Button}detail_approve_investment`;
    //Tab thông tin chung
    public static readonly InvestTabGeneralInfoApproveInvestment = `${PermissionInvestConst.Tab}general_info_approve_investment`;
    public static readonly InvestButtonHanleRequestApproveInvestment = `${PermissionInvestConst.Button}handle_request_approve_investment`;
    //Tab tổng quan
    public static readonly InvestTabOverviewApproveInvestment = `${PermissionInvestConst.Tab}overview_approve_investment`;
    // public static readonly InvestTableDescriptionApproveInvestment = `${PermissionInvestConst.Table}description_approve_investment`;
    // public static readonly InvestTabViewProjectOrganizationApproveInvestment = `${PermissionInvestConst.Tab}view_project_organization_approve_investment`;
    // public static readonly InvestTableProjectOrganizationApproveInvestment = `${PermissionInvestConst.Table}project_organization_approve_investment`;
    // public static readonly InvestButtonDetailProjectOrganizationApproveInvestment = `${PermissionInvestConst.Button}detail_project_organization_approve_investment`;
    // public static readonly InvestTableProjectFileApproveInvestment = `${PermissionInvestConst.Table}project_file_approve_investment`;
    // public static readonly InvestTableProjectMediaApproveInvestment = `${PermissionInvestConst.Table}project_media_approve_investment`;
    //Tab chính sách áp dụng
    public static readonly InvestTabPolicyApplyApproveInvestment = `${PermissionInvestConst.Tab}poliy_apply_approve_investment`;
    public static readonly InvestTablePolicyApplyApproveInvestment = `${PermissionInvestConst.Table}poliy_apply_approve_investment`;
    public static readonly InvestButtonDetailPolicyApplyApproveInvestment = `${PermissionInvestConst.Button}detail_poliy_apply_approve_investment`;
    //Tab hợp đồng mẫu
    public static readonly InvestTabContractTemplateApproveInvestment = `${PermissionInvestConst.Tab}contract_template_approve_investment`;
    public static readonly InvestTableContractTemplateApproveInvestment = `${PermissionInvestConst.Table}contract_template_approve_investment`;
    public static readonly InvestButtonFillPersonalContractTemplateApproveInvestment = `${PermissionInvestConst.Button}fill_personal_contract_template_approve_investment`;
    public static readonly InvestButtonFillBusinessContractTemplateApproveInvestment = `${PermissionInvestConst.Button}fill_business_contract_template_approve_investment`;
    //Tab mẫu giao nhận HĐ
    public static readonly InvestTabReceiveContractApproveInvestment = `${PermissionInvestConst.Tab}invest_receive_contract_approve_investment`;
    public static readonly InvestButtonAddReceiveContractApproveInvestment = `${PermissionInvestConst.Button}invest_add_receive_contract_approve_investment`;
    public static readonly InvestTableReceiveContractApproveInvestment = `${PermissionInvestConst.Table}invest_receive_contract_approve_investment`;
    public static readonly InvestButtonUpdateReceiveContractApproveInvestment = `${PermissionInvestConst.Button}invest_update_receive_contract_approve_investment`;
    public static readonly InvestButtonTestFillWordReceiveContractApproveInvestment = `${PermissionInvestConst.Button}invest_fill_word_receive_contract_approve_investment`;
    public static readonly InvestButtonTestFillPdfReceiveContractApproveInvestment = `${PermissionInvestConst.Button}invest_fill_pdf_receive_contract_approve_investment`;
    public static readonly InvestButtonActiveReceiveContractApproveInvestment = `${PermissionInvestConst.Button}invest_active_receive_contract_approve_investment`;



    //Hợp đồng đầu tư
    public static readonly InvestMenuContractInvestment = `${PermissionInvestConst.Menu}invest_contract_investment`;
    //Menu sổ lệnh
    public static readonly InvestMenuOrder = `${PermissionInvestConst.Menu}invest_order`;
    public static readonly InvestButtonAddOrder = `${PermissionInvestConst.Button}add_invest_order`;
    public static readonly InvestTableListOrder = `${PermissionInvestConst.Table}list_invest_order`;
    public static readonly InvestButtonDetailOrder = `${PermissionInvestConst.Button}detail_invest_order`;
    public static readonly InvestButtonRemoveOrder = `${PermissionInvestConst.Button}remove_invest_order`;
    //Tab thông tin chung
    public static readonly InvestTabGeneralInfoOrder = `${PermissionInvestConst.Tab}general_info_invest_order`;
    public static readonly InvestButtonUpdateGeneralInfoOrder = `${PermissionInvestConst.Button}update_general_info_invest_order`;
    public static readonly InvestTabViewGeneralInfoOrder = `${PermissionInvestConst.Tab}view_general_info_invest_order`;
    //Tab thanh toán
    public static readonly InvestTabPaymentOrder = `${PermissionInvestConst.Tab}payment_order`;
    public static readonly InvestTablePaymentOrder = `${PermissionInvestConst.Table}payment_invest_order`;
    public static readonly InvestButtonAddPaymentOrder = `${PermissionInvestConst.Button}add_payment_invest_order`;
    public static readonly InvestButtonDetailPaymentOrder = `${PermissionInvestConst.Button}detail_payment_invest_order`;
    public static readonly InvestButtonApprovePaymentOrder = `${PermissionInvestConst.Button}approve_payment_invest_order`;
    public static readonly InvestButtonRemovePaymentOrder = `${PermissionInvestConst.Button}remove_payment_invest_order`;
    //Tab hợp đồng
    public static readonly InvestTabContract = `${PermissionInvestConst.Tab}invest_contract`;
    public static readonly InvestButtonESignaltureContract = `${PermissionInvestConst.Button}esignalture_invest_contract`;
    public static readonly InvestButtonUpdateContract = `${PermissionInvestConst.Button}update_invest_contract`;
    public static readonly InvestTableContract = `${PermissionInvestConst.Table}invest_contract`;
    public static readonly InvestButtonApproveContract = `${PermissionInvestConst.Button}approve_contract_invest_contract`;
    //Tab dòng tiền
    public static readonly InvestTabCashFlow = `${PermissionInvestConst.Tab}invest_cash_flow`;
    public static readonly InvestTableCashFlow = `${PermissionInvestConst.Table}invest_cash_flow`;

    //menu hợp đồng
    public static readonly InvestMenuContract = `${PermissionInvestConst.Menu}invest_contract`;
    public static readonly InvestTableListContract = `${PermissionInvestConst.Table}list_invest_contract`;
    public static readonly InvestButtonDetailContract = `${PermissionInvestConst.Button}detail_invest_contract`;
    public static readonly InvestButtonReceiveContract = `${PermissionInvestConst.Button}receive_invest_contract`;
    public static readonly InvestButtonBlockadeContract = `${PermissionInvestConst.Button}blockade_invest_contract`;
    //Tab thông tin chung
    public static readonly InvestTabGeneralInfoContract = `${PermissionInvestConst.Tab}general_info_invest_contract`;
    public static readonly InvestButtonUpdateCustomerInfoContract = `${PermissionInvestConst.Button}update_customer_info_invest_contract`;
    public static readonly InvestButtonUpdateSaleInfoContract = `${PermissionInvestConst.Button}update_sale_invest_contract`;
    public static readonly InvestButtonUpdateNoteInfoContract = `${PermissionInvestConst.Button}update_note_invest_contract`;
    public static readonly InvestTabViewGeneralInfoContract = `${PermissionInvestConst.Tab}view_general_info_invest_contract`;
    //Tab thanh toán
    public static readonly InvestTabPaymentContract = `${PermissionInvestConst.Tab}payment_invest_contract`;
    public static readonly InvestTablePaymentContract = `${PermissionInvestConst.Table}payment_invest_contract`;
    public static readonly InvestButtonDetailPaymentContract = `${PermissionInvestConst.Button}detail_payment_invest_contract`;
    //Tab hợp đồng
    public static readonly InvestTabContractInInvestmentContract = `${PermissionInvestConst.Tab}contract_invest_contract`;
    public static readonly InvestTableContractInInvestmentContract = `${PermissionInvestConst.Table}contract_invest_contract`;

    public static readonly InvestButtonESignaltureContractInInvestmentContract = `${PermissionInvestConst.Button}esignalture_contract_invest_contract`;
    public static readonly InvestButtonCancelApproveContractInInvestmentContract = `${PermissionInvestConst.Button}cancel_approve_contract_invest_contract`;
    public static readonly InvestButtonReceiveContractInInvestmentContract = `${PermissionInvestConst.Button}receive_contract_invest_contract`;
    //Tab dòng tiền
    public static readonly InvestTabCashFlowContract = `${PermissionInvestConst.Tab}cash_flow_invest_contract`;
    public static readonly InvestTableCashFlowContract = `${PermissionInvestConst.Table}cash_flow_invest_contract`;

    //Menu chi trả lợi tức
    public static readonly InvestMenuInterestPayment = `${PermissionInvestConst.Menu}invest_payment_income`;
    public static readonly InvestButtonExportExcelInterestPayment = `${PermissionInvestConst.Button}export_excel_invest_payment_income`;
    public static readonly InvestButtonCreateListInterestPayment = `${PermissionInvestConst.Button}create_list_invest_payment_income`;
    public static readonly InvestButtonDetailInterestPayment = `${PermissionInvestConst.Button}detail_invest_payment_income`;
    public static readonly InvestButtonApproveAutoInterestPayment = `${PermissionInvestConst.Button}approve_auto_invest_payment_income`;
    public static readonly InvestButtonApproveManualInterestPayment = `${PermissionInvestConst.Button}approve_manual_invest_payment_income`;
    public static readonly InvestButtonCancelInterestPayment = `${PermissionInvestConst.Button}cancel_invest_payment_income`;
    public static readonly InvestTableInterestPayment = `${PermissionInvestConst.Table}invest_payment_income`;
    //Tab thông tin chung
    public static readonly InvestTabGeneralInfoInterestPayment = `${PermissionInvestConst.Tab}general_info_invest_payment_income`;
    public static readonly InvestButtonUpdateCustomerInfoInterestPayment = `${PermissionInvestConst.Button}update_customer_info_invest_payment_income`;
    public static readonly InvestButtonUpdateSaleInterestPayment = `${PermissionInvestConst.Button}update_sale_invest_payment_income`;
    public static readonly InvestButtonUpdateNoteInterestPayment = `${PermissionInvestConst.Button}update_note_invest_payment_income`;
    public static readonly InvestTabViewGeneralInfoInterestPayment = `${PermissionInvestConst.Tab}view_general_info_invest_payment_income`;
    //Tab thanh toán
    public static readonly InvestTabPaymentInInterestPayment = `${PermissionInvestConst.Tab}payment_in_invest_payment_income`;
    public static readonly InvestTablePaymentInInterestPayment = `${PermissionInvestConst.Table}payment_in_invest_payment_income`;
    public static readonly InvestButtonDetailPaymentInInterestPayment = `${PermissionInvestConst.Button}detail_payment_in_invest_payment_income`;
    //Tab Hợp đồng
    public static readonly InvestTabContractInterestPayment = `${PermissionInvestConst.Tab}contract_invest_payment_income`;
    public static readonly InvestButtonESignaltureContractInterestPayment = `${PermissionInvestConst.Button}esignalture_contract_invest_payment_income`;
    public static readonly InvestButtonCancelContractInterestPayment = `${PermissionInvestConst.Button}cancel_contract_invest_payment_income`;
    public static readonly InvestButtonReceiveContractInterestPayment = `${PermissionInvestConst.Button}receive_contract_invest_payment_income`;
    public static readonly InvestTableContractInterestPayment = `${PermissionInvestConst.Table}contract_invest_payment_income`;
    //Tab dòng tiền
    public static readonly InvestTabCashFlowInterestPayment = `${PermissionInvestConst.Tab}cash_flow_invest_payment_income`;
    public static readonly InvestTableCashFlowInterestPayment = `${PermissionInvestConst.Table}cash_flow_invest_payment_income`;

    //Menu chi trả đáo hạn
    public static readonly InvestMenuPaymentMaturity = `${PermissionInvestConst.Menu}invest_payment_maturity`;
    public static readonly InvestButtonExportExcelPaymentMaturity = `${PermissionInvestConst.Button}export_excel_invest_payment_maturity`;
    public static readonly InvestButtonCreateListPaymentMaturity = `${PermissionInvestConst.Button}create_list_invest_payment_maturity`;
    public static readonly InvestButtonDetailPaymentMaturity = `${PermissionInvestConst.Button}detail_invest_payment_maturity`;
    public static readonly InvestButtonApproveAutoPaymentMaturity = `${PermissionInvestConst.Button}approve_auto_invest_payment_maturity`;
    public static readonly InvestButtonApproveManualPaymentMaturity = `${PermissionInvestConst.Button}approve_manual_invest_payment_maturity`;
    public static readonly InvestButtonCancelPaymentMaturity = `${PermissionInvestConst.Button}cancel_invest_payment_maturity`;
    public static readonly InvestTablePaymentMaturity = `${PermissionInvestConst.Table}invest_payment_maturity`;
    //Tab thông tin chung
    public static readonly InvestTabGeneralInfoPaymentMaturity = `${PermissionInvestConst.Tab}general_info_invest_payment_maturity`;
    public static readonly InvestButtonUpdateCustomerInfoPaymentMaturity = `${PermissionInvestConst.Button}update_customer_info_invest_payment_maturity`;
    public static readonly InvestButtonUpdateSalePaymentMaturity = `${PermissionInvestConst.Button}update_sale_invest_payment_maturity`;
    public static readonly InvestButtonUpdateNotePaymentMaturity = `${PermissionInvestConst.Button}update_note_invest_payment_maturity`;
    public static readonly InvestTabViewGeneralInfoPaymentMaturity = `${PermissionInvestConst.Tab}view_general_info_invest_payment_maturity`;
    //Tab thanh toán
    public static readonly InvestTabPaymentInPaymentMaturity = `${PermissionInvestConst.Tab}payment_in_invest_payment_maturity`;
    public static readonly InvestTablePaymentInPaymentMaturity = `${PermissionInvestConst.Table}payment_in_invest_payment_maturity`;
    public static readonly InvestButtonDetailPaymentInPaymentMaturity = `${PermissionInvestConst.Button}detail_payment_in_invest_payment_maturity`;
    //Tab Hợp đồng
    public static readonly InvestTabContractPaymentMaturity = `${PermissionInvestConst.Tab}contract_invest_payment_maturity`;
    public static readonly InvestButtonESignaltureContractPaymentMaturity = `${PermissionInvestConst.Button}esignalture_contract_invest_payment_maturity`;
    public static readonly InvestButtonCancelContractPaymentMaturity = `${PermissionInvestConst.Button}cancel_contract_invest_payment_maturity`;
    public static readonly InvestButtonReceiveContractPaymentMaturity = `${PermissionInvestConst.Button}receive_contract_invest_payment_maturity`;
    public static readonly InvestTableContractPaymentMaturity = `${PermissionInvestConst.Table}contract_invest_payment_maturity`;
    //Tab dòng tiền
    public static readonly InvestTabCashFlowPaymentMaturity = `${PermissionInvestConst.Tab}cash_flow_invest_payment_maturity`;
    public static readonly InvestTableCashFlowPaymentMaturity = `${PermissionInvestConst.Table}cash_flow_invest_payment_maturity`;


    //Menu giao nhận hợp đồng
    public static readonly InvestMenuDeliveryContract = `${PermissionInvestConst.Menu}invest_delivery_contract`;
    public static readonly InvestButtonDowloadDeliveryContract = `${PermissionInvestConst.Button}invest_download_delivery_contract`;
    public static readonly InvestTableDeliveryContract = `${PermissionInvestConst.Table}invest_delivery_contract`;
    public static readonly InvestButtonDetailDeliveryContract = `${PermissionInvestConst.Button}invest_detail_delivery_contract`;
    public static readonly InvestButtonDownloadFileDeliveryContract = `${PermissionInvestConst.Button}invest_download_file_delivery_contract`;
    public static readonly InvestButtonUpdateStatusDeliveryContract = `${PermissionInvestConst.Button}invest_update_status_delivery_contract`;
    //Tab thông tin chung
    public static readonly InvestTabGeneralInfoDeliveryContract = `${PermissionInvestConst.Tab}invest_general_info_delivery_contract`;
    public static readonly InvestButtonUpdateCustomerInfoDeliveryContract = `${PermissionInvestConst.Button}update_customer_info_invest_delivery_contract`;
    public static readonly InvestButtonUpdateSaleDeliveryContract = `${PermissionInvestConst.Button}update_sale_invest_delivery_contract`;
    public static readonly InvestButtonUpdateNoteDeliveryContract = `${PermissionInvestConst.Button}update_note_invest_delivery_contract`;
    public static readonly InvestTabViewGeneralInfoDeliveryContract = `${PermissionInvestConst.Tab}view_general_info_invest_delivery_contract`;
    //Tab hợp đồng
    public static readonly InvestTabContractDeliveryContract = `${PermissionInvestConst.Tab}invest_contract_delivery_contract`;
    public static readonly InvestButtonESignaltureContractDeliveryContract = `${PermissionInvestConst.Button}esignalture_contract_invest_delivery_contract`;
    public static readonly InvestButtonCancelContractDeliveryContract = `${PermissionInvestConst.Button}cancel_contract_invest_delivery_contract`;
    public static readonly InvestButtonReceiveContractDeliveryContract = `${PermissionInvestConst.Button}receive_contract_invest_delivery_contract`;
    public static readonly InvestTableContractDeliveryContract = `${PermissionInvestConst.Table}contract_invest_delivery_contract`;


    //Menu phong tỏa, giải tỏa
    public static readonly InvestMenuBlockadeLiberation = `${PermissionInvestConst.Menu}invest_blockade_liberation`;
    public static readonly InvestTableBlockadeLiberation = `${PermissionInvestConst.Table}invest_blockade_liberation`;
    public static readonly InvestButtonDetailBlockadeLiberation = `${PermissionInvestConst.Button}invest_detail_blockade_liberation`;
    public static readonly InvestButtonRelieveBlockadeLiberation = `${PermissionInvestConst.Button}invest_relieve_blockade_liberation`;
    public static readonly InvestButtonUpdateRelieveBlockadeLiberation = `${PermissionInvestConst.Button}update_invest_relieve_blockade_liberation`;

    //Thiết lập
    public static readonly InvestMenuSetting = `${PermissionInvestConst.Menu}invest_setting`;
    //Menu ngày nghi lễ
    public static readonly InvestMenuHolidaySetting = `${PermissionInvestConst.Menu}invest_holiday_setting`;
    public static readonly InvestTableHolidaySetting = `${PermissionInvestConst.Table}invest_holiday_setting`;
    public static readonly InvestButtonUpdateHolidaySetting = `${PermissionInvestConst.Button}update_invest_holiday_setting`;
    //Menu chính sách mẫu
    public static readonly InvestMenuPolicyTemp = `${PermissionInvestConst.Menu}invest_policy_temp`;
    public static readonly InvestButtonAddPolicyTemp = `${PermissionInvestConst.Button}add_invest_policy_temp`;
    public static readonly InvestTablePolicyTemp = `${PermissionInvestConst.Table}invest_policy_temp`;
    public static readonly InvestButtonDetailPolicyTemp = `${PermissionInvestConst.Button}invest_detail_policy_temp`;
    public static readonly InvestButtonAddPolicyDetailTemp = `${PermissionInvestConst.Button}add_invest_policy_detail_temp`;
    public static readonly InvestButtonActiveDetailTemp = `${PermissionInvestConst.Button}active_invest_policy_temp`;
    public static readonly InvestButtonRemoveDetailTemp = `${PermissionInvestConst.Button}remove_invest_temp`;
    //Tab thông tin chính sách
    public static readonly InvestButtonUpdatePolicyTempInfo = `${PermissionInvestConst.Button}invest_update_policy_temp_info`;
    public static readonly InvestTabPolicyTempInfo = `${PermissionInvestConst.Tab}invest_policy_temp_info`;
    public static readonly InvestButtonAddPolicyDetailTempInfo = `${PermissionInvestConst.Button}invest_add_policy_detail_temp_info`;
    public static readonly InvestTablePolicyDetailTempInfo = `${PermissionInvestConst.Table}invest_policy_detail_temp_info`;
    public static readonly InvestButtonUpdatePolicyDetailTempInfo = `${PermissionInvestConst.Button}invest_update_policy_detail_temp_info`;
    public static readonly InvestButtonActivePolicyDetailTempInfo = `${PermissionInvestConst.Button}invest_active_policy_detail_temp_info`;
    public static readonly InvestButtonRemovePolicyDetailTempInfo = `${PermissionInvestConst.Button}invest_remove_policy_detail_temp_info`;
    //Menu thông báo hệ thống
    public static readonly InvestMenuNotification = `${PermissionInvestConst.Menu}invest_notification`;
    public static readonly InvestButtonUpdateNotification = `${PermissionInvestConst.Button}invest_update_notification`;
    public static readonly InvestTabNotification = `${PermissionInvestConst.Tab}invest_notification`;


    //Menu báo cáo thống kê
    public static readonly InvestRepostAnalysisMenu = `${PermissionInvestConst.Menu}invest_report_analysis`;
    public static readonly InvestReportManagementMenu = `${PermissionInvestConst.Menu}invest_report_management`;
    public static readonly InvestReportManagementButtonDownloadExcel = `${PermissionInvestConst.Button}invest_report_management_download_excel`;
    public static readonly InvestReportOperationMenu = `${PermissionInvestConst.Menu}invest_report_operation`;
    public static readonly InvestReportOperationButtonDownloadExcel = `${PermissionInvestConst.Menu}invest_report_operation_download_excel`;
    public static readonly InvestReportBusinessMenu = `${PermissionInvestConst.Menu}invest_report_business`;
    public static readonly InvestReportBusinessButtonDownloadExcel = `${PermissionInvestConst.Menu}invest_report_business_download_excel`;

}