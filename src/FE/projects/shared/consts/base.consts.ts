import { Confirmation } from "primeng/api";

export enum EStatusResonse {
    SUCCESS = 1,
    ERROR = 0,
}

export enum EYesNo {
    YES = "Y",
    NO = "N",
}

export enum EActiveDeactive {
    ACTIVE = "A",
    DEACTIVE = "D",
}

export enum EUnitTime {
    DAY = 'D',  // NGÀY
    MONTH = 'M', // THÁNG
    YEAR = 'Y', // NĂM
    QUARTER = 'Q', // QUÝ
}

export enum EAction {
    ADD = 1,
    UPDATE = 2,
    DELETE = 3,
}

export enum EFormatDate {
    DATE_TIME_SECOND = 'DD-MM-YYYY HH:mm:ss',
    DATE_TIME = 'DD-MM-YYYY HH:mm',
    DATE = 'DD-MM-YYYY',
    DATE_DMY_Hms = 'DD-MM-YYYY HH:mm:ss',
    DATE_DMY_Hm = 'DD-MM-YYYY HH:mm',
    DATE_DMY = 'DD-MM-YYYY',
    DATE_YMD_Hms = 'YYYY-MM-DDTHH:mm:ss',
}

export enum EFormatDateDisplay {
    DATE_TIME_SECOND = 'DD/MM/YYYY HH:mm:ss',
    DATE_TIME = 'DD/MM/YYYY HH:mm',
    DATE = 'DD/MM/YYYY',
    DATE_DMY_Hms = 'DD/MM/YYYY HH:mm:ss',
    DATE_DMY_Hm = 'DD/MM/YYYY HH:mm',
    DATE_DMY = 'DD/MM/YYYY',
    DATE_YMD_Hms = 'YYYY/MM/DDTHH:mm:ss',
}

export enum EIconConfirm {
    APPROVE = '/shared/assets/layout/images/icon-dialog-confirm/approve.svg',
    DELETE = '/shared/assets/layout/images/icon-dialog-confirm/delete.svg',
    WARNING = '/shared/assets/layout/images/icon-dialog-confirm/warning.svg',
    QUESTION = '/shared/assets/layout/images/icon-dialog-confirm/question.svg',
}

export enum EContentTypeEView {
    MARKDOWN = 'MARKDOWN',
    HTML = 'HTML',
    IMAGE = "IMAGE",
    FILE = "FILE",
}

export enum EAcceptFile {
    ALL = '',
    IMAGE = 'image',
    VIDEO = 'video',
    MEDIA = 'media',
}

export enum ETypeHandleLinkYoutube {
    CHECK_LINK = 'CHECK_LINK',
    GET_ID = 'GET_ID',
    GET_EMBED_LINK = 'GET_EMBED_LINK',
    GET_WATCH_LINK = 'GET_WATCH_LINK',
}

export enum ETypeUrlYoutube {
    WATCH = 'https://www.youtube.com/watch',
    LIVE = 'https://www.youtube.com/live',
    SHORT = 'https://youtu.be',
}

export enum ERedirectActiveTabNames  {
  PRODUCT_DISTRIBUTION_DETAIL_PRODUCT_LIST = 'productList',
  OPEN_SELL_DETAIL_PRODUCT_LIST = 'productList',
}

export class BaseConsts {
    /* Variable readonly  */
    static readonly imageExtensions = ['jpg', 'jpeg', 'png', 'bmp'];
    static readonly videoExtensions = ['webm', 'mkv', 'flv', 'vob', 'ogv', 'ogg', 'rrc', 'gifv', 'mng', 'mov', 'avi', 'qt', 'wmv', 'yuv', 'rm', 'asf', 'amv', 'mp4', 'm4p', 'm4v', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'm4v', 'svi', '3gp', '3g2', 'mxf', 'roq', 'nsv', 'flv', 'f4v', 'f4p', 'f4a', 'f4b', 'mod'];
    //
    static readonly localStorageUser = 'userInfo';
    static readonly pageContentId = "page-content";

    public static backLinkParam = 'backLink';
    public static redirectLoginPath = '/home'//'/auth/login/redirect';
    public static methodApiPost = ['POST', 'PUT', 'PATCH'];

    static readonly messageError = "Có lỗi xảy ra. Vui lòng thử lại sau ít phút!";

    static readonly authorization = {
        accessToken: 'access_token',
        refreshToken: 'refresh_token',
        state: 'state',
        codeVerifier: 'code_verifier',
    };

    static defaultAvatar = "shared/assets/layout/images/avatar/anonymous-avatar.jpg";

    static readonly DEBOUNCE_TIME = 1200;

    static readonly messageOnUpload = "Đang upload file ..."
    static readonly emptyMessage = "Không có dữ liệu"


    static redicrectHrefOpenDocs = "https://docs.google.com/viewerng/viewer?url=";

    static keyCrypt = 'idCrypt';

    static pdfType = 'application/pdf';
    static docxType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    static docType = 'application/msword';
    static excelType = 'application/vnd.ms-excel';


    static readonly OPTIONS_MIN_LENGTH_SHOW_FILTER = 5;
    static readonly PAGINATOR_MIN_LENGTH_SHOW = 25;

    static readonly maxNumberInput = 999999999999999;

    static readonly exceptionErrorCodeNothingPass = [4104];

}

export class LocalStorageName {
  static token = 'token';
  static user = 'user';
  static refreshToken = 'refreshToken';
}


export class StatusResponseConst {
  public static list = [
      {
          value: false,
          status: 0,
      },
      {
          value: true,
          status: 1,
      },
  ]

  public static RESPONSE_TRUE = 1;
  public static RESPONSE_FALSE = 0;

}

export class ActiveDeactiveConst {
    public static ACTIVE = 1;
    public static DEACTIVE = 2;

    public static list = [
        {
            name: 'Kích hoạt',
            code: this.ACTIVE,
            class: 'tag-active',
        },
        {
            name: 'Đang khóa',
            code: this.DEACTIVE,
            class: 'tag-lock',
            isDisableCheckbox: true
        }
    ];



    public static getInfo(code, atribution = null) {
        let status = this.list.find(type => type.code == code);
        return status && atribution ? status[atribution] : status;
    }
}

export class StatusDeleteConst {
    public static list = [
        {
            name: 'Đã xóa',
            code: 'Y',
        },
        {
            name: 'Chưa xóa',
            code: 'N',
        },
    ]

    public static DELETE_TRUE = 'Y';
    public static DELETE_FALSE = 'N';
}

export const AtributionConfirmConst: Confirmation = {
    header: 'Thông báo!',
    icon: 'pi pi-exclamation-triangle',
    acceptLabel: 'Đồng ý',
    rejectLabel: 'Hủy',
}

export class SortConst {
    public static ASCENDING = 1;
    public static DESCENDING = -1;

    public static listSort = [
        {
            code: this.ASCENDING,
            value: 'asc',
        },
        {
            code: this.DESCENDING,
            value: 'desc',
        },
    ];

    public static getValueSort(code){
        const sort = this.listSort.find(s => s.code == code);
        return sort ? sort.value : null;
    }
}

export class MarkDownHtmlConst{
    public static HTML = 1;
    public static MARKDOWN = 2;

    public static types = [
        {
            code: 1,
            value: 'HTML'
        },
        {
            code: 2,
            value: 'MARKDOWN'
        }
    ];

    public static getType(code){
        const type = this.types.find(s => s.code == code);
        return type ? type.value : '';
    };

    public static getValue(value){
        const type = this.types.find(s => s.value == value);
        return type ? type.code : '';
    }
}

export class TabView {
    public static FIRST = 0;
    public static SECOND = 1;
    public static THIRD = 2;
    public static FOURTH = 3;
    public static FIFTH = 4;
    public static SIXTH = 5;
    public static SEVENTH = 6;
    public static EIGHTH = 7;
    public static NINTH = 8;
    public static TENTH = 9;
}
