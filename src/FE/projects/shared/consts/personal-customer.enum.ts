export enum EPersonalCustomerStatusTemp {
    INACTIVE = 1,
    PENDING_APPROVAL = 2,
    ACTIVE = 3,
    SUSPENDED = 4,
}

export enum EPersonalCustomerStatus {
    ACTIVE = 1,
    INACTIVE,
    UPDATETING,
}

export enum EIdentificationTypes {
    PASSPORT = 'passport',
    CMND = 'cmnd',
    CCCD = 'cccd',
    CCCD_CHIP = 'cccd_chip'
}

export enum ESex {
    MALE = 1,
    FEMALE = 2,
}

export enum EApproveAction{
    ADD = 1,
    UPDATE = 2,
    DELETE = 3
}

export enum EApproveStatus{
    PENIDNG = 1,
    APPROVE = 2,
    CANCEL = 3
}

export enum ESaleStatus{
    ACIVE = 1,
    DEACTIVE = 2
}

export enum ESaleType{
    MANAGER = 1,
    STAFF = 2,
    COLLABORATOR = 3,
    CROSS_SELLING = 4
}

export enum EIdentificationChangeReason {
    BLURRED = 1,
    TORNED = 2,
    EXPIRED = 3,
    OTHER = 4,
}

export enum ESource {
    ONLINE = 1,
    OFFLINE = 2,
    SALE = 3,
}

export enum ERelationship {
    CHONG = 1,
    VO,
    BO_ME_DE,
    BO_ME_VO_CHONG,
    CON_DE,
    CON_DAU_RE,
    CON_NUOI,
    ANH_RUOT,
    CHI_RUOT,
    EM_RUOT,
    ANH_EM_HO,
    ONG_BA,
    NGUOI_GIAM_HO,
    KHAC,
}