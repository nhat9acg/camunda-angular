export const KeyFilter = {
    taxCode: new RegExp(/^[0-9-]+$/),
    ownerAccountBank: new RegExp(/^[a-zA-Z]+$/),
}