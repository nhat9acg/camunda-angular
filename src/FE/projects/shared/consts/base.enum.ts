export enum EMaxLength {
    DEFAULT = 50,
    PHONE = 10,
    ACCOUNT_BANK = 20,
    REFERRAL_CODE = 30,
}

export enum ESource {
    ONLINE = 1,
    OFFLINE,
}

export enum ESourceOrder {
    ADMIN = 1,
    CUSTOMER,
    SALE,
}

