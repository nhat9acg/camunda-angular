export class SuccessMessage {
    public static SUC_ADD = "Thêm mới thành công";
    public static SUC_UPDATE = "Chỉnh sửa thành công";
    public static SUC_DELETE = "Xóa thành công";
    public static SUC_ADD_REQUEST = "Yêu cầu thêm mới thành công";
    public static SUC_UPDATE_REQUEST = "Yêu cầu chỉnh sửa thành công";
    public static SUC_DELETE_REQUEST = "Xóa thành công";
    public static SUC_VERIFY_REQUEST = "Yêu cầu xác minh tài khoản thành công";
}
