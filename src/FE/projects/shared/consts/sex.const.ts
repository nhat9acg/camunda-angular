export enum ESex {
    MALE = 1,
    FEMALE = 2,
}

export class SexConst {
	public static list = [
		{
			name: 'Nam',
			code: ESex.MALE,
		},
		{
			name: 'Nữ',
			code: ESex.FEMALE,
		},
	];

	public static getValue(code) {
		const item = this.list.find(s => s.code === code);
		return item?.name;
	}
}