import { environment } from "projects/shared/environments/environment";
import { BaseConsts } from "projects/shared/consts/base.consts";

export class HomeConsts {
    static readonly redirectUriCallbackLogin = `${environment.baseUrlHome}${BaseConsts.redirectLoginPath}?callBack=true`;
}
