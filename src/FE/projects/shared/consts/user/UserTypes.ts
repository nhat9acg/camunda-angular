export class UserTypes {
    //
    public static SUPER_ADMIN = 1;
    public static ADMIN = 2;
    public static CUSTOMER = 3;
    //
    public static list = [
        {
            name: 'SuperAdmin',
            code: this.SUPER_ADMIN,
            class: '',
        },
        {
            name: 'Admin',
            code: this.ADMIN,
            class: '',
        },
        {
            name: 'Customer',
            code: this.CUSTOMER,
            class: '',
        },
    ];

    public static getUserTypeInfo(code, property) {
        let type = this.list.find(t => t.code == code);
        if (type) return type[property];
        return '';
    }

}