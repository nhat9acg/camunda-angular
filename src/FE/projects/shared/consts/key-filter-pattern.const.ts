export class KeyFilterPatternConst {
    public static fullNamePattern: RegExp = /^[\p{L}\s'"\-]+/u;
    public static businessNamePattern: RegExp = /^[a-zA-Z0-9&.,+\-_]*$/;
    public static allCharactersPattern: RegExp = /^.$/;
    public static emailPattern: RegExp = /^[\p{L}\d\s,./-@+]+$/u;
    public static numberOnly: RegExp = /^[0-9]+$/u;
    public static blockSpace: RegExp = /[^s]/;
    public static globalPhonePattern: RegExp = /^[\d()+]+$/;
    public static numberDecimal: RegExp = /^[0-9.]+$/u;
    public static longitudeAndLatitude: RegExp = /^[0-9.]+$/u;
}
