export enum EKeyFilterTypes {
    INT = 'int',
    NUM = 'num',
    MONEY = 'money',
    HEX = 'hex',
    ALPHA = 'alpha',
    ALPHANUM = 'alphanum',
    // Thêm các giá trị khác nếu cần
}