
export enum EFormElements {
    INPUT = 'input',
    INPUT_SEARCH = 'input-search',
    CALENDAR = 'calendar',
    DROPDOWN = 'dropdown',
    TEXTAREA = 'textarea',
    INPUT_SWITCH = 'inputSwitch',
    MULTI_SELECT = 'multiSelect',
    LIB_MARKDOWN_HTML = 'lib-markdown-html',
    BUTTON = 'button',
    FORM_IMAGE= 'form-image',
    PASSWORD = 'password',
    NAVIGATION_APP = 'navigation-app',
    INPUT_VALUE = 'input-value',
    // Thêm các giá trị khác nếu cần
  }