export enum DefaultImage{
    IMG_ADD = 'shared/assets/layout/images/default-media-image/add-image-bg.png',
    IMG_DEFAUT = 'shared/assets/layout/images/default-media-image/image-bg-default.png',
    IMG_FRONT = 'shared/assets/layout/images/front-image.png',
    IMG_BACK = 'shared/assets/layout/images/back-image.png',
    AVATAR_DEFAULT = 'shared/assets/layout/images/avatar/avatar.png',
    MEDIA_DEFAULT_IMAGE = 'shared/assets/layout/images/default-media-image/BACKGROUND_IMAGE_DEFAULT.svg',
    IMG_AVATAR = 'shared/assets/layout/images/avatar/anonymous-avatar.jpg',
    LOGO_MEEYFINANCE = 'shared/assets/layout/images/logo/logo-meeyfinance.png',
    IMG_AVATAR_DEFAULT = 'shared/assets/layout/images/avatar/avatar-carmelita.png',
    PAGE_NOT_FOUND = 'shared/assets/layout/images/404NotFound/404NotFound.svg',
}
