export enum PersonalCustomerStatusEnum {
  ACTIVE = 1,
  DEACTIVE = 2,
}

export enum ApproveActionEnum {
  CREATE = 1,
  UPDATE = 2,
  DELETE = 3,
}

export enum ApproveStatusEnum {
  INIT = 1,
  APPROVE = 2,
  CANCEL = 3,
}

export enum RequestStatusEnum {
  INIT = 1,
  REQUEST = 2,
  APPROVE = 3,
  CANCEL = 4,
}
