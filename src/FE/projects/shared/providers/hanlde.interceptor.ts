import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Observable, EMPTY } from 'rxjs';
import { catchError, finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import { TokenService } from '../../my-lib/src/lib/shared/services/auth/token.service';
import { ErrorCode } from 'projects/shared/consts/error-code.const';
import { Utils } from 'projects/shared/utils';
import { ITokenResponse } from 'projects/shared/interfaces/token.interface';
import { AppAuthService } from 'projects/my-lib/src/lib/shared/services/auth/app-auth.service';
import { environment } from '../environments/environment';

@Injectable()
export class HanldeHttpInterceptor implements HttpInterceptor {
    constructor(
        private messageService: MessageService,
        private _tokenService: TokenService,
        private _authService: AppAuthService,
    ) {}

    requestPostApi: string;
	  env = environment;

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token: string = this._tokenService.getToken();
        let refreshToken: string = this._tokenService.getRefreshToken();
        const method: string = request.method;
        console.log('method', method);

        const methodCreateOrUpdates = ['POST', 'PUT', 'PATCH'];
        const paramRemoves = ['unAuthorize', 'enableMultileRequest'];
        const unAuthorize = request.params.get('unAuthorize') === true.toString();
        const enableMultileRequest = !!request.params.get('enableMultileRequest');

        // BLOCK DOUBLE CLICK CALL API POST, PUT, PATH
        if(methodCreateOrUpdates.includes(method) && !enableMultileRequest) {
            if(request.url !== this.requestPostApi) {
                this.requestPostApi = request.url;
            } else {
                return EMPTY;
            }
        }

        let params = new HttpParams();
        let paramKeys = request.params.keys();
        //
        paramKeys.forEach(key => {
            // Get value full của key nếu nhiều value thì for append mới truyền key nhiều giá trị được
            if(!paramRemoves.includes(key)){
                request.params.getAll(key).forEach(value => {
                    params = params.append(key, value);
                })
            }
        })
        // Add Token to api
        if(token) {
            let headers = !unAuthorize ? {'Authorization': `Bearer ${token}`} : {};
            request = request.clone({
                setHeaders: headers,
                params: params,
            });
        }

        return next.handle(request).pipe(
            finalize(() => {
				    // this.removeNgxLoading();
                this.requestPostApi = '';
            }),
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    if(err?.status === 401 && !token) {
                        return this.refreshToken(request, next);
                    } else {
                        let message: string = '';
                        let statusCode: number = err?.status;
                        switch (statusCode) {
                            case 404:
                                message = 'Đường dẫn không tồn tại';
                                break;
                            case 401:
                                message = 'Tài khoản không có quyền truy cập';
                                break;
                            case 403:
                                message = err?.error?.message || err?.message;
                                break;
                            default:
                                message = 'Có lỗi xảy ra vui lòng thử lại sau!';
                        }
                        //
                        Utils.log(`statusCode ${err?.status}: ${ErrorCode.list[err?.status] || ''}`, `${message || err?.error?.message || err?.message}`)
                        this.messageService.add({
                            severity: 'error',
                            detail: message,
                            life: 2000
                        });
                    }
                }
                throw (err);
            }),

        );
    }

    refreshToken(request: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        return this._authService.refreshToken().pipe(
            switchMap((token: ITokenResponse) => {
                request = request.clone({
                    setHeaders: {
                        'Authorization': `Bearer ${token.access_token}`,
                    },
                });
                return next.handle(request);
            }),
            catchError(() => {
              this._authService.connectAuthorize();
              return next.handle(request);
            })
        );
    }

	// removeNgxLoading() {
	// 	const ngxEl: HTMLCollection = document.getElementsByTagName('ngx-spinner');
	// 	const ngxOverlayEl: HTMLCollection = ngxEl[0].getElementsByClassName('ngx-spinner-overlay');
	// 	console.log('ngxOverlayEl', ngxOverlayEl);
	// 	if(ngxEl.length) {
	// 		ngxOverlayEl[0].remove();
	// 	}

	// }
}
