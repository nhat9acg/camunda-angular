import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "projects/shared/environments/environment";
import { BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class PermissionsService {

    constructor(
		private http: HttpClient
	) {}

    permissions: string[] = [];

    permissionSubject = new BehaviorSubject<any>(null);
    getPermissions$ = this.permissionSubject.asObservable();
    //

    setPermissions(permissions) {
        this.permissionSubject.next(permissions)
    }

    getPermissions() {
        this.getPermissions$.subscribe((response) => {
            this.permissions = response || [];
        });
    }

    activeGetPermisionSubject = new BehaviorSubject<any>(null);
    fetchPermission$ = this.activeGetPermisionSubject.asObservable();

    activeCheckPermission(currentPermission) {
        this.activeGetPermisionSubject.next(currentPermission);
    }

    /**
     * Kiểm tra có permission không
     * @param permissionName
     * @returns
     */
    isGrantedRoot(permissionNames: string[] = []): boolean {
        for(let permission of permissionNames) {
            if(permission === undefined) console.log("Key quyền không tồn tại FE!", permission);
            if(this.permissions.includes(permission)) {
                return true;
            }
        }
        return false;
    }

    getPermissionInWeb(webKey?: number) {
        let params = new HttpParams();
        if(webKey) params = params.set('permissionInWeb', webKey);
        //
        let domainApi = environment.urlAuthLogin;
        return this.http.get(`${domainApi}/api/auth/permission/get-permissions`, { params: params });
    }
}
