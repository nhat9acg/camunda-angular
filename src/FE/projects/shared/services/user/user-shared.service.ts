import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from 'projects/user/src/app/core/services/user/user.serice';

@Injectable({
    providedIn: 'root'
})
export class UserSharedService extends UserService {
    constructor(
        http: HttpClient
    ) {
        super(http)
    }
}
