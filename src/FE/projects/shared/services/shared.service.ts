import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from 'projects/shared/models/page';
import { BasicFilter } from 'projects/shared/models/filter.model';
import { environment } from 'projects/shared/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TestService {

    private apiUrl = `${environment.api}/api/core/business-customer`;
    public businessCustomerId: number;
    constructor(private http: HttpClient) {}

    getBusinessCustomerList(): Observable<any> {
        let params = new HttpParams()
                .set('pageNumber', 1)
                .set('pageSize', 25);
        //
        return this.http.get<any>(`${this.apiUrl}/find-all`, { params });
    }
}
