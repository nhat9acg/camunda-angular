export interface IMenu {
	label: string;
	icon: string;
	routerLink: [string];
	isShow?: boolean;
	items?: IMenu[];
}

