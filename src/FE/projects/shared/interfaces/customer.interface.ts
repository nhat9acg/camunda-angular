export interface IBank {
    bankCode?: string,
    bankName?: string,
    id: number,
    fullBankName?: string,
    labelName?: string,
    bankAccount?: string,
    ownerAccount?: string
}