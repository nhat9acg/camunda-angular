export interface IValidRangeNumber {
    min?: number,
    max?: number,
    minMessage?: string,
    maxMessage?: string,
    field?: string,
    unit?: string
}
