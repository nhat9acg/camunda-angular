import { EFormatDate, EFormatDateDisplay } from "projects/shared/consts/base.consts";
import { ETableColumnType, ETableFrozen, OrderSort, ValueType } from "projects/shared/consts/e-table.consts";

export interface IColumn {
    field: string;
    header: string;
    title?:string,
    width?: number;
    minWidth?: number;
    left?: number; // fix frozen left
    right?: number; // fix frozen right
    isShow?: boolean,
    hasUpdateIsShow?: boolean,
    isSort?: boolean,
    type?: ETableColumnType | EFormatDateDisplay;
    otherType?: ETableColumnType | EFormatDateDisplay;
    isPin?: boolean; // Khóa cột
    isFrozen?: boolean; // Fixed cột
    alignFrozen?: ETableFrozen;   // Vị trí fixed cột left|right
    isResize?: boolean; // width auto
    class?: string;
    icon?: string;
    classButton?: string;
    isCutText?: boolean;
    position?: number;
    displaySettingColumn?: boolean;
    unit?: string;
    fieldSort?: string;
    getTagInfo?: Function;
    getDisabled?: Function;
    action?: Function;
    customValue?: Function;
    valueType?: ValueType;
    sliceString?: number;
    isPermission?: boolean;
    height?: number;
    isCheckboxIcon?:boolean;
    isHideOptionCheckbox?: boolean;
    checkDisableButtonAction?: Function;
}

export interface IAction {
    data: any;
    label: string;
    icon: string;
    command: Function;
}

export interface ISort {
    field: string;
    order: OrderSort;
}

export interface ICustomValueImage {
    value: string;
    url: string;
	default?: string;
}
