import { EAcceptFile, EIconConfirm, ERedirectActiveTabNames } from "../consts/base.consts";

export interface IEConfirm {
    message: string,
    icon?: EIconConfirm,
}

export interface IParamHandleDTO {
    idName?: string;
    isCheckNull?: boolean;
}

export interface IType {
    name?: string;
    code?: string | number;
}

export interface ITag extends IType {
    severity: string,
}

export interface IErrorCode {
    [key: number] : string,
}

export interface IDialogUploadFileConfig {
    folderUpload?: string,
    header?: string,
    width?:string,
    uploadServer?: boolean,
    multiple?: boolean,
    accept?: EAcceptFile,
    quantity?: number, // Số phần tử upload lấy từ phần tử đầu tiên
    previewBeforeUpload?: boolean,
    callback?: Function,
    chooseLabel?: string,
    domain?: string,
    //
    titleInput?: string,
    inputValue?: string,
    inputRequired?: boolean,
    isChooseNow?: boolean,
}

export interface IResponseDialogUpload<T> {
    inputData: string,
    fileUrls: T[], // string[] | File[]
}

export interface BackLinkParams {
  backLink?: string,
  activeTabName?: ERedirectActiveTabNames | string,
}

export interface IDropdown{
    labelName: string,
    value: number | string
}
