import { PermissionTypes, WebKeys } from "../consts/permissionWeb/permission.enum";

export interface IWebConfig {
	image: string,
	code: string,
	type: PermissionTypes,
	webKey: WebKeys,
	permissionWeb: string,
	permissionKeySetting: string,
	name: string,
	url: string,
	description: string,
	createdDate: string,
	logo: string,
}

export interface IPermissionTabs{
	segmentFirst?: string,
	routerFunction?: string,
	tabs: IPermissionTab[]
}

export interface IPermissionTab{
	name: string,
	permission: string
}
