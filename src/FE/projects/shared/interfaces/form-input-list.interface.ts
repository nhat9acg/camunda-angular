import { ValidatorFn } from "@angular/forms";
import { EFormElements } from "../enums/form-elements.enum";

export interface FormInputList {
    title: string;
    formGroupName?: string;
    inputs: FieldDetails[];
}

export interface FieldDetails {
    label: string;
    controlName: string;
    type: EFormElements | null;
    col: number;
    required?: boolean;
    options?: any[];
    optionLabel?: string;
    optionValue?: string;
    rows?: number;
    title?: string;
    hidden?: boolean;
    formControlContentType?: string;
    pKeyFilter?: string | RegExp;
    validatorMessage?: any;
    disabled?: boolean;
    filterBy?: string;
    placeholder?: string;
    maximumDisplayLength?: number;
    isTooltip?: boolean;
    customValidator?: [ValidatorFn]; //
    defaultValuePath?: string; //đường dẫn mặc định
    defaultValue?: any; //giá trị mặc định
    hiddenLabel?: boolean;
    maxLength?: number;
	templateDropdown?: boolean;
    isShowIcon?: boolean;
}

export interface IndexList {
    groupIndex: number;
    inputIndex : number;
}
