import { UserTypes } from "../consts/user/UserTypes";

export interface IUserLogin {
    username?: string, 
    userType?: UserTypes, 
    id?: number, 
    fullName?: string,
    position?: number,
    avatarImageUri?: string,
}